[![pipeline status](https://gitlab.inria.fr/bramas/autovesk/badges/master/pipeline.svg)](https://gitlab.inria.fr/bramas/autovesk/commits/master)

[![coverage report](https://gitlab.inria.fr/bramas/autovesk/badges/master/coverage.svg)](https://bramas.gitlabpages.inria.fr/autovesk/)

Autovesk is a standalone tool for vectorizing automatically static kernels.
For instance, Autovesk can vectorize:
```cpp
template <class NumType>
void KernelExample3(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(size == 10);
    outValue[0] = inValue2[4] + inValue1[2];
    outValue[3] = inValue2[4] + inValue1[2];
    outValue[1] = inValue2[3] + inValue1[5];
    outValue[9] = inValue2[1] + inValue1[5];
    outValue[5] = inValue2[8] + inValue1[4];
    outValue[6] = inValue2[6] + inValue1[4];
    outValue[4] = inValue2[0] + inValue1[0];
    outValue[7] = inValue2[1] + inValue1[0];
}

```

Or
```cpp
template <class NumType>
void KernelExample4(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+2)%size];
    }
}
```

Autovesk generates a graph of scalar instructions from a C++ code and then works on it to generate a graph of vectorial instructions.
Several heuristics are used to achieve this objective. 
Autovesk is presented in a publication that has been sumbited recently, but a preprint is available here https://hal.inria.fr/hal-03914178 and can be used for citation.


