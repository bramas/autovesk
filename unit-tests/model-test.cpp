#include <immintrin.h>
#include <iostream>
#include <iostream>
#include <memory>
#include <assert.h>
#include "../src/timer.hpp"


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void KernelExample_vectorize(const double* tab0, const double* tab1, double* tab2){    
    @AVX-SOURCE
}

template <class NumType>
void KernelExample(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    @SCALAR-SOURCE
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// g++ -march=skylake-avx512 -mtune=skylake-avx512 test.cpp -o test.exe
int main(){
    std::cout << "[TEST]" << std::endl;
    const long int size = 10;
    double tab0[size];
    double tab1[size];
        
    for(long int idx = 0 ; idx < size ; ++idx){
        tab0[idx] = double(idx);
        tab1[idx] = double(idx);
    }

    double tab2[size] = {0};
    KernelExample(tab0, tab1, tab2, size);
    
    double tab2_vec[size] = {0};
    KernelExample_vectorize(tab0, tab1, tab2_vec);
    
    for(long int idx = 0 ; idx < size ; ++idx){
        std::cout << "Res[" << idx << "] " << "scalar: " << tab2[idx] << " # vectorial: " << tab2_vec[idx] << std::endl;
        assert(tab2[idx] == tab2_vec[idx]);
    }

    std::cout << "test succeeded" << std::endl;

    return  0;
}
