#include <memory>
#include <iostream>
#include <queue>
#include <variant>
#include <sstream>
#include <unordered_map>
#include <queue>
#include <cassert>
#include <set>
#include <fstream>
#include <map>
#include <algorithm>
#include "../src/autovesk.hpp"


// TODO add headers

/////////////////////////////////////////////////
/// Test functions
/////////////////////////////////////////////////

template <class NumType>
void KernelExample1(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[idx] * inValue2[idx];
    }
}

template <class NumType>
void KernelExample2(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx*2)%size];
    }
}

template <class NumType>
void KernelExample3(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    assert(size == 10);
    outValue[0] = inValue2[4] + inValue1[2];
    outValue[3] = inValue2[4] + inValue1[2];
    outValue[1] = inValue2[3] + inValue1[5];
    outValue[9] = inValue2[1] + inValue1[5];
    outValue[5] = inValue2[8] + inValue1[4];
    outValue[6] = inValue2[6] + inValue1[4];
    outValue[4] = inValue2[0] + inValue1[0];
    outValue[7] = inValue2[1] + inValue1[0];
}


template <class NumType>
void KernelExample4(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+2)%size];
    }
}

template <class NumType>
void KernelExample5(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[(idx+2)%size] = inValue1[idx] * inValue2[idx];
    }
}


template <class NumType>
void KernelExample6(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+4)%size];
    }
}

template <class NumType>
void KernelExample7(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = (idx%1 ? inValue1[idx] * inValue2[idx] : inValue1[idx] + inValue2[idx]);
    }
}

template <class NumType>
void KernelExample8(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = (idx%1 ? inValue1[(idx+2)%size] * inValue2[(idx+2)%size] : inValue1[(idx+2)%size] + inValue2[(idx+2)%size]);
    }
}

template <class NumType>
void KernelExample9(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    assert(size == 10);
    outValue[0] = inValue2[3] + inValue1[9];
    outValue[1] = inValue2[4] * inValue1[8];
    outValue[2] = inValue2[2] * inValue1[7];
    outValue[3] = inValue2[1] + inValue1[6];
    outValue[4] = inValue2[8] * inValue1[5];
    outValue[5] = inValue2[9] * inValue1[0];
    outValue[6] = inValue2[0] + inValue1[1];
    outValue[7] = inValue2[7] * inValue1[2];
    outValue[8] = inValue2[6] * inValue1[3];
    outValue[9] = inValue2[5] + inValue1[4];
}

template <class NumType>
void KernelExample10(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    assert(size == 10);
    NumType x,y;
    for(long int idx = 0 ; idx < size ; ++idx){
        x = inValue1[idx] + inValue2[idx];
        y = inValue1[(idx+2)%size] + inValue2[(idx+2)%size];
        outValue[idx] = x + y;
    }
}

template <class NumType>
void KernelExample11(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    assert(size == 10);
    NumType x,y;
    for(long int idx = 0 ; idx < size ; ++idx){
        x = inValue1[idx] + inValue2[idx];
        y = inValue1[(idx+2)%size] + inValue2[(idx+2)%size];
        outValue[idx] = x + y;
    }
}

template <class NumType>
void KernelExample12(const NumType* inValue1, [[maybe_unused]] const NumType* inValue2, NumType* outValue, const long size){
    assert(size == 10);
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample13(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] + inValue2[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample14(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] * inValue2[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample15(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    NumType x = NumType(0.);
    x = (x + inValue1[0] * inValue2[0]) + inValue1[1] * inValue2[1];

    outValue[0] = x;
}


int main(int argc, char** argv){
    using TreeType = Autovec::ScalarType;

    if((argc != 1 && argc != 3) || (argc == 3 && argv[1] != std::string("-export"))){
        std::cout << "Only -export can be set here" << std::endl;
        return 0;
    }

    Config configExport(argc, argv);

    if(!configExport.valid){
        return 1;
    }

    using BenchFunc = std::function<void(const TreeType*, const TreeType*, TreeType*, int)>;
    std::vector<std::tuple<BenchFunc, std::string, std::string>> testFuncs;

    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string>(KernelExample1<TreeType>, "KernelExample1", 
        "    for(long int idx = 0 ; idx < size ; ++idx){\n"
        "        outValue[idx] = inValue1[idx] * inValue2[idx];\n"
        "    }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string>(KernelExample2<TreeType>, "KernelExample2",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx*2)%size];\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string>(KernelExample3<TreeType>, "KernelExample3",
        "   assert(size == 10);\n"
        "   outValue[0] = inValue2[4] + inValue1[2];\n"
        "   outValue[3] = inValue2[4] + inValue1[2];\n"
        "   outValue[1] = inValue2[3] + inValue1[5];\n"
        "   outValue[9] = inValue2[1] + inValue1[5];\n"
        "   outValue[5] = inValue2[8] + inValue1[4];\n"
        "   outValue[6] = inValue2[6] + inValue1[4];\n"
        "   outValue[4] = inValue2[0] + inValue1[0];\n"
        "   outValue[7] = inValue2[1] + inValue1[0];\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample4<TreeType>, "KernelExample4",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+2)%size];\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample5<TreeType>, "KernelExample5",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[(idx+2)%size] = inValue1[idx] * inValue2[idx];\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample6<TreeType>, "KernelExample6",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+4)%size];\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample7<TreeType>, "KernelExample7",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[idx] = (idx%1 ? inValue1[idx] * inValue2[idx] : inValue1[idx] + inValue2[idx]);\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample8<TreeType>, "KernelExample8",
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   outValue[idx] = (idx%1 ? inValue1[(idx+2)%size] * inValue2[(idx+2)%size] : inValue1[(idx+2)%size] + inValue2[(idx+2)%size]);\n"
        "   }\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample9<TreeType>, "KernelExample9",
        "   assert(size == 10);\n"
        "   outValue[0] = inValue2[3] + inValue1[9];\n"
        "   outValue[1] = inValue2[4] * inValue1[8];\n"
        "   outValue[2] = inValue2[2] * inValue1[7];\n"
        "   outValue[3] = inValue2[1] + inValue1[6];\n"
        "   outValue[4] = inValue2[8] * inValue1[5];\n"
        "   outValue[5] = inValue2[9] * inValue1[0];\n"
        "   outValue[6] = inValue2[0] + inValue1[1];\n"
        "   outValue[7] = inValue2[7] * inValue1[2];\n"
        "   outValue[8] = inValue2[6] * inValue1[3];\n"
        "   outValue[9] = inValue2[5] + inValue1[4];\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample10<TreeType>, "KernelExample10",
        "   assert(size == 10);\n"
        "   NumType x,y;\n"
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   x = inValue1[idx] + inValue2[idx];\n"
        "   y = inValue1[(idx+2)%size] + inValue2[(idx+2)%size];\n"
        "   outValue[idx] = x + y;\n"
        "}\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample11<TreeType>, "KernelExample11",
        "   assert(size == 10);\n"
        "   NumType x,y;\n"
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   x = inValue1[idx] + inValue2[idx];\n"
        "   y = inValue1[(idx+2)%size] + inValue2[(idx+2)%size];\n"
        "   outValue[idx] = x + y;\n"
        "}\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample12<TreeType>, "KernelExample12",
        "   assert(size == 10);\n"
        "   NumType x = NumType(0.);\n"
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   x += inValue1[idx];\n"
        "   }\n"
        "   outValue[0] = x;\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample13<TreeType>, "KernelExample13",
        "   NumType x = NumType(0.);\n"
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   x += inValue1[idx] + inValue2[idx];\n"
        "   }\n"
        "   outValue[0] = x;\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample14<TreeType>, "KernelExample14",
        "   NumType x = NumType(0.);\n"
        "   for(long int idx = 0 ; idx < size ; ++idx){\n"
        "   x += inValue1[idx] * inValue2[idx];\n"
        "   }\n"
        "   outValue[0] = x;\n"));
    testFuncs.emplace_back(std::make_tuple<BenchFunc, std::string, std::string>(KernelExample15<TreeType>, "KernelExample15",
        "   NumType x = NumType(0.);\n"
        "   x = (x + inValue1[0] * inValue2[0]) + inValue1[1] * inValue2[1];\n"
        "   outValue[0] = x;\n"));

    std::string scalar("@SCALAR-SOURCE");
    std::string vectorial("@AVX-SOURCE");

    for(const auto& currentFunc : testFuncs){
        std::cout << "=> Test " << std::get<1>(currentFunc) << std::endl;

        std::cout << "Init data" << std::endl;

        const long int Size = 10;
        TreeType vec1[Size];
        TreeType vec2[Size];
        TreeType result[Size];

        for(long int idx = 0 ; idx < Size ; ++idx){
            vec1[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 0, idx));
            vec2[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 1, idx));
            result[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 2, idx));
        }

        std::cout << "Run kernel" << std::endl;

        std::get<0>(currentFunc)(vec1, vec2, result, Size);

        std::cout << "Insert tree" << std::endl;

        for(long int idx = 0 ; idx < Size ; ++idx){
            assert(result[idx].node->getNbInputs(1) == 0 || result[idx].node->getNbInputs(0) != 0);
            if(result[idx].node->getNbInputs(0)
                    || result[idx].node->getData()->isSet()
                    || (result[idx].node->getData()->isLoad() &&
                        (result[idx].node->getData()->getScalar().getLoad().ptr != 2
                        || result[idx].node->getData()->getScalar().getLoad().offset != idx))){
                std::shared_ptr<Autovec::Node> newNode;
                newNode.reset(new Autovec::Node);
                newNode->getData() = (Autovec::Instruction::make_scalar_store("double", 2, idx));
                Autovec::Node::Connect(newNode, result[idx].node, 0);
                result[idx].node = newNode;
            }
        }

        std::cout << "======================" << std::endl;
        std::cout << "Resulting instructions" << std::endl;
        std::cout << "======================" << std::endl;

        std::vector<std::shared_ptr<Autovec::Node>> outputs;

        for(long int idx = 0 ; idx < Size ; ++idx){
            if(result[idx].node->getNbInputs(0)){
                outputs.emplace_back(result[idx].node);
            }
        }

        Print3AC(outputs);

        TreeToDot(outputs, Autovec::StringMerge("/tmp/scalar-", std::get<1>(currentFunc), ".dot"));

        const int VEC_SIZE = 8;
        std::cout << "=================================" << std::endl;
        std::cout << "Vectorize (Vec size = " << VEC_SIZE << ")" << std::endl;
        std::cout << "=================================" << std::endl;

        Config config(SplitLoadsAndStores::GreedyWithConfig, SplitOperations::GreedySplitOp,
                      Reduction::GreedyReduction, ConstantPropagation::EnableConstantPropagation, true,
                      VEC_SIZE);

        std::vector<std::shared_ptr<Autovec::Node>>  vectorizedOutputs = Autovec::GraphTransformation::Vectorize(&outputs, config);

        Print3AC(vectorizedOutputs);

        TreeToDot(vectorizedOutputs, Autovec::StringMerge("/tmp/vectorized-", std::get<1>(currentFunc), "-", VEC_SIZE, ".dot"));

        if(configExport.format == ExportFormat::AVX512){
            std::cout << "======================" << std::endl;
            std::cout << "AVX12" << std::endl;
            std::cout << "======================" << std::endl;

            std::stringstream avxCode;
            GenerateAVX512(vectorizedOutputs, avxCode, config.minimizeRegisters, config.vecsize); // TODO templatize to pass output
            std::cout << avxCode.str() << std::endl;

            std::ifstream inputFile;
            inputFile.open("../unit-tests/model-test.cpp", std::ifstream::in);

            if (inputFile.is_open()) {
                std::string fileContents = std::string((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
                inputFile.close();

                size_t pos = fileContents.find(scalar);
                fileContents.replace(pos, scalar.length(), std::get<2>(currentFunc));

                pos = fileContents.find(vectorial);
                fileContents.replace(pos, vectorial.length(), avxCode.str());

                std::ofstream outfile("../test-kernels/" + std::get<1>(currentFunc) + ".cpp");
                outfile << fileContents << std::endl;
                outfile.close();

            }else{
                std::cerr << "Could not open the file - '" << "model-test.cpp" << "'" << std::endl;
            }
        }
        if(configExport.format == ExportFormat::AVX512ASM){
            std::cout << "======================" << std::endl;
            std::cout << "AVX12ASM" << std::endl;
            std::cout << "======================" << std::endl;

            std::stringstream avxCode;
            GenerateAVX512Asm(vectorizedOutputs, avxCode, config.minimizeRegisters, config.vecsize,
                              "KernelExample_vectorize", false);
            std::cout << avxCode.str() << std::endl;

            std::ifstream inputFile;
            inputFile.open("../unit-tests/model-test-asm.cpp", std::ifstream::in);

            if (inputFile.is_open()) {
                std::string fileContents = std::string((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
                inputFile.close();

                size_t pos = fileContents.find(scalar);
                fileContents.replace(pos, scalar.length(), std::get<2>(currentFunc));

                pos = fileContents.find(vectorial);
                fileContents.replace(pos, vectorial.length(), avxCode.str());

                std::ofstream outfile("../test-kernels/" + std::get<1>(currentFunc) + "-asm.cpp");
                outfile << fileContents << std::endl;
                outfile.close();

            }else{
                std::cerr << "Could not open the file - '" << "model-test-asm.cpp" << "'" << std::endl;
            }
        }
        if(configExport.format == ExportFormat::ARMSVE){
            std::cout << "======================" << std::endl;
            std::cout << "ARMSVE" << std::endl;
            std::cout << "======================" << std::endl;

            std::stringstream avxCode;
            GenerateARM_SVE(vectorizedOutputs, avxCode, config.minimizeRegisters, config.vecsize);
            std::cout << avxCode.str() << std::endl;

            std::ifstream inputFile;
            inputFile.open("../unit-tests/model-test-armsve.cpp", std::ifstream::in);

            if (inputFile.is_open()) {
                std::string fileContents = std::string((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
                inputFile.close();

                size_t pos = fileContents.find(scalar);
                fileContents.replace(pos, scalar.length(), std::get<2>(currentFunc));

                pos = fileContents.find(vectorial);
                fileContents.replace(pos, vectorial.length(), avxCode.str());

                std::ofstream outfile("../test-kernels/" + std::get<1>(currentFunc) + "-armsve.cpp");
                outfile << fileContents << std::endl;
                outfile.close();

            }else{
                std::cerr << "Could not open the file - '" << "model-test-armsve.cpp" << "'" << std::endl;
            }
        }
    }
    return 0;
}
