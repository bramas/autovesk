#include <cassert>
#include "../src/config.hpp"

int main(){
    {
        Config conf;
        assert(conf.valid);
    }
    {
        int argc = 0;
        char** argv = nullptr;
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 1;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("--help");
        Config conf(argc, argv);
        assert(conf.valid == false);
    }
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("false");
        Config conf(argc, argv);
        assert(conf.valid == false);
    }
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-verbose");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    // -reduction [enable*, disable]
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-reduction");
        Config conf(argc, argv);
        assert(conf.valid == false);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-reduction");
        argv[2] = const_cast<char*>("enable");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-reduction");
        argv[2] = const_cast<char*>("disable");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    // -split-ls [naive*, greedy]
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-ls");
        Config conf(argc, argv);
        assert(conf.valid == false);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-ls");
        argv[2] = const_cast<char*>("naive");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-ls");
        argv[2] = const_cast<char*>("greedy");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    // -split-op [no-order*, together-true, together-false, one-true, one-false]
    {
        const int argc = 2;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        Config conf(argc, argv);
        assert(conf.valid == false);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        argv[2] = const_cast<char*>("no-order");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        argv[2] = const_cast<char*>("together-true");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        argv[2] = const_cast<char*>("together-false");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        argv[2] = const_cast<char*>("one-true");
        Config conf(argc, argv);
        assert(conf.valid);
    }
    {
        const int argc = 3;
        char* argv[argc];
        argv[0] = const_cast<char*>("autovestk...");
        argv[1] = const_cast<char*>("-split-op");
        argv[2] = const_cast<char*>("one-false");
        Config conf(argc, argv);
        assert(conf.valid);
    }


    return 0;
}
