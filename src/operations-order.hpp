#ifndef OPERATIONS_ORDER_HPP
#define OPERATIONS_ORDER_HPP
#include <iostream>
#include <vector>
#include <memory>
#include <set>
#include <unordered_map>
#include <algorithm>
#include <cassert>
#include <utility>

struct Operation{
    int idOperation;
    std::vector<std::pair<int,int>> depIdIndex;
    bool operator==(const Operation& inOther){
        return depIdIndex == inOther.depIdIndex;
    }
};

auto printOperations = [](const auto& inOperations){
    for(int idx = 0 ; idx < int(inOperations.size()) ; ++idx){
        std::cout << "[" << idx << "] = op(";
        for(int idxDep = 0 ; idxDep < int(inOperations[idx].depIdIndex.size()) ; ++idxDep){
            std::cout << inOperations[idx].depIdIndex[idxDep].first << "[" << inOperations[idx].depIdIndex[idxDep].second << "]";
            if(idxDep+1 != int(inOperations[idx].depIdIndex.size())){
                std::cout << ", ";
            }
        }
        std::cout << ") => id operation = " << inOperations[idx].idOperation << std::endl;
    }
};

auto countExtract = [](const auto& inOperations) -> int {
    std::set<int> depWithExtract;
    for(int idx = 0 ; idx < int(inOperations.size()) ; ++idx){
        for(int idxDep = 0 ; idxDep < int(inOperations[idx].depIdIndex.size()) ; ++idxDep){
            if(idx != inOperations[idx].depIdIndex[idxDep].second){
                depWithExtract.insert(inOperations[idx].depIdIndex[idxDep].first);
            }
        }
    }
    return int(depWithExtract.size());
};

std::vector<Operation> GenerateOrder(const std::vector<Operation>& inOperations){
    auto areDisjoint = [](const auto& s1, const auto& s2) -> bool {
        for(auto& i1 : s1){
            if(s2.find(i1) != s2.end()){
                return false;
            }
        }
        return true;
    };

    struct DepNode{
        explicit DepNode(const int inIdDep) : idDep(inIdDep){}

        int idDep;
        std::set<int> depsNode;
        std::set<const Operation*> operations;
        std::set<int> idxs;
        std::unordered_map<const Operation*, std::set<int>> operationToIdxs;
        std::unordered_map<int, std::set<const Operation*>> idxToOperations;
    };

    std::vector<std::shared_ptr<DepNode>> allNodes;
    std::unordered_map<int, std::shared_ptr<DepNode>> idToNode;

    // Create the nodes
    for(const auto& op : inOperations){
        // Iterate over dependecies indices
        for(int idx1 = 0 ; idx1 < int(op.depIdIndex.size()); ++idx1){
            // Create the node if it doesn't exist in the mapping idToNode
            if(idToNode.find(op.depIdIndex[idx1].first) == idToNode.end()){
                idToNode[op.depIdIndex[idx1].first] = std::make_shared<DepNode>(DepNode(op.depIdIndex[idx1].first));
                allNodes.emplace_back(idToNode[op.depIdIndex[idx1].first]);
            }
            idToNode[op.depIdIndex[idx1].first]->operations.insert(&op);
            idToNode[op.depIdIndex[idx1].first]->idxs.insert(op.depIdIndex[idx1].second);
            idToNode[op.depIdIndex[idx1].first]->operationToIdxs[&op].insert(op.depIdIndex[idx1].second);
            idToNode[op.depIdIndex[idx1].first]->idxToOperations[op.depIdIndex[idx1].second].insert(&op);
        }
    }

    // Connect the nodes if they are linked by some operations (including self connect)
    for(const auto& op : inOperations){
        for(int idx1 = 0 ; idx1 < int(op.depIdIndex.size()); ++idx1){
            for(int idx2 = idx1+1 ; idx2 < int(op.depIdIndex.size()); ++idx2){
                // Not the same index, so fixing one implies extract to the other
                if(op.depIdIndex[idx1].second != op.depIdIndex[idx2].second){
                    idToNode[op.depIdIndex[idx1].first]->depsNode.insert(op.depIdIndex[idx2].first);
                    idToNode[op.depIdIndex[idx2].first]->depsNode.insert(op.depIdIndex[idx1].first);
                }
            }
        }
    }

    for(int idx1 = 0 ; idx1 < int(allNodes.size()); ++idx1){
        for(int idx2 = idx1+1 ; idx2 < int(allNodes.size()); ++idx2){
            if(allNodes[idx1]->depsNode.find(allNodes[idx2]->idDep) == allNodes[idx1]->depsNode.end()){
                for(const int& idxUsed : allNodes[idx1]->idxs){
                    if(allNodes[idx2]->idxToOperations.find(idxUsed) != allNodes[idx2]->idxToOperations.end()
                            && !areDisjoint(allNodes[idx1]->idxToOperations[idxUsed], allNodes[idx2]->idxToOperations[idxUsed])){
                        // They have the same source position for different operations
                        // fixing one implies extract for the other
                        allNodes[idx1]->depsNode.insert(allNodes[idx2]->idDep);
                        allNodes[idx2]->depsNode.insert(allNodes[idx1]->idDep);
                        break;
                    }
                }
            }
        }
    }

    // Sort the nodes by number of constraints
    std::sort(allNodes.begin(), allNodes.end(), [](const auto& n1, const auto& n2){
        return n1->depsNode.size() < n2->depsNode.size();
    });

    std::set<int> unusedPositions;
    std::set<const Operation*> unusedOperations;
    for(int idxOp = 0 ; idxOp < int(inOperations.size()) ; ++idxOp){
        unusedPositions.insert(idxOp);
        unusedOperations.insert(&inOperations[idxOp]);
    }

    std::vector<Operation> result;
    result.resize(inOperations.size());

    std::set<int> blockedIds;

    for(const auto& nd : allNodes){
        if(blockedIds.find(nd->idDep) == blockedIds.end()){
            bool needExtractAnyway = false;
            for(const int& idxUsed : nd->idxs){
                // There is more than one operation, need extract anyway for idx
                if(nd->idxToOperations[idxUsed].size() > 1){
                    needExtractAnyway = true;
                    break;
                }
            }
            for(const auto& op : nd->operations){
                // The operation is used more than once, need extract anyway
                if(nd->operationToIdxs[op].size() > 1){
                    needExtractAnyway = true;
                    break;
                }
            }
            for(const auto& op : nd->operations){
                // Could not find an available position from the given index, need extract anyway 
                if(unusedPositions.find(*nd->operationToIdxs[op].begin()) == unusedPositions.end()){
                    needExtractAnyway = true;
                    break;
                }
            }
            if(!needExtractAnyway){
                for(const auto& op : nd->operations){
                    if(unusedOperations.find(op) != unusedOperations.end()){
                        result[*nd->operationToIdxs[op].begin()] = *op;
                        assert(unusedPositions.find(*nd->operationToIdxs[op].begin()) != unusedPositions.end());
                        unusedPositions.erase(*nd->operationToIdxs[op].begin());
                        unusedOperations.erase(op);
                    }
                    else{
                        assert(result[*nd->operationToIdxs[op].begin()] == *op);
                    }
                }           

                blockedIds.insert(nd->idDep);
                for(const auto& depId : nd->depsNode){
                    blockedIds.insert(depId);
                }
            }
        }
    }
    
    {
        assert(unusedPositions.size() == unusedOperations.size());
        auto iterIdx = unusedPositions.begin();
        auto iterOp = unusedOperations.begin();
        auto iterOpEnd = unusedOperations.end();
        while(iterOp != iterOpEnd){
            result[*iterIdx] = **iterOp;
            ++iterIdx;
            ++iterOp;
        }
    }
    
    return result;
}
#endif
