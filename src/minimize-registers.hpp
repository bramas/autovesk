#ifndef MINIMIZEREGISTERS_HPP
#define MINIMIZEREGISTERS_HPP
#include <vector>
#include <iostream>
#include <cassert>
#include <unordered_map>
#include <limits>
#include <set>
#include <memory>
#include <queue>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <numeric>
#include <map>

namespace MinimizeRegisters{

struct InsDeps{
    std::vector<int> write;
    std::vector<int> read;
    int nbTmpRegisters = 0;
    bool isStore = false;

    std::string toStr() const{
        std::ostringstream str;
        str << "[READ] {";
        for(auto idxR : read){
            str << idxR << " ";
        }
        str << "}  Write {";
        for(auto idxW : write){
            str << idxW << " ";
        }
        str << "}";
        if(isStore){
            str << " isStore ";
        }
        str << " nbTmpRegisteres " << nbTmpRegisters;
        return str.str();
    }
};

inline int GetMaxId(const std::vector<InsDeps>& inIds){
    int maxId = 0;
    for(int idxId = 0 ; idxId < int(inIds.size()) ; ++idxId){
        const auto& op = inIds[idxId];
        for(auto& idxR : op.read){
            maxId = std::max(maxId, idxR);
        }
        for(auto& idxW : op.write){
            maxId = std::max(maxId, idxW);
        }
    }
    return maxId;
}


struct Node{
    int opId;
    std::vector<std::shared_ptr<Node>> predecessors;
    std::vector<std::shared_ptr<Node>> successors;
};

template <class T1, class T2>
inline std::set<int> Intersection(const T1& inL1, const T2& inL2){
    std::set<int> res;
    for(const auto& val : inL2){
        if(inL1.find(val) != inL1.end()){
            res.insert(val);
        }
    }
    return res;
}

inline void DagToDot(const std::vector<std::shared_ptr<Node>>& inRoots,
              const std::string filename,
              const std::unordered_map<int,std::string>& meta){
    std::ofstream ostream(filename);

    ostream << "digraph G {\n";
    {
        std::vector<int> countDeps(inRoots.size(), 0);
        std::queue<std::shared_ptr<Node>> readyNodes;

        for(auto& nd : inRoots){
            readyNodes.push(nd);
        }

        while(readyNodes.size()){
            auto nd = readyNodes.front();
            readyNodes.pop();

            if(meta.find(nd->opId) != meta.end()){
                ostream << nd->opId << " [label=\"id = " << nd->opId << "\\l" << (*meta.find(nd->opId)).second << "\"];\n";
            }

            for(auto& succ : nd->successors){
                ostream << nd->opId << " -> " << succ->opId << ";\n";
                if(int(countDeps.size()) <= succ->opId){
                    countDeps.resize(succ->opId+1, 0);
                }
                countDeps[succ->opId] += 1;
                assert(countDeps[succ->opId] <= int(succ->predecessors.size()));
                if(countDeps[succ->opId] == int(succ->predecessors.size())){
                    readyNodes.push(succ);
                }
            }
        }
    }

    ostream << "}\n";
}

inline void DagToDot(const std::vector<std::shared_ptr<Node>>& inRoots,
              const std::string filename){
    std::unordered_map<int,std::string> meta;
    DagToDot(inRoots, filename, meta);
}

inline std::tuple<std::unordered_map<int, std::shared_ptr<Node>>,std::vector<std::shared_ptr<Node>>>
            CreateDag(const std::vector<InsDeps>& inIds){
    std::unordered_map<int, std::shared_ptr<Node>> allNodes;
    std::vector<std::shared_ptr<Node>> roots;

    std::unordered_map<int,std::vector<int>> useRead;
    std::unordered_map<int,int> useWrite;

    for(int idxOp = 0 ; idxOp < int(inIds.size()) ; ++idxOp){
        std::shared_ptr<Node> nd(new Node);
        nd->opId = idxOp;
        allNodes[idxOp] = nd;

        const auto& op = inIds[idxOp];
        bool hasPredecessors = false;

        for(const int idxR : op.read){
            if(std::find(op.write.begin(), op.write.end(), idxR) == op.write.end()
                    && useWrite.find(idxR) != useWrite.end()){
                allNodes[useWrite[idxR]]->successors.push_back(nd);
                nd->predecessors.push_back(allNodes[useWrite[idxR]]);
                hasPredecessors = true;
            }
            useRead[idxR].push_back(nd->opId);
        }
        for(const int idxW : op.write){
            if(useWrite.find(idxW) != useWrite.end()
                    && useRead[idxW].size() == 0){
                allNodes[useWrite[idxW]]->successors.push_back(nd);
                nd->predecessors.push_back(allNodes[useWrite[idxW]]);
                hasPredecessors = true;
            }
            else {
                for(const int idxR : useRead[idxW]){
                    allNodes[idxR]->successors.push_back(nd);
                    nd->predecessors.push_back(allNodes[idxR]);
                    hasPredecessors = true;
                }
            }
            useRead[idxW].clear();
            useWrite[idxW] = nd->opId;
        }

        if(!hasPredecessors){
            roots.push_back(nd);
        }
    }
    assert(roots.size());
    assert(allNodes.size() <= inIds.size());

    return std::make_tuple(allNodes, roots);
}


inline std::tuple<int,int,std::vector<std::vector<int>>,std::vector<std::vector<int>>,
                  std::vector<std::unordered_map<int,int>>>
            GetNbPushPop(const std::vector<InsDeps>& inIds, const int nbRegisters){
    std::vector<std::unordered_map<int,int>> distanceNext(inIds.size());

    {
        std::unordered_map<int,int> lastestView;
        for(int idxId = 0 ; idxId < int(inIds.size()) ; ++idxId){
            const auto& op = inIds[idxId];
            for(auto idxR : op.read){
                assert(lastestView.find(idxR) != lastestView.end()); // must be use in write before
                lastestView[idxR] = int(inIds.size());
            }
            assert(op.write.size() || op.isStore);
            for(auto idxW : op.write){
                assert(lastestView.find(idxW) == lastestView.end()); // all variable can be written only once
                lastestView[idxW] = int(inIds.size());
            }
        }

        for(int idxIdsBack = int(inIds.size())-1 ; idxIdsBack >= 0 ; --idxIdsBack){
            const auto& op = inIds[idxIdsBack];

            assert(nbRegisters >= int(op.read.size() + op.write.size()));

            distanceNext[idxIdsBack] = lastestView;
            for(auto idxR : op.read){
                lastestView[idxR] = idxIdsBack;
            }
            for(auto idxW : op.write){
                lastestView[idxW] = idxIdsBack;
            }
        }
    }

    std::unordered_map<int,int> usedRegisters;
    std::set<int> inStacks;
    int nbPush = 0;
    int nbPop = 0;

    std::vector<std::vector<int>> pushDetails(inIds.size());
    std::vector<std::vector<int>> popDetails(inIds.size());

    std::vector<std::unordered_map<int,int>> registerDetails(inIds.size());

    std::set<int> availableRegisters;
    for(int idx = 0 ; idx < nbRegisters ; ++idx){
        availableRegisters.insert(idx);
    }

    for(int idxOp = 0 ; idxOp < int(inIds.size()) ; ++idxOp){
        assert(int(usedRegisters.size()) <= nbRegisters);

        const auto& op = inIds[idxOp];
        // Fast access to what is used here
        std::set<int> usedHere;
        for(auto idxR : op.read){
            usedHere.insert(idxR);
        }
        for(auto idxW : op.write){
            usedHere.insert(idxW);
        }

        // We should not have durty registers
        for(auto inreg : usedRegisters){
            assert(distanceNext[idxOp][inreg.first] < int(inIds.size())
                   || usedHere.find(inreg.first) != usedHere.end());
            assert(idxOp == 0 || distanceNext[idxOp-1][inreg.first] != idxOp
                        || usedHere.find(inreg.first) != usedHere.end());
        }
//#define USE_VAL_IN_STACK
#ifndef USE_VAL_IN_STACK
        // We should not have durty stack
        for(auto instk : inStacks){
            assert(distanceNext[idxOp][instk] < int(inIds.size())
                   || usedHere.find(instk) != usedHere.end());
            assert(idxOp == 0 || distanceNext[idxOp-1][instk] != idxOp
                        || usedHere.find(instk) != usedHere.end());
        }
#else
        int idxInputInStack = -1;
        if(op.nbTmpRegisters == 0){// Not if temp reg is needed
            // Only for commutative
//            for(auto idxR : op.read){
//                if(usedRegisters.find(idxR) == usedRegisters.end()){
//                    if(idxInputInStack == -1
//                        || distanceNext[idxOp][idxR] > distanceNext[idxOp][idxInputInStack]){
//                        idxInputInStack = idxR;
//                    }
//                }
//            }
            if(op.read.size() && usedRegisters.find(op.read.front()) == usedRegisters.end()){
                idxInputInStack = op.read.front();
            }
            if(idxInputInStack != -1 ){
                const auto usedRegistersEnd = usedRegisters.end();
                for(auto iter = usedRegisters.begin() ; iter != usedRegistersEnd ; ++iter){
                    if(distanceNext[idxOp][(*iter).first] > distanceNext[idxOp][idxInputInStack]){
                        idxInputInStack = -1;
                        break;
                    }
                }
            }
        }
#endif
        std::unordered_map<int,int> pushedRegs;

//#define REUSE_LINPUT_AS_OUTPUT
#ifdef REUSE_LINPUT_AS_OUTPUT
        std::set<int> availableRegistersForOutput;
#endif
        // For all variables read by op
        for(auto idxR : op.read){
#ifdef USE_VAL_IN_STACK
            if(idxInputInStack == idxR){
                assert(usedRegisters.find(idxR) == usedRegisters.end());
            }
            else{
#endif
                // If currently NOT in a register
                if(usedRegisters.find(idxR) == usedRegisters.end()){
                    // We will get it back, but do registers are full?
                    if(availableRegisters.size() == 0){
                        // Cannot happen the first iteration!
                        assert(idxOp);
                        // Yes, so move one into stack
                        auto toRemove = usedRegisters.begin();
                        const auto usedRegistersEnd = usedRegisters.end();
                        for(auto iter = usedRegisters.begin() ; iter != usedRegistersEnd ; ++iter){
                            if(distanceNext[idxOp-1][(*iter).first] > distanceNext[idxOp-1][(*toRemove).first]){
                                toRemove = iter;
                            }
                        }
                        // We know it will be used later, otherwise it would have been removed by the previous
                        // loops
                        assert(distanceNext[idxOp][(*toRemove).first] < int(inIds.size()));
                        // Should not be used by current op
                        assert(usedHere.find((*toRemove).first) == usedHere.end());
                        pushDetails[idxOp].push_back((*toRemove).first);
                        inStacks.insert((*toRemove).first);
                        pushedRegs[(*toRemove).first] = (*toRemove).second;
                        nbPush += 1;
                        assert(availableRegisters.find((*toRemove).second) == availableRegisters.end());
                        availableRegisters.insert((*toRemove).second);
                        usedRegisters.erase(toRemove);
                    }
                    // At first, I would feel that it must be in stack (since it was not in register...)
                    assert(inStacks.find(idxR) != inStacks.end());
                    popDetails[idxOp].push_back(idxR);
                    inStacks.erase(idxR);
                    nbPop += 1;
                    assert(availableRegisters.size());
                    usedRegisters[idxR] = (*availableRegisters.begin());
                    availableRegisters.erase(availableRegisters.begin());
                }
    #ifdef REUSE_LINPUT_AS_OUTPUT
                // This one will not be used after
                if(distanceNext[idxOp][idxR] == int(inIds.size())){
                    assert(availableRegistersForOutput.find(usedRegisters[idxR]) == availableRegistersForOutput.end());
                    availableRegistersForOutput.insert(usedRegisters[idxR]);
                }
    #endif
#ifdef USE_VAL_IN_STACK
            }
#endif
        }

        std::unordered_map<int,int> inputUsedAsOutput;
        std::vector<int> availableRegistersForTmp;

        // For all variable write by op
        for(auto idxW : op.write){
            // Actually we know it will not be in register, since
            // we said a variable should be written only once
            assert(usedRegisters.find(idxW) == usedRegisters.end());
            // If never be used in read => it is a store, no register needed
            if(distanceNext[idxOp][idxW] != int(inIds.size())){
                int idReg = -1;
#ifdef REUSE_LINPUT_AS_OUTPUT
                if(availableRegistersForOutput.size()){
                    idReg = (*availableRegistersForOutput.begin());
                    availableRegistersForOutput.erase(availableRegistersForOutput.begin());
                }
                else
#endif
                {
                    // Do registers are full?
                    if(availableRegisters.size() == 0){
                        auto toRemove = usedRegisters.begin();
                        const auto usedRegistersEnd = usedRegisters.end();
                        for(auto iter = usedRegisters.begin() ; iter != usedRegistersEnd ; ++iter){
                            // Notice it is idxOp, and not idxOp+1 has for read
#ifdef REUSE_LINPUT_AS_OUTPUT
                            if(distanceNext[idxOp][(*iter).first] > distanceNext[idxOp][(*toRemove).first]){
#else
                            if((usedHere.find((*toRemove).first) != usedHere.end()
                                    && usedHere.find((*iter).first) == usedHere.end())
                                    || (distanceNext[idxOp][(*iter).first] > distanceNext[idxOp][(*toRemove).first]
                                        && usedHere.find((*iter).first) == usedHere.end())){
#endif
                                toRemove = iter;
                            }
                        }
                        assert(distanceNext[idxOp][(*toRemove).first] < int(inIds.size()));
                        pushDetails[idxOp].push_back((*toRemove).first);
                        inStacks.insert((*toRemove).first);
                        pushedRegs[(*toRemove).first] = (*toRemove).second;
                        nbPush += 1;
                        assert(availableRegisters.find((*toRemove).second) == availableRegisters.end());
                        if(usedHere.find((*toRemove).first) != usedHere.end()){
                            assert(inputUsedAsOutput.find((*toRemove).first) == inputUsedAsOutput.end());
                            inputUsedAsOutput[(*toRemove).first] = (*toRemove).second;
                        }
                        else{
                            availableRegistersForTmp.push_back((*toRemove).second);
                        }
                        idReg = (*toRemove).second;
                        usedRegisters.erase(toRemove);
                    }
                    else{
                        idReg = (*availableRegisters.begin());
                        availableRegisters.erase(availableRegisters.begin());
                        availableRegistersForTmp.push_back(idReg);
                    }
                    assert(usedRegisters.find(idxW) == usedRegisters.end());
                }
                assert(idReg != -1);
                usedRegisters[idxW] = idReg;
            }
            assert(idxOp == 0 || distanceNext[idxOp-1][idxW] == idxOp);
        }        

        // For needed tmp registers
        // Unfortunatly we cannot use the output registers as tmp sometime.....
        // So put zero for now
// #define USE_OUTPUT_AS_TMP
#ifdef  USE_OUTPUT_AS_TMP
        const int nbTempTakenFromOutput = std::min(op.nbTmpRegisters,int(availableRegistersForTmp.size()));
#else
        const int nbTempTakenFromOutput = 0;
#endif
        for(int idxTmp = 1 ; idxTmp <= nbTempTakenFromOutput ; ++idxTmp){
            assert((availableRegistersForTmp.size()));
            usedRegisters[-idxTmp] = availableRegistersForTmp.back();
            availableRegistersForTmp.pop_back();
        }
        for(int idxTmp = nbTempTakenFromOutput+1 ; idxTmp <= op.nbTmpRegisters ; ++idxTmp){
            // We will get it back, but do registers are full?
            if(availableRegisters.size() == 0){
                // Cannot happen the first iteration!
                assert(idxOp);
                // Yes, so move one into stack
                auto toRemove = usedRegisters.begin();
                const auto usedRegistersEnd = usedRegisters.end();
                for(auto iter = usedRegisters.begin() ; iter != usedRegistersEnd ; ++iter){
                    if(distanceNext[idxOp-1][(*iter).first] > distanceNext[idxOp-1][(*toRemove).first]){
                        toRemove = iter;
                    }
                }
                // We know it will be used later, otherwise it would have been removed by the previous
                // loops
                assert(distanceNext[idxOp][(*toRemove).first] < int(inIds.size()));
                // Should not be used by current op
                assert(usedHere.find((*toRemove).first) == usedHere.end());
                pushDetails[idxOp].push_back((*toRemove).first);
                inStacks.insert((*toRemove).first);
                pushedRegs[(*toRemove).first] = (*toRemove).second;
                nbPush += 1;
                assert(availableRegisters.find((*toRemove).second) == availableRegisters.end());
                availableRegisters.insert((*toRemove).second);
                usedRegisters.erase(toRemove);
            }
            assert(availableRegisters.size());
            // Do not see why assert(availableRegisters.size() != usedHere.size());
            usedRegisters[-idxTmp] = (*availableRegisters.begin());
            availableRegisters.erase(availableRegisters.begin());
        }

        // Save state
        registerDetails[idxOp] = usedRegisters;
        // Clean after op and update state
        for(auto toRemove : inputUsedAsOutput){
            assert(inStacks.find(toRemove.first) != inStacks.end() && usedHere.find(toRemove.first) != usedHere.end()
                    && distanceNext[idxOp][toRemove.first] < int(inIds.size()));
            assert(usedRegisters.find(toRemove.first) == usedRegisters.end());
            assert(registerDetails[idxOp].find(toRemove.first) == registerDetails[idxOp].end());
            registerDetails[idxOp][toRemove.first] = toRemove.second;
        }
        for(auto preg : pushedRegs){
            if(inputUsedAsOutput.find(preg.first) == inputUsedAsOutput.end()){
                assert(usedRegisters.find(preg.first) == usedRegisters.end());
                assert(registerDetails[idxOp].find(preg.first) == registerDetails[idxOp].end());
                registerDetails[idxOp][preg.first] = preg.second;
            }
        }

        for(int idxTmp = 1 ; idxTmp <= nbTempTakenFromOutput ; ++idxTmp){
            assert(usedRegisters.find(-idxTmp) != usedRegisters.end());
            usedRegisters.erase(-idxTmp);
        }
        for(int idxTmp = nbTempTakenFromOutput+1 ; idxTmp <= op.nbTmpRegisters ; ++idxTmp){
            availableRegisters.insert(usedRegisters[-idxTmp]);
            assert(usedRegisters.find(-idxTmp) != usedRegisters.end());
            usedRegisters.erase(-idxTmp);
        }
        for(auto iterReg = usedRegisters.begin() ; iterReg != usedRegisters.end(); ){
            assert(iterReg->first >= 0);
            if(distanceNext[idxOp][iterReg->first] == int(inIds.size())){
#ifdef REUSE_LINPUT_AS_OUTPUT
                if(availableRegistersForOutput.find(iterReg->second) != availableRegistersForOutput.end())
#endif
                {
                    assert(availableRegisters.find(iterReg->second) == availableRegisters.end());
                    availableRegisters.insert(iterReg->second);
                }
                iterReg = usedRegisters.erase(iterReg);
            }
            else{
                ++iterReg;
            }
        }
        // Sanity check, each register should appear once!
        assert(int(usedRegisters.size() + availableRegisters.size()) == nbRegisters);
        std::set<int> alreadyUse;
        for(auto reg : usedRegisters){
            assert(availableRegisters.find(reg.second) == availableRegisters.end());
            assert(alreadyUse.find(reg.second) == alreadyUse.end());
            alreadyUse.insert(reg.second);
        }
    }
    // Stack and registers should be empty
#ifndef USE_VAL_IN_STACK
    assert(inStacks.size() == 0);
#endif
    assert(usedRegisters.size() == 0);
    assert(int(availableRegisters.size()) == nbRegisters);

    return std::make_tuple(nbPush,nbPop,std::move(pushDetails),std::move(popDetails),std::move(registerDetails));
}

inline std::vector<std::set<int>> BuildGroups(const std::unordered_map<int, std::shared_ptr<Node>>& allNodes,
                                       const std::vector<std::shared_ptr<Node>>& roots){
    std::vector<std::set<int>> groups;

    std::vector<int> countDeps(allNodes.size(), 0);
    std::queue<int> readyNodes;

    for(auto& nd : roots){
        groups.emplace_back(std::set<int>{nd->opId});

        for(auto& succ : nd->successors){
            countDeps[succ->opId] += 1;
            assert(countDeps[succ->opId] <= int(succ->predecessors.size()));
            if(countDeps[succ->opId] == int(succ->predecessors.size())){
                readyNodes.push(succ->opId);
            }
        }
    }

    while(readyNodes.size()){
        auto ndId = readyNodes.front();
        const auto& nd = (*allNodes.find(ndId)).second;
        readyNodes.pop();

        assert(nd->predecessors.size());

        int idxGroup = -1;
        for(int idxTest = 0 ; idxTest < int(groups.size()) ; ++idxTest){
            if(groups[idxTest].find(nd->predecessors[0]->opId) != groups[idxTest].end()){
                idxGroup = idxTest;
                break;
            }
        }
        assert(idxGroup != -1);
        groups[idxGroup].insert(nd->opId);

        for(int idxPred = 1 ; idxPred < int(nd->predecessors.size()); ++idxPred){
            int idxOther = -1;
            for(int idxTest = 0 ; idxTest < int(groups.size()) ; ++idxTest){
                if(groups[idxTest].find(nd->predecessors[idxPred]->opId) != groups[idxTest].end()){
                    idxOther = idxTest;
                    break;
                }
            }
            assert(idxOther != -1);
            if(idxGroup != idxOther){
                if(idxOther < idxGroup){
                    std::swap(idxOther, idxGroup);
                }
                groups[idxGroup].insert(groups[idxOther].begin(), groups[idxOther].end());
                std::swap(groups[idxOther], groups.back());
                groups.pop_back();
            }
        }

        for(auto& succ : nd->successors){
            countDeps[succ->opId] += 1;
            assert(countDeps[succ->opId] <= int(succ->predecessors.size()));
            if(countDeps[succ->opId] == int(succ->predecessors.size())){
                readyNodes.push(succ->opId);
            }
        }
    }

    {// Sanity check
        for(int idxTg1 = 0 ; idxTg1 < int(groups.size()) ; ++idxTg1){
            for(int idxTg2 = idxTg1+1 ; idxTg2 < int(groups.size()) ; ++idxTg2){
                const auto& group1 = groups[idxTg1];
                const auto& group2 = groups[idxTg2];
                assert(std::size(Intersection(group1, group2)) == 0);
            }
        }
    }

    return groups;
}

template <class T>
inline std::vector<T> ApplyPermutation(const std::vector<T>& inOps,
                   const std::vector<int>& inPermute){
    assert(inOps.size() == inPermute.size());
    std::vector<T> permutedOps(inOps.size());
    for(int idx = 0 ; idx < int(inOps.size()) ; ++idx){
        assert(0 <= inPermute[idx] && inPermute[idx] < int(inOps.size()));
        permutedOps[idx] = inOps[inPermute[idx]];
    }
    return permutedOps;
}

template <class T>
inline std::vector<T> ApplyPermutationPartial(const std::vector<T>& inOps,
                   const std::vector<int>& inPermute){
    assert(inOps.size() >= inPermute.size());
    std::vector<T> permutedOps(inPermute.size());
    for(int idx = 0 ; idx < int(inPermute.size()) ; ++idx){
        assert(0 <= inPermute[idx] && inPermute[idx] < int(inOps.size()));
        permutedOps[idx] = inOps[inPermute[idx]];
    }
    return permutedOps;
}


inline std::vector<int> PermuteCutAndBranch(const std::vector<InsDeps>& inIds, const int inNbRegisters){
    if(inIds.size() == 0){
        return std::vector<int>();
    }

    std::unordered_map<int, std::shared_ptr<Node>> allNodes;
    std::vector<std::shared_ptr<Node>> roots;
    std::tie(allNodes,roots) = CreateDag(inIds);

    // Find independant graph
    std::vector<std::set<int>> groups = BuildGroups(allNodes, roots);

    std::cout << "There are " << groups.size() << " sub-graphs" << std::endl;
    assert(groups.size());


    struct State{
        std::vector<int> readyNodes;
        std::vector<int> countDeps;
        int choice;

        State() : choice(0){}

        bool hasNext() const{
            return choice != int(readyNodes.size());
        }

        int getAndMoveNext(){
            assert(hasNext());
            return choice++;
        }
    };

    std::vector<int> operationOrdered;

    for(const auto& group : groups){
        std::vector<int> bestOperationOrdered(group.size());
        //std::iota(std::begin(bestOperationOrdered), std::end(bestOperationOrdered), 0);
        std::copy(group.begin(), group.end(), bestOperationOrdered.begin());
        int bestPushPop = 0;
        {
            auto pushPop = GetNbPushPop(ApplyPermutationPartial(inIds, bestOperationOrdered), inNbRegisters);
            bestPushPop += std::get<0>(pushPop);
            bestPushPop += std::get<1>(pushPop);
        }
        std::vector<int> currentOperationOrdered;

        std::vector<State> statesAlloc(group.size());
        std::vector<State> states;
        {
            std::vector<int> readyNodes;

            for(auto& rtOpId : group){
                if(allNodes[rtOpId]->predecessors.size() == 0){
                    assert(std::find(roots.begin(), roots.end(), allNodes[rtOpId]) != roots.end());
                    readyNodes.push_back(rtOpId);
                }
            }
            assert(readyNodes.size());

            std::vector<int> countDeps(allNodes.size(), 0);

            State initState;
            initState.readyNodes = std::move(readyNodes);
            initState.countDeps = std::move(countDeps);
            states.emplace_back(std::move(initState));
        }

        while(states.size()){
            State& state = states.back();
            assert(state.hasNext());
            assert(state.readyNodes.size());

            const int nodeId = state.getAndMoveNext();
            assert(statesAlloc.size());
            std::vector<int> readyNodes = std::move(statesAlloc.back().readyNodes);
            readyNodes = state.readyNodes;
            std::vector<int> countDeps = std::move(statesAlloc.back().countDeps);
            countDeps = state.countDeps;

            assert(allNodes.find(readyNodes[nodeId]) != allNodes.end());
            auto toproceed = allNodes[readyNodes[nodeId]];
            assert(toproceed);
            std::swap(readyNodes[nodeId], readyNodes.back());
            readyNodes.pop_back();

            // Keep track of the found order
            assert(std::find(currentOperationOrdered.begin(), currentOperationOrdered.end(), toproceed->opId) == currentOperationOrdered.end());
            currentOperationOrdered.push_back(toproceed->opId);

            // Release dependencies
            for(auto succ : toproceed->successors){
                countDeps[succ->opId] += 1;
                if(countDeps[succ->opId] == int(succ->predecessors.size())){
                    assert(std::find(currentOperationOrdered.begin(), currentOperationOrdered.end(), succ->opId) == currentOperationOrdered.end());
                    assert(std::find(readyNodes.begin(), readyNodes.end(), succ->opId) == readyNodes.end());
                    readyNodes.push_back(succ->opId);
                }
            }

            if(readyNodes.size()){
                int currentPushPop = 0;
                {
                    auto pushPop = GetNbPushPop(ApplyPermutationPartial(inIds, currentOperationOrdered), inNbRegisters);
                    currentPushPop += std::get<0>(pushPop);
                    currentPushPop += std::get<1>(pushPop);
                }

                const bool cutBranch = (currentPushPop >= 2) && (double(currentPushPop)/double(currentOperationOrdered.size()))
                                            > (double(bestPushPop)/double(bestOperationOrdered.size()));
                if(!cutBranch){
                    State followingState;
                    followingState.readyNodes = std::move(readyNodes);
                    followingState.countDeps = std::move(countDeps);
                    states.emplace_back(std::move(followingState));
                }
                else{
                    std::cout << "Skip " << currentPushPop << " size " << currentOperationOrdered.size()
                                 << " against " << bestPushPop << std::endl;
                    currentOperationOrdered.pop_back();
                    while(states.size() && !states.back().hasNext()){
                        statesAlloc.emplace_back(std::move(states.back()));
                        states.pop_back();
                        currentOperationOrdered.pop_back();
                    }
                }
            }
            else{
                // Retreive score
                assert(group.size() == currentOperationOrdered.size());

                int currentPushPop = 0;
                {
                    auto pushPop = GetNbPushPop(ApplyPermutationPartial(inIds, currentOperationOrdered), inNbRegisters);
                    currentPushPop += std::get<0>(pushPop);
                    currentPushPop += std::get<1>(pushPop);
                }

                std::cout << "Test " << currentPushPop << " against " << bestPushPop << std::endl;

                if(currentPushPop < bestPushPop){
                    bestOperationOrdered = currentOperationOrdered;
                    bestPushPop = currentPushPop;

                    if(bestPushPop == 0){
                        break;
                    }
                }

                currentOperationOrdered.pop_back();
                while(states.size() && !states.back().hasNext()){
                    statesAlloc.emplace_back(std::move(states.back()));
                    states.pop_back();
                    currentOperationOrdered.pop_back();
                }
            }
        }

        assert(group.size() == bestOperationOrdered.size());
        std::copy(bestOperationOrdered.begin(), bestOperationOrdered.end(), std::back_inserter(operationOrdered));
    }

    assert(inIds.size() == operationOrdered.size());
    return operationOrdered;
}

template <class T>
inline void ProceedInOrder(const std::unordered_map<int, std::shared_ptr<Node>>& allNodes,
                    T&& func){
    std::vector<int> readyNodes;
    for(auto& iter : allNodes){
        auto& nd = iter.second;
        if(nd->predecessors.size() == 0){
            readyNodes.push_back(nd->opId);
        }
    }

    std::vector<int> countDeps(allNodes.size(), 0);

    while(readyNodes.size()){
        const int nodeId = readyNodes.back();
        readyNodes.pop_back();

        auto toproceed = (*allNodes.find(nodeId)).second;
        func(toproceed);

        for(auto succ : toproceed->successors){
            countDeps[succ->opId] += 1;
            if(countDeps[succ->opId] == int(succ->predecessors.size())){
                readyNodes.push_back(succ->opId);
            }
        }
    }
}

template <class T>
inline void ProceedReverse(const std::unordered_map<int, std::shared_ptr<Node>>& allNodes,
                    T&& func){
    std::vector<int> readyNodes;
    for(auto& iter : allNodes){
        auto& nd = iter.second;
        if(nd->successors.size() == 0){
            readyNodes.push_back(nd->opId);
        }
    }

    std::vector<int> countDeps(allNodes.size(), 0);

    while(readyNodes.size()){
        const int nodeId = readyNodes.back();
        readyNodes.pop_back();

        auto toproceed = (*allNodes.find(nodeId)).second;
        func(toproceed);

        for(auto pred : toproceed->predecessors){
            countDeps[pred->opId] += 1;
            if(countDeps[pred->opId] == int(pred->successors.size())){
                readyNodes.push_back(pred->opId);
            }
        }
    }
}

template <class T>
inline int CompareVecs(const std::vector<T>& vec1, const std::vector<T>& vec2,
                const bool shortedIsBetter = false){
    auto i1 = vec1.begin();
    auto end1 = vec1.end();

    auto i2 = vec2.begin();
    auto end2 = vec2.end();

    while(i1 != end1 && i2 != end2 && (*i1) == (*i2)){
        ++i1;
        ++i2;
    }

    int diff = 0;
    if(i1 != end1 && i2 != end2){
        diff = ((*i1) > (*i2) ? 1 : -1);
    }
    else if(i1 == end1){
        diff = (shortedIsBetter ? 1 : -1);
    }
    else{
        diff = (!shortedIsBetter ? 1 : -1);
    }
    return diff;
}

inline std::vector<int> Permute(const std::vector<InsDeps>& inIds,
                                const bool saveDots = false){
    if(inIds.size() == 0){
        return std::vector<int>();
    }

    // TODO start from all roots.
    // TODO re-check push/pop
    // TODO randomized after a given depth

    std::unordered_map<int, std::shared_ptr<Node>> allNodes;
    std::vector<std::shared_ptr<Node>> roots;
    std::tie(allNodes,roots) = CreateDag(inIds);

    // Find independant graph
    std::vector<std::set<int>> groups = BuildGroups(allNodes, roots);
    std::cout << "There are " << groups.size() << " sub-graphs" << std::endl;
    assert(groups.size());

    std::vector<int> operationOrdered;

    for(const auto& group : groups){
        std::vector<int> readyNodes;

        for(auto& rtOpId : group){
            if(allNodes[rtOpId]->predecessors.size() == 0){
                assert(std::find(roots.begin(), roots.end(), allNodes[rtOpId]) != roots.end());
                readyNodes.push_back(rtOpId);
            }
        }
        assert(readyNodes.size());

        std::vector<int> countDeps(allNodes.size(), 0);
        std::set<int> done;

        while(readyNodes.size()){
            int bestNodeIdx = -1;
            {
                auto delta = [&allNodes, &readyNodes, &done](const int id) -> double {
                    // Count the number of succ of pred that are done
                    double score = 0;
                    const auto nd = (*allNodes.find(id)).second;

                    // Someone will use id, so the new register will live after
                    if(nd->successors.size()){
                        score += 1;
                    }

                    for(auto& pred : nd->predecessors){
                        // Pred will disapear after this instruction
                        if(pred->successors.size() == 1){
                            score -= 1;
                        }
                        else if(done.find(pred->opId) != done.end()){
                            // Parent is done, is nd is only remaining
                            int countShare = 1;
                            for(auto& succ : pred->successors){
                                if(succ->opId != id && done.find(pred->opId) != done.end()){
                                    countShare += 1;
                                }
                            }
                            score -= double(1)/double(countShare);
                        }
                        else{
                            // Parent is not done, and as more than one succ,
                            // Nothing to do here
                        }

                    }
                    return score;
                };

                if(saveDots){
                    static int idx = 0;
                    std::unordered_map<int,std::string> meta;
                    ProceedInOrder(allNodes,
                                   [&meta, &delta](auto ndMeta){
                        meta[ndMeta->opId] = "delta=" + std::to_string(-delta(ndMeta->opId));
                    });
                    DagToDot(roots, "/tmp/details-" + std::to_string(idx) +".dot", meta);
                    idx += 1;
                }

                std::unordered_map<int,int> invDepth;
                ProceedReverse(allNodes,
                               [&invDepth, &done](auto ndMeta){
                    if(invDepth.find(ndMeta->opId) == invDepth.end()){
                        invDepth[ndMeta->opId] = 0;
                    }

                    for(auto& succ : ndMeta->successors){
                        assert(invDepth.find(succ->opId) != invDepth.end());
                        invDepth[ndMeta->opId] = std::max(invDepth[ndMeta->opId],
                                                          invDepth[succ->opId]+1);
                    }
                });


                std::vector<std::vector<double>> nodesScores(readyNodes.size());
                for(int idxNode = 0 ; idxNode < int(readyNodes.size()) ; ++idxNode){
                    nodesScores[idxNode].push_back(-delta(readyNodes[idxNode]));
                    assert(invDepth.find(readyNodes[idxNode]) != invDepth.end());
                    nodesScores[idxNode].push_back(invDepth[readyNodes[idxNode]]);
                }

                bestNodeIdx = 0;
                for(int idxNode = 1 ; idxNode < int(readyNodes.size()) ; ++idxNode){
                    const int diff = CompareVecs(nodesScores[bestNodeIdx],
                                                 nodesScores[idxNode]);

                    const bool otherIsBetter = (diff == -1);
                    if(otherIsBetter){
                        bestNodeIdx = idxNode;
                    }
                }

            }
            assert(bestNodeIdx != -1);
            const int bestNodeId = readyNodes[bestNodeIdx];
            std::swap(readyNodes[bestNodeIdx], readyNodes.back());
            readyNodes.pop_back();

            auto toproceed = allNodes[bestNodeId];
            done.insert(toproceed->opId);

            // Keep track of the found order
            assert(std::find(operationOrdered.begin(), operationOrdered.end(), toproceed->opId) == operationOrdered.end());
            operationOrdered.push_back(toproceed->opId);
            // Release dependencies
            for(auto succ : toproceed->successors){
                countDeps[succ->opId] += 1;
                if(countDeps[succ->opId] == int(succ->predecessors.size())){
                    assert(std::find(operationOrdered.begin(), operationOrdered.end(), succ->opId) == operationOrdered.end());
                    assert(std::find(readyNodes.begin(), readyNodes.end(), succ->opId) == readyNodes.end());
                    readyNodes.push_back(succ->opId);
                }
            }
        }
    }

    assert(inIds.size() == operationOrdered.size());
    return operationOrdered;
}

inline void PrintOperations(const std::vector<InsDeps>& inIds){
    for(auto& ins : inIds){
        std::cout << ins.toStr() << std::endl;
    }
}

inline void PrintOperations(const std::vector<InsDeps>& inIds,
                     const std::vector<std::vector<int>>& pushDetails,
                     const std::vector<std::vector<int>>& popDetails){
    for(int idxOp = 0 ; idxOp < int(inIds.size()); ++idxOp){
        auto& ins = inIds[idxOp];
        if(pushDetails[idxOp].size()){
            std::cout << "Push: ";
            for(auto& idxp : pushDetails[idxOp]){
                std::cout << idxp << " ";
            }
            std::cout << "\n";
        }
        if(popDetails[idxOp].size()){
            std::cout << "Pop: ";
            for(auto& idxp : popDetails[idxOp]){
                std::cout << idxp << " ";
            }
            std::cout << "\n";
        }
        std::cout << ins.toStr() << std::endl;
    }
}

}

#endif // MINIMIZEREGISTERS_HPP
