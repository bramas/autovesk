#include <memory>
#include <iostream>
#include <queue>
#include <utility>
#include <variant>
#include <sstream>
#include <unordered_map>
#include <queue>
#include <cassert>
#include <set>
#include <fstream>
#include <map>
#include <algorithm>
#include <functional>
#include <vector>
#include <limits>

#include "all-vec-splits.hpp"
#include "suboperations-algos.hpp"
#include "operations-order.hpp"
#include "config.hpp"
#include "global.hpp"
#include "minimize-registers.hpp"

namespace Autovec{

/////////////////////////////////////////////////
/// Utils
/////////////////////////////////////////////////

template<class T>
struct is_char_array : std::false_type {};

template<>
struct is_char_array<char[]> : std::true_type {};

template<std::size_t N>
struct is_char_array<char[N]> : std::true_type {};


// End function for the string merger
std::string StringMerge(){
    return std::string();
}

// Append inObj to the string
template <class FirstType, class ... T>
std::string StringMerge(FirstType&& inObj, T... others){
    std::ostringstream result;
    result << inObj;
    result << (StringMerge(std::forward<T>(others)...));
    return result.str();
}

// Append inObj to the string
template <class FirstType>
std::string StringMerge(FirstType&& inObj){
    std::ostringstream result;
    result << inObj;
    return result.str();
}

// Perfom the intersection of two vectors (using sets)
template <class T>
std::vector<T> VectorsIntersection(const std::vector<T>& v1,
                                            const std::vector<T>& v2){

    std::set<T> sv1;
    std::copy(v1.begin(), v1.end(), std::inserter(sv1, sv1.end()));

    std::set<T> sv2;
    std::copy(v2.begin(), v2.end(), std::inserter(sv2, sv2.end()));

    std::vector<T> v3;

    std::set_intersection(sv1.begin(),sv1.end(),
                          sv2.begin(),sv2.end(),
                          back_inserter(v3));

    return v3;
}

// Convert the content of a container into a string
template <class ContainerType>
std::string ContainerToStr(ContainerType&& container, const bool inRemoveNegative = false, const bool replaceWithMaxLong = false){
    std::string res;
    for(const auto& val : container){
        if(inRemoveNegative == false || val >= 0){
            if(res.length()){
                res.append(", ");
            }
            res.append(std::to_string(val));
        }
        else if (replaceWithMaxLong){
            if (res.length()){
                res.append(", ");
            }
            res.append(std::to_string(std::numeric_limits<unsigned long>::max()));
        }
    }
    return res;
}

template <class ContainerType>
std::string ContainerToStrRev(ContainerType&& container, const bool inRemoveNegative = false){
    std::string res;
    const auto firstIter = std::begin(container);
    auto iter = std::end(container);
    while(iter != firstIter){
        --iter;
        const auto& val = (*iter);
        if(inRemoveNegative == false || val >= 0){
            if(res.length()){
                res.append(", ");
            }
            res.append(std::to_string(val));
        }
    }
    return res;
}

/////////////////////////////////////////////////
/// Node type
/////////////////////////////////////////////////

// Graph node represent a node of the graph
// It has inputs and outputs
// It has different lists of inputs based on NbNextTypes_t
// (which must start from 0)
template <class DataType, int NbNextTypes_t>
class GraphNode{
    DataType action;
    ////!! NbNextTypes_t is the number of possible paths to arrive to the current node
    std::array<std::vector<std::shared_ptr<GraphNode>>, NbNextTypes_t> inputs;
    std::vector<std::shared_ptr<GraphNode>> outputs;
public:
    static constexpr int NbNextTypes = NbNextTypes_t;

    GraphNode(){}

    void addInput(std::shared_ptr<GraphNode>& inNode, const int inTypes = 0){
        inputs[inTypes].emplace_back(inNode);
    }
    // Append inObj to the string

    void addOutput(std::shared_ptr<GraphNode>& inNode){
        outputs.emplace_back(inNode);
    }

    void addInputs(std::vector<std::shared_ptr<GraphNode>>& inNodes, const int inTypes = 0){
        inputs[inTypes].insert(inputs[inTypes].end(), inNodes.begin(), inNodes.end());
    }

    void addOutputs(std::vector<std::shared_ptr<GraphNode>>& inNodes){
        outputs.insert(outputs.end(), inNodes.begin(), inNodes.end());
    }

    void removeInput(const std::shared_ptr<GraphNode>& inNode, const int inTypes = 0){
        inputs[inTypes].erase(std::remove(inputs[inTypes].begin(),
                                              inputs[inTypes].end(), inNode),
                                    inputs[inTypes].end());
    }

    void removeAnyInput(const std::shared_ptr<GraphNode>& inNode){
        for(int idxInput = 0 ; idxInput < NbNextTypes ; ++idxInput){
            removeInput(inNode, idxInput);
        }
    }

    void removeOutput(std::shared_ptr<GraphNode>& inNode){
        outputs.erase(std::remove(outputs.begin(),
                                              outputs.end(), inNode),
                                    outputs.end());
    }

    void removeInputs(const std::vector<std::shared_ptr<GraphNode>>& inNodes, const int inTypes = 0){
        for(auto& nd : inNodes){
            removeInput(nd, inTypes);
        }
    }

    void removeAnyInputs(const std::vector<std::shared_ptr<GraphNode>>& inNodes){
        for(int idxInput = 0 ; idxInput < NbNextTypes ; ++idxInput){
            removeInputs(inNodes, idxInput);
        }
    }

    void removeOutputs(const std::vector<std::shared_ptr<GraphNode>>& inNodes){
        for(auto& nd : inNodes){
            removeOutput(nd);
        }
    }

    bool nodeInInputs(const std::shared_ptr<GraphNode>& inNode, const int inTypes = 0){
        return std::find(inputs[inTypes].begin(), inputs[inTypes].end(), inNode)
                                           != inputs[inTypes].end();
    }

    bool nodeInAnyInputs(const std::shared_ptr<GraphNode>& inNode){
        for(int idxInput = 0 ; idxInput < NbNextTypes ; ++idxInput){
            if(nodeInInputs(inNode, idxInput)){
                return true;
            }
        }
        return false;
    }

    bool nodeInOutputs(const std::shared_ptr<GraphNode>& inNode){
        return std::find(outputs.begin(), outputs.end(), inNode)
                                           != outputs.end();
    }

    int getNbInputs(const int inTypes = 0) const {
        return int(inputs[inTypes].size());
    }

    int getNbInputsAll(const int inTypes = 0) const {
        int cpt = 0;
        for(int idxInput = 0 ; idxInput < NbNextTypes ; ++idxInput){
            cpt += int(inputs[inTypes].size());
        }
        return cpt;
    }

    int getNbOutputs() const {
        return int(outputs.size());
    }

    auto& getInputs(const int inTypes = 0) {
        return inputs[inTypes];
    }

    auto& getOutputs() {
        return outputs;
    }

    auto& getInputs(const int inTypes = 0) const {
        return inputs[inTypes];
    }

    auto& getOutputs() const {
        return outputs;
    }

    DataType& getData(){
        return action;
    }

    void setData(const DataType& dataType){
        action = dataType;
    }

    const DataType& getData() const {
        return action;
    }

    bool isRoot() const{
        return outputs.size() == 0;
    }

    bool isLeaf() const{
        for(auto& input : inputs){
            if(input.size()){
                return false;
            }
        }
        return true;
    }

    // Connect two nodes
    static void Connect(std::shared_ptr<GraphNode>& inOutput,
                        std::shared_ptr<GraphNode>& inInput,
                        const int inTypes = 0){
        if(!inOutput->nodeInInputs(inInput, inTypes)){
            inOutput->addInput(inInput, inTypes);
        }
        // else already in inputs
        
        if(!inInput->nodeInOutputs(inOutput)){
            inInput->addOutput(inOutput);
        }
        // else already in outputs

        //assert(!inOutput->nodeInInputs(inInput, inTypes));
        //assert(!inInput->nodeInOutputs(inOutput));
    }

    // Disconnect two nodes
    static void Disconnect(std::shared_ptr<GraphNode>& inOutput,
                        std::shared_ptr<GraphNode>& inInput,
                        const int inTypes = 0){
        if(inOutput->nodeInInputs(inInput, inTypes)){
            inOutput->removeInput(inInput, inTypes);
        }
        if(inInput->nodeInOutputs(inOutput)){
            inInput->removeOutput(inOutput);
        }
    }
};

/////////////////////////////////////////////////
/// An instruction
/////////////////////////////////////////////////

// An instruction can be a scalar instruction
// or a vectorial instruction
struct Instruction{
    static std::string ValueToString(const std::variant<double, float, int, long int>& inValue){
        if(inValue.index() == 0) return "(double)" + std::to_string(std::get<0>(inValue));
        if(inValue.index() == 1) return "(float)" + std::to_string(std::get<1>(inValue));
        if(inValue.index() == 2) return "(int)" + std::to_string(std::get<2>(inValue));
        if(inValue.index() == 3) return "(long int)" + std::to_string(std::get<3>(inValue));
        return "unknown";
    }

    enum ActionType{
        LoadType = 0,
        SetType = 1,
        StoreType = 2,
        OperationType = 3
    };

    /////////////////////////////////////////////
    /// Scalar type
    /////////////////////////////////////////////

    struct ScalarLoad{
        std::ptrdiff_t ptr;
        std::ptrdiff_t offset;

        std::variant<double, float, int, long int> value;
    };


    struct ScalarSet{
        std::variant<double, float, int, long int> value;
    };

    struct ScalarStore{
        std::ptrdiff_t ptr;
        std::ptrdiff_t offset;
    };

    struct ScalarOperation{
        std::string operation;

        bool isCommutative() const{
            return operation == "+" || operation == "*";
        }
    };

    struct ScalarData {
        std::string type;
        std::variant<ScalarLoad, ScalarSet, ScalarStore, ScalarOperation> data;

        bool isLoad() const{
            return data.index() == ActionType::LoadType;
        }

        bool isSet() const{
            return data.index() == ActionType::SetType;
        }

        bool isLoadOrSet() const{
            return isLoad() || isSet();
        }

        bool isStore() const{
            return data.index() == ActionType::StoreType;
        }

        bool isOperation() const{
            return data.index() == ActionType::OperationType;
        }

        const auto& getLoad() const{
            assert(isLoad());
            return std::get<ActionType::LoadType>(data);
        }

        const auto& getSet() const{
            assert(isSet());
            return std::get<ActionType::SetType>(data);
        }

        const auto& getStore() const{
            assert(isStore());
            return std::get<ActionType::StoreType>(data);
        }

        const auto& getOperation() const{
            assert(isOperation());
            return std::get<ActionType::OperationType>(data);
        }
    };

    /////////////////////////////////////////////
    /// Vector type
    /////////////////////////////////////////////

    struct VectorialLoad{
        std::ptrdiff_t ptr;
        std::vector<std::ptrdiff_t> offset;

        std::variant<double, float, int, long int> value;
    };

    struct VectorialSet{
        std::variant<double, float, int, long int> value;
    };

    struct VectorialStore{
        std::ptrdiff_t ptr;
        std::vector<std::ptrdiff_t> offset;
    };

    struct VectorialOperation{
        std::string operation;
        bool useIndices;
        std::vector<std::ptrdiff_t> indices;
        size_t reductionCount;
    };

    struct VectorialData {
        std::string type;
        std::variant<VectorialLoad, VectorialSet, VectorialStore, VectorialOperation> data;

        bool isLoad() const{
            return data.index() == ActionType::LoadType;
        }

        bool isSet() const{
            return data.index() == ActionType::SetType;
        }

        bool isLoadOrSet() const{
            return isLoad() || isSet();
        }

        bool isStore() const{
            return data.index() == ActionType::StoreType;
        }

        bool isOperation() const{
            return data.index() == ActionType::OperationType;;
        }

        const auto& getLoad() const{
            assert(isLoad());
            return std::get<ActionType::LoadType>(data);
        }

        const auto& getSet() const{
            assert(isSet());
            return std::get<ActionType::SetType>(data);
        }

        const auto& getStore() const{
            assert(isStore());
            return std::get<ActionType::StoreType>(data);
        }

        const auto& getOperation() const{
            assert(isOperation());
            return std::get<ActionType::OperationType>(data);
        }
    };



    /////////////////////////////////////////////
    /// Core part
    /////////////////////////////////////////////


    enum InstructionType{
        ScalarType = 0,
        VectorialType = 1
    };

    std::variant<ScalarData, VectorialData> data;

    ////////////////////////////////////////////////////////

    bool isScalar() const{
        return data.index() == InstructionType::ScalarType;
    }

    bool isVectorial() const{
        return data.index() == InstructionType::VectorialType;
    }

    bool isScalarLoad() const{
        return data.index() == InstructionType::ScalarType
                && std::get<InstructionType::ScalarType>(data).data.index() == ActionType::LoadType;
    }

    bool isScalarSet() const{
        return data.index() == InstructionType::ScalarType
                && std::get<InstructionType::ScalarType>(data).data.index() == ActionType::SetType;
    }

    bool isScalarLoadOrSet() const{
        return isScalarLoad() || isScalarSet();
    }

    bool isScalarStore() const{
        return data.index() == InstructionType::ScalarType
                && std::get<InstructionType::ScalarType>(data).data.index() == ActionType::StoreType;
    }

    bool isScalarOperation() const{
        return data.index() == InstructionType::ScalarType
                && std::get<InstructionType::ScalarType>(data).data.index() == ActionType::OperationType;
    }

    bool isVectorialLoad() const{
        return data.index() == InstructionType::VectorialType
                && std::get<InstructionType::VectorialType>(data).data.index() == ActionType::LoadType;
    }

    bool isVectorialSet() const{
        return data.index() == InstructionType::VectorialType
                && std::get<InstructionType::VectorialType>(data).data.index() == ActionType::SetType;
    }

    bool isVectorialLoadOrSet() const{
        return isVectorialLoad() || isVectorialSet();
    }

    bool isVectorialStore() const{
        return data.index() == InstructionType::VectorialType
                && std::get<InstructionType::VectorialType>(data).data.index() == ActionType::StoreType;
    }

    bool isVectorialOperation() const{
        return data.index() == InstructionType::VectorialType
                && std::get<InstructionType::VectorialType>(data).data.index() == ActionType::OperationType;
    }

    bool isLoad() const{
        return isScalarLoad() || isVectorialLoad();
    }

    bool isSet() const{
        return isScalarSet() || isVectorialSet();
    }

    bool isLoadOrSet() const{
        return isScalarLoadOrSet() || isVectorialLoadOrSet();
    }

    bool isStore() const{
        return isScalarStore() || isVectorialStore();
    }

    bool isOperation() const{
        return isScalarOperation() || isVectorialOperation();
    }

    const auto& getScalar() const{
        assert(isScalar());
        return std::get<InstructionType::ScalarType>(data);
    }

    const auto& getVectorial() const{
        assert(isVectorial());
        return std::get<InstructionType::VectorialType>(data);
    }

    std::string getDescription(const bool lightDescription = false) const{
        if(lightDescription){
            if(data.index() == InstructionType::ScalarType){
                const auto& scalar = std::get<InstructionType::ScalarType>(data);
                if(scalar.data.index() == ActionType::LoadType){
                    const auto& load = std::get<ActionType::LoadType>(scalar.data);
                    return StringMerge("Load @", load.ptr, "[", load.offset, "]");
                }
                if(scalar.data.index() == ActionType::SetType){
                    const auto& set = std::get<ActionType::SetType>(scalar.data);
                    return StringMerge("Set ", Instruction::ValueToString(set.value));
                }
                if(scalar.data.index() == ActionType::StoreType){
                    const auto& store = std::get<ActionType::StoreType>(scalar.data);
                    return StringMerge("Store @", store.ptr, "[", store.offset, "]");
                }
                if(scalar.data.index() == ActionType::OperationType){
                    const auto& operation = std::get<ActionType::OperationType>(scalar.data);
                    return StringMerge("", operation.operation);
                }
                return "Undefined scalar";
            }
            if(data.index() == InstructionType::VectorialType){
                const auto& vector = std::get<InstructionType::VectorialType>(data);
                if(vector.data.index() == ActionType::LoadType){
                    const auto& load = std::get<ActionType::LoadType>(vector.data);
                    return StringMerge("Load @", load.ptr, "[", ContainerToStr(load.offset), "]");
                }
                if(vector.data.index() == ActionType::SetType){
                    const auto& set = std::get<ActionType::SetType>(vector.data);
                    return StringMerge("Set ", Instruction::ValueToString(set.value));
                }
                if(vector.data.index() == ActionType::StoreType){
                    const auto& store = std::get<ActionType::StoreType>(vector.data);
                    return StringMerge("Store @", store.ptr, "[", ContainerToStr(store.offset), "]");
                }
                if(vector.data.index() == ActionType::OperationType){
                    const auto& operation = std::get<ActionType::OperationType>(vector.data);
                    return StringMerge("", operation.operation);
                }
                return "Undefined vector";
            }
        }
        else{
            if(data.index() == InstructionType::ScalarType){
                const auto& scalar = std::get<InstructionType::ScalarType>(data);
                if(scalar.data.index() == ActionType::LoadType){
                    const auto& load = std::get<ActionType::LoadType>(scalar.data);
                    return StringMerge("Scalar load at ", load.ptr, "[", load.offset, "]");
                }
                if(scalar.data.index() == ActionType::SetType){
                    const auto& set = std::get<ActionType::SetType>(scalar.data);
                    return StringMerge("Scalar set value ", Instruction::ValueToString(set.value));
                }
                if(scalar.data.index() == ActionType::StoreType){
                    const auto& store = std::get<ActionType::StoreType>(scalar.data);
                    return StringMerge("Scalar store at ", store.ptr, "[", store.offset, "]");
                }
                if(scalar.data.index() == ActionType::OperationType){
                    const auto& operation = std::get<ActionType::OperationType>(scalar.data);
                    return StringMerge("Scalar operation ", operation.operation);
                }
                return "Undefined scalar";
            }
            if(data.index() == InstructionType::VectorialType){
                const auto& vector = std::get<InstructionType::VectorialType>(data);
                if(vector.data.index() == ActionType::LoadType){
                    const auto& load = std::get<ActionType::LoadType>(vector.data);
                    return StringMerge("Vectorial load at ", load.ptr, "[", ContainerToStr(load.offset), "]");
                }
                if(vector.data.index() == ActionType::SetType){
                    const auto& set = std::get<ActionType::SetType>(vector.data);
                    return StringMerge("Vectorial set value ", Instruction::ValueToString(set.value));
                }
                if(vector.data.index() == ActionType::StoreType){
                    const auto& store = std::get<ActionType::StoreType>(vector.data);
                    return StringMerge("Vectorial store at ", store.ptr, "[", ContainerToStr(store.offset), "]");
                }
                if(vector.data.index() == ActionType::OperationType){
                    const auto& operation = std::get<ActionType::OperationType>(vector.data);
                    return StringMerge("Vectorial operation ", operation.operation);
                }
                return "Undefined vector";
            }
        }
        return "Action undefined";
    }

    /////////////////////////////////////////////
    /// Creation part
    /////////////////////////////////////////////

    static std::shared_ptr<Instruction> make_scalar_load(std::string inType, const std::ptrdiff_t inPtr, const std::ptrdiff_t inOffset){
        ScalarLoad load;
        load.ptr = inPtr;
        load.offset = inOffset;

        ScalarData data;
        data.data = std::move(load);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    template <class NumType>
    static std::shared_ptr<Instruction> make_scalar_set(std::string inType, const NumType inValue){
        ScalarSet set;
        set.value = inValue;

        ScalarData data;
        data.data = std::move(set);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }


    static std::shared_ptr<Instruction> make_scalar_store(std::string inType, const std::ptrdiff_t inPtr, const std::ptrdiff_t inOffset){
        ScalarStore store;
        store.ptr = inPtr;
        store.offset = inOffset;

        ScalarData data;
        data.data = store;
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    static std::shared_ptr<Instruction> make_scalar_operation(std::string inType, std::string inOperation){
        ScalarOperation operation;
        operation.operation = std::move(inOperation);

        ScalarData data;
        data.data = std::move(operation);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    static std::shared_ptr<Instruction> make_vectorial_load(std::string inType, const std::ptrdiff_t inPtr, const std::vector<std::ptrdiff_t>& inOffset){
        VectorialLoad load;
        load.ptr = inPtr;
        load.offset = inOffset;

        VectorialData data;
        data.data = std::move(load);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    template <class NumType>
    static std::shared_ptr<Instruction> make_vectorial_set(std::string inType, const NumType inValue){
        VectorialSet set;
        set.value = inValue;

        VectorialData data;
        data.data = std::move(set);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }


    static std::shared_ptr<Instruction> make_vectorial_store(std::string inType, const std::ptrdiff_t inPtr, const std::vector<std::ptrdiff_t>& inOffset){
        VectorialStore store;
        store.ptr = inPtr;
        store.offset = inOffset;

        VectorialData data;
        data.data = store;
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    static std::shared_ptr<Instruction> make_vectorial_operation(std::string inType, std::string inOperation){
        VectorialOperation operation;
        operation.operation = std::move(inOperation);
        operation.useIndices = false;

        VectorialData data;
        data.data = std::move(operation);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    static std::shared_ptr<Instruction> make_vectorial_operation_with_indices(std::string inType, std::string inOperation,
                                                                              const std::vector<std::ptrdiff_t>& inIndices){
        VectorialOperation operation;
        operation.operation = std::move(inOperation);
        operation.useIndices = true;
        operation.indices = inIndices;

        VectorialData data;
        data.data = std::move(operation);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }

    static std::shared_ptr<Instruction> make_vectorial_reduction_operation(std::string inType, std::string inOperation,
                                                                            size_t reductionCount){
        assert(inOperation.rfind("Reduction ", 0) == 0);
        
        VectorialOperation operation;
        operation.operation = std::move(inOperation);
        operation.useIndices = false;
        operation.reductionCount = reductionCount;

        VectorialData data;
        data.data = std::move(operation);
        data.type = std::move(inType);

        std::shared_ptr<Instruction> instruction(new Instruction);
        instruction->data = std::move(data);
        return instruction;
    }
};

///!! 2 is the number of sources "a*b" (a and b are distinct)
using Node = GraphNode<std::shared_ptr<Instruction>, 2>;

/////////////////////////////////////////////
/// Group type
/////////////////////////////////////////////

// A GroupInstruction is a vector a Node
// it is an helper class to manage several
// instructions together (usually scalar instructions
// that will be converted into a vectorial one)
class GroupInstruction{
public:
    enum class GroupType{
        GroupOfLoads,
        GroupOfSets,
        GroupOfStores,
        GroupOfOperations
    };

    std::vector<std::shared_ptr<Node>> nodes;
    GroupType type;

    bool isGroupLoad() const{
        return type == GroupType::GroupOfLoads;
    }

    bool isGroupSet() const{
        return type == GroupType::GroupOfSets;
    }

    bool isGroupLoadOrSet() const{
        return isGroupLoad() || isGroupSet();
    }

    bool isGroupStore() const{
        return type == GroupType::GroupOfStores;
    }

    bool isGroupOperation() const{
        return type == GroupType::GroupOfOperations;
    }

    std::string getDescription() const{
        if(isGroupLoad()){
            //const auto& load = std::get<0>(group.data);
            return StringMerge("Group load");
        }
        if(isGroupSet()){
            //const auto& load = std::get<1>(group.data);
            return StringMerge("Group set");
        }
        if(isGroupStore()){
            //const auto& store = std::get<2>(group.data);
            return StringMerge("Group store");
        }
        if(isGroupOperation()){
            //const auto& operation = std::get<3>(group.data);
            return StringMerge("Group operation");
        }
        return "Undefined group";
    }

    void addNode(const std::shared_ptr<Node>& inNode){
        nodes.emplace_back(inNode);
    }

    void removeNode(const std::shared_ptr<Node>& inNode){
        assert(hasNode(inNode));
        nodes.erase(std::remove(nodes.begin(),
                                nodes.end(), inNode),
                        nodes.end());
    }

    bool hasNode(const std::shared_ptr<Node>& inNode){
        return std::find(nodes.cbegin(), nodes.cend(), inNode) != nodes.cend();
    }

    static std::shared_ptr<GroupInstruction> make_group_load(std::vector<std::shared_ptr<Node>> inScalar){
        std::shared_ptr<GroupInstruction> instruction(new GroupInstruction);
        instruction->nodes = std::move(inScalar);
        instruction->type = GroupType::GroupOfLoads;
        return instruction;
    }

    static std::shared_ptr<GroupInstruction> make_group_set(std::vector<std::shared_ptr<Node>> inScalar){
        std::shared_ptr<GroupInstruction> instruction(new GroupInstruction);
        instruction->nodes = std::move(inScalar);
        instruction->type = GroupType::GroupOfSets;
        return instruction;
    }

    static std::shared_ptr<GroupInstruction> make_group_store(std::vector<std::shared_ptr<Node>> inScalar){
        std::shared_ptr<GroupInstruction> instruction(new GroupInstruction);
        instruction->nodes = std::move(inScalar);
        instruction->type = GroupType::GroupOfStores;
        return instruction;
    }

    static std::shared_ptr<GroupInstruction> make_group_operation(std::vector<std::shared_ptr<Node>> inScalar){
        std::shared_ptr<GroupInstruction> instruction(new GroupInstruction);
        instruction->nodes = std::move(inScalar);
        instruction->type = GroupType::GroupOfOperations;
        return instruction;
    }

    static std::shared_ptr<GroupInstruction> make_group_selects(std::vector<std::shared_ptr<Node>> inScalar){
        std::shared_ptr<GroupInstruction> instruction(new GroupInstruction);
        instruction->nodes = std::move(inScalar);
        instruction->type = GroupType::GroupOfOperations;
        return instruction;
    }
};

using GroupNode = GraphNode<std::shared_ptr<GroupInstruction>, 2>;

///!! This part is related to the generation of the graph
///!! Should not change (instead we cannot generate something
///!! in the scalar graph).


/////////////////////////////////////////////////
/// Mask type
/////////////////////////////////////////////////

// A mask type is a class that should be used
// for building the AST (so it is not directly
// used by the vectorizer engine but to explore
// the user code)
class ScalarTypeMask {
public:
    // Classic constructors
    inline ScalarTypeMask(){
        this->node.reset(new Node);
    }

    inline ScalarTypeMask(const ScalarTypeMask& inOther){
        this->node = inOther.node;
    }

    inline ScalarTypeMask& operator=(const ScalarTypeMask& inOther){
        this->node = inOther.node;
        return *this;
    }

    inline explicit operator bool() const{
        return false;
    }

    // Binary methods
    inline ScalarTypeMask Not() const{
        ScalarTypeMask result;
        result.node->getData() = (Instruction::make_scalar_operation("bool", "!"));
        Node::Connect(result.node, (*this).node, 0);
        return result;
    }

    mutable std::shared_ptr<Node> node;
};

// Mask must have operators
inline ScalarTypeMask operator&(const ScalarTypeMask& inMask1, const ScalarTypeMask& inMask2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "&"));
    Node::Connect(result.node, inMask1.node, 0);
    Node::Connect(result.node, inMask2.node, 1);
    return result;
}

inline ScalarTypeMask operator|(const ScalarTypeMask& inMask1, const ScalarTypeMask& inMask2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "|"));
    Node::Connect(result.node, inMask1.node, 0);
    Node::Connect(result.node, inMask2.node, 1);
    return result;
}

inline ScalarTypeMask operator^(const ScalarTypeMask& inMask1, const ScalarTypeMask& inMask2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "^"));
    Node::Connect(result.node, inMask1.node, 0);
    Node::Connect(result.node, inMask2.node, 1);
    return result;
}

inline ScalarTypeMask operator==(const ScalarTypeMask& inMask1, const ScalarTypeMask& inMask2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "=="));
    Node::Connect(result.node, inMask1.node, 0);
    Node::Connect(result.node, inMask2.node, 1);
    return result;
}

inline ScalarTypeMask operator!=(const ScalarTypeMask& inMask1, const ScalarTypeMask& inMask2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "!="));
    Node::Connect(result.node, inMask1.node, 0);
    Node::Connect(result.node, inMask2.node, 1);
    return result;
}

/////////////////////////////////////////////////
/// Scalar type
/////////////////////////////////////////////////

// A scalar type is a class that should be used
// for building the AST (so it is not directly
// used by the vectorizer engine but to explore
// the user code)
class ScalarType {
protected:
public:
    using MaskType             = ScalarTypeMask;

    inline ScalarType(){
        this->node.reset(new Node);
    }

    inline explicit ScalarType(const float inValue){
        std::shared_ptr<Node> newNode(new Node);
        newNode->getData() = (Instruction::make_scalar_set("double", inValue));
        node = newNode;
    }

    inline ScalarType(const ScalarType& inOther){
        this->node = inOther.node;
    }

    inline ScalarType& operator = (const ScalarType& inOther){
        this->node = inOther.node;
        return *this;
    }

    inline ScalarType& operator = (const float inValue){
        std::shared_ptr<Node> newNode(new Node);
        newNode->getData() = (Instruction::make_scalar_set("double", inValue));
        node = newNode;
        return *this;
    }

    inline explicit operator float() const{
        return 0;
    }

    // Inner operators
    inline ScalarType& operator+=(const ScalarType& inVec){
        std::shared_ptr<Node> newNode(new Node);
        assert((*this).node && (*this).node->getData());
        newNode->getData() = (Instruction::make_scalar_operation(std::get<0>((*this).node->getData()->data).type, "+"));
        Node::Connect(newNode, node, 0);
        Node::Connect(newNode, inVec.node, 1);
        node = newNode;
        return *this;
    }

    inline ScalarType& operator-=(const ScalarType& inVec){
        std::shared_ptr<Node> newNode(new Node);
        assert((*this).node && (*this).node->getData());
        newNode->getData() = (Instruction::make_scalar_operation(std::get<0>((*this).node->getData()->data).type, "-"));
        Node::Connect(newNode, node, 0);
        Node::Connect(newNode, inVec.node, 1);
        node = newNode;
        return *this;
    }

    inline ScalarType& operator/=(const ScalarType& inVec){
        std::shared_ptr<Node> newNode(new Node);
        assert((*this).node && (*this).node->getData());
        newNode->getData() = (Instruction::make_scalar_operation(std::get<0>((*this).node->getData()->data).type, "/"));
        Node::Connect(newNode, node, 0);
        Node::Connect(newNode, inVec.node, 1);
        node = newNode;
        return *this;
    }

    inline ScalarType& operator*=(const ScalarType& inVec){
        std::shared_ptr<Node> newNode(new Node);
        assert((*this).node && (*this).node->getData());
        newNode->getData() = (Instruction::make_scalar_operation(std::get<0>((*this).node->getData()->data).type, "*"));
        Node::Connect(newNode, node, 0);
        Node::Connect(newNode, inVec.node, 1);
        node = newNode;
        return *this;
    }

    inline ScalarType operator-() const {
        ScalarType result;
        assert((*this).node && (*this).node->getData());
        result.node->getData() = (Instruction::make_scalar_operation(std::get<0>((*this).node->getData()->data).type, "-"));
        Node::Connect(result.node, node);
        return result;
    }

    mutable std::shared_ptr<Node> node;
};

// Bits operators
inline ScalarType operator&(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "&"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarType operator|(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "|"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarType operator^(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "^"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

// Dual operators
inline ScalarType operator+(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "+"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarType operator-(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "-"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarType operator/(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "/"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarType operator*(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarType result;
    assert(inVec1.node && inVec1.node->getData());
    result.node->getData() = (Instruction::make_scalar_operation(std::get<0>(inVec1.node->getData()->data).type, "*"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

// Tests and comparions
inline ScalarTypeMask operator<(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "<"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarTypeMask operator<=(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "<="));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarTypeMask operator>(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", ">"));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarTypeMask operator>=(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", ">="));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarTypeMask operator==(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "=="));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

inline ScalarTypeMask operator!=(const ScalarType& inVec1, const ScalarType& inVec2){
    ScalarTypeMask result;
    result.node->getData() = (Instruction::make_scalar_operation("bool", "!="));
    Node::Connect(result.node, inVec1.node, 0);
    Node::Connect(result.node, inVec2.node, 1);
    return result;
}

/////////////////////////////////////////////////
/// Vectorization
/////////////////////////////////////////////////


auto TestIfListConnectedToNode(const std::vector<std::shared_ptr<Node>>& inNodesOfGroup, const std::shared_ptr<Node>& inNodeToTest, const int inInputIdx) {
    for(const auto& iter : inNodesOfGroup){
        if(iter->nodeInInputs(inNodeToTest, inInputIdx)){
            assert(inNodeToTest->nodeInOutputs(iter));
            return true;
        }
    }
    return false;
}

auto TestIfListsConnected(const std::vector<std::shared_ptr<Node>>& inOutputList,
                          const std::vector<std::shared_ptr<Node>>& inInputList,
                          const int inInputIdx) {
    for(const auto& input : inInputList){
        if(TestIfListConnectedToNode(inOutputList, input, inInputIdx)){
            return true;
        }
    }
    return false;
}

std::vector<std::shared_ptr<Node>> GetInputs(const std::vector<std::shared_ptr<Node>>& outputs){
    std::vector<std::shared_ptr<Node>> inputs;

    std::set<const Node*> alreadyInAllNodes;

    std::queue<std::shared_ptr<Node>> nodesToProceed;
    for(auto& node : outputs){
        nodesToProceed.push(node);
    }

    while(nodesToProceed.size()){
        std::shared_ptr<Node> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        if(alreadyInAllNodes.find(currentNode.get()) == alreadyInAllNodes.end()){
            alreadyInAllNodes.insert(currentNode.get());

            if(currentNode->getNbInputs(0)){
                assert(currentNode->getNbInputs(0) == 1);
                nodesToProceed.push(currentNode->getInputs(0).front());
                if(currentNode->getNbInputs(1)){
                    assert(currentNode->getNbInputs(1) == 1);
                    nodesToProceed.push(currentNode->getInputs(1).front());
                }
            }
            else{
                assert(currentNode->getNbInputs(1) == 0);
            }

            if(currentNode->isLeaf()){
                inputs.emplace_back(currentNode);
            }
        }
    }
    return inputs;
}

std::vector<std::shared_ptr<Node>> GetAllNodes(const std::vector<std::shared_ptr<Node>>& outputs){
    std::vector<std::shared_ptr<Node>> allNodes;

    std::set<const Node*> alreadyInAllNodes;

    std::queue<std::shared_ptr<Node>> nodesToProceed;
    for(auto& node : outputs){
        nodesToProceed.push(node);
    }

    while(nodesToProceed.size()){
        std::shared_ptr<Node> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        if(alreadyInAllNodes.find(currentNode.get()) == alreadyInAllNodes.end()){
            alreadyInAllNodes.insert(currentNode.get());
            allNodes.emplace_back(currentNode);

            if(currentNode->getNbInputs(0)){
                assert(currentNode->getNbInputs(0) == 1);
                nodesToProceed.push(currentNode->getInputs(0).front());
                if(currentNode->getNbInputs(1)){
                    assert(currentNode->getNbInputs(1) == 1);
                    nodesToProceed.push(currentNode->getInputs(1).front());
                }
            }
            else{
                assert(currentNode->getNbInputs(1) == 0);
            }
        }
    }
    return allNodes;
}

std::vector<std::shared_ptr<GroupNode>> GetAllNodes(const std::vector<std::shared_ptr<GroupNode>>& outputs){
    std::vector<std::shared_ptr<GroupNode>> allNodes;

    std::set<const GroupNode*> alreadyInAllNodes;

    std::queue<std::shared_ptr<GroupNode>> nodesToProceed;
    for(auto& node : outputs){
        nodesToProceed.push(node);
    }

    while(nodesToProceed.size()){
        std::shared_ptr<GroupNode> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        if(alreadyInAllNodes.find(currentNode.get()) == alreadyInAllNodes.end()){
            alreadyInAllNodes.insert(currentNode.get());
            allNodes.emplace_back(currentNode);

            if(currentNode->getNbInputs(0)){
                for(auto& input : currentNode->getInputs(0)){
                    nodesToProceed.push(input);
                }
                for(auto& input : currentNode->getInputs(1)){
                    nodesToProceed.push(input);
                }
            }
            else{
                assert(currentNode->getNbInputs(1) == 0);
            }
        }
    }
    return allNodes;
}

std::vector<std::shared_ptr<GroupNode>> GetAllNodesFromInputs(const std::vector<std::shared_ptr<GroupNode>>& inputs){
    std::vector<std::shared_ptr<GroupNode>> allNodes;

    std::set<const GroupNode*> alreadyInAllNodes;

    std::queue<std::shared_ptr<GroupNode>> nodesToProceed;
    for(auto& node : inputs){
        nodesToProceed.push(node);
    }

    while(nodesToProceed.size()){
        std::shared_ptr<GroupNode> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        if(alreadyInAllNodes.find(currentNode.get()) == alreadyInAllNodes.end()){
            alreadyInAllNodes.insert(currentNode.get());
            allNodes.emplace_back(currentNode);

            for(auto& output : currentNode->getOutputs()){
                nodesToProceed.push(output);
            }
        }
    }
    return allNodes;
}

int GetNbNodes(const std::vector<std::shared_ptr<Node>>& outputs){
    return int(GetAllNodes(outputs).size());
}

std::vector<std::shared_ptr<Node>> DuplicateGraph(const std::vector<std::shared_ptr<Node>>& outputs){
    std::vector<std::shared_ptr<Node>> allNodes = GetAllNodes(outputs);

    std::vector<std::shared_ptr<Node>> newOutputs;

    std::unordered_map<const Node*, std::shared_ptr<Node>> copies;
    for(const auto & node : allNodes){
        std::shared_ptr<Node> newNode(new Node);
        newNode->getData() = node->getData();
        copies[node.get()] = newNode;

        if(node->isRoot())
            newOutputs.emplace_back(newNode);
    }

    for(const auto & node : allNodes){
        for(int idxInput = 0; idxInput < 2; ++idxInput){
            for(const auto& input : node->getInputs(idxInput)){
                Node::Connect(copies[node.get()], copies[input.get()], idxInput);
            }
        }
    }

    return newOutputs;
}

std::unordered_map<const Node*, int> GetMaxDistanceFromOutputs(const std::vector<std::shared_ptr<Node>>& outputs){
    std::unordered_map<const Node*, int> maxDistanceFromOutputs;

    std::set<const Node*> visited;

    std::queue<std::shared_ptr<Node>> nodesToProceed;
    for(auto& node : outputs){
        nodesToProceed.push(node);
        assert(node->isRoot());
        maxDistanceFromOutputs[node.get()] = 0;
    }

    while(nodesToProceed.size()){
        std::shared_ptr<Node> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        visited.insert(currentNode.get());

        if(currentNode->getNbInputs(0)){
            assert(currentNode->getNbInputs(0) == 1);
            if(/*visited.find(currentNode->getInputs(0).front().get()) == visited.end()
                    &&*/ maxDistanceFromOutputs[currentNode.get()] + 1 > maxDistanceFromOutputs[currentNode->getInputs(0).front().get()]){
                maxDistanceFromOutputs[currentNode->getInputs(0).front().get()] = maxDistanceFromOutputs[currentNode.get()] + 1;
                nodesToProceed.push(currentNode->getInputs(0).front());
            }
            if(currentNode->getNbInputs(1)){
                assert(currentNode->getNbInputs(1) == 1);
                if(/*visited.find(currentNode->getInputs(1).front().get()) == visited.end()
                        &&*/ maxDistanceFromOutputs[currentNode.get()] + 1 > maxDistanceFromOutputs[currentNode->getInputs(1).front().get()]){
                    maxDistanceFromOutputs[currentNode->getInputs(1).front().get()] = maxDistanceFromOutputs[currentNode.get()] + 1;
                    nodesToProceed.push(currentNode->getInputs(1).front());
                }
            }
        }
        else{
            assert(currentNode->getNbInputs(1) == 0);
        }
    }
    assert(visited.size() == maxDistanceFromOutputs.size());

    return maxDistanceFromOutputs;
}

std::unordered_map<const Node*, int> GetMaxDistanceFromInputs(const std::vector<std::shared_ptr<Node>>& outputs){
    std::unordered_map<const Node*, int> distanceFromInputs;

    std::vector<std::shared_ptr<Node>> inputs = GetInputs(outputs);

    std::set<const Node*> visited;

    std::queue<std::shared_ptr<Node>> nodesToProceed;
    for(auto& node : inputs){
        assert(node->getNbInputs(0) == 0);
        assert(node->getNbInputs(1) == 0);
        nodesToProceed.push(node);
    }

    while(nodesToProceed.size()){
        std::shared_ptr<Node> currentNode = nodesToProceed.front();
        nodesToProceed.pop();

        visited.insert(currentNode.get());

        for(auto& output : currentNode->getOutputs()){
            if(/*visited.find(output.get()) == visited.end()
                    &&*/ distanceFromInputs[currentNode.get()] + 1 > distanceFromInputs[output.get()]){
                distanceFromInputs[output.get()] = distanceFromInputs[currentNode.get()] + 1;
                nodesToProceed.push(output);
            }
        }
    }
    return distanceFromInputs;
}

std::unordered_map<const GroupNode*, int> GetMaxDistanceFromInputsGroups(const std::vector<std::shared_ptr<GroupNode>>& inputs){
    std::unordered_map<const GroupNode*, int> distanceFromInputs;

    std::set<const GroupNode*> visited;

    std::queue<std::shared_ptr<GroupNode>> groupNodesToProceed;
    for(auto& groupNode : inputs){
        assert(groupNode->getNbInputs(0) == 0);
        assert(groupNode->getNbInputs(1) == 0);
        groupNodesToProceed.push(groupNode);
    }

    while(!groupNodesToProceed.empty()){
        std::shared_ptr<GroupNode> currentGroupNode = groupNodesToProceed.front();
        groupNodesToProceed.pop();

        visited.insert(currentGroupNode.get());

        for(auto& output : currentGroupNode->getOutputs()){
            if(/*visited.find(output.get()) == visited.end()
                    &&*/ distanceFromInputs[currentGroupNode.get()] + 1 > distanceFromInputs[output.get()]){
                distanceFromInputs[output.get()] = distanceFromInputs[currentGroupNode.get()] + 1;
                groupNodesToProceed.push(output);
            }
        }
    }
    return distanceFromInputs;
}

bool CompareSetValues(std::variant<double, float, int, long int> value, int expected){
    if(value.index() == 0) return std::get<0>(value) == (double) expected;
    if(value.index() == 1) return std::get<1>(value) == (float) expected;
    if(value.index() == 2) return std::get<2>(value) == expected;
    if(value.index() == 3) return std::get<3>(value) == (long int) expected;

    return false;
}

std::tuple<std::vector<MinimizeRegisters::InsDeps>, std::unordered_map<Node*, int>>
        ConvertNodesToOpsWithDetails(const std::vector<std::shared_ptr<Node>>& allNodes){
    auto IsContiguous = [](const auto& vec) -> bool{
        for(int idx = 1; idx < int(std::size(vec)) ; ++idx){
            if(vec[idx-1]+1 != vec[idx]){
                return false;
            }
        }
        return true;
    };

    std::vector<MinimizeRegisters::InsDeps> ops;

    std::unordered_map<Node*, int> nodeToNameMapping;

    for(auto currentNode : allNodes){
        assert(currentNode->getData());
        MinimizeRegisters::InsDeps deps;

        if(currentNode->getData()->isStore()){
            assert(currentNode->getOutputs().size() == 0);
            deps.isStore = true;
        }
        else{
            nodeToNameMapping[currentNode.get()] = int(nodeToNameMapping.size());
            deps.write.push_back(nodeToNameMapping[currentNode.get()]);
            deps.isStore = false;
        }


        if((currentNode->getData()->isVectorialLoad() && !IsContiguous(currentNode->getData()->getVectorial().getLoad().offset))
                || (currentNode->getData()->isVectorialStore() && !IsContiguous(currentNode->getData()->getVectorial().getStore().offset))
                    || (currentNode->getData()->isVectorialOperation() && currentNode->getData()->getVectorial().getOperation().indices.size())
                || (currentNode->getData()->isVectorialOperation() && currentNode->getData()->getVectorial().getOperation().operation.rfind("Reduction ", 0) == 0)){
            deps.nbTmpRegisters += 1;
        }

        if(currentNode->getInputs(0).size()){
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            deps.read.push_back(nodeToNameMapping[currentNode->getInputs(0).front().get()]);
        }
        if(currentNode->getInputs(1).size()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
            deps.read.push_back(nodeToNameMapping[currentNode->getInputs(1).front().get()]);
        }

        ops.push_back(std::move(deps));
    }

    return std::make_tuple(std::move(ops), std::move(nodeToNameMapping));
}

std::vector<MinimizeRegisters::InsDeps> ConvertNodesToOps(const std::vector<std::shared_ptr<Node>>& allNodes){
    auto withDetails = ConvertNodesToOpsWithDetails(allNodes);
    return std::get<0>(withDetails);
}

template <class T1, class T2>
std::unordered_map<T2,T1> ReverseMap(const std::unordered_map<T1,T2>& inMap){
    std::unordered_map<T2,T1> res;
    for(auto kv : inMap){
        assert(res.find(kv.second) == res.end());
        res[kv.second] = kv.first;
    }
    return res;
}

namespace GraphTransformation{

    void InitGroups(const std::vector<std::shared_ptr<Node>>& outputs,
                std::vector<std::shared_ptr<GroupNode>> *inLoadGroups,
                    std::vector<std::shared_ptr<GroupNode>> *inStoreGroups,
                    const bool useReduction,
                    const int VEC_SIZE){
        auto& loadGroups = (*inLoadGroups);
        auto& storeGroups = (*inStoreGroups);

        // 0) Init node properties
        std::vector<std::shared_ptr<Node>> allNodes = GetAllNodes(outputs);
        std::vector<std::shared_ptr<Node>> inputs = GetInputs(outputs);
        std::unordered_map<const Node*, int> maxDistanceFromOutputs = GetMaxDistanceFromOutputs(outputs);
        assert(maxDistanceFromOutputs.size() == allNodes.size());
        std::unordered_map<const Node*, int> distanceFromInputs = GetMaxDistanceFromInputs(outputs);
        // Not necessarly true if there is dead code, assert(distanceFromInputs.size() == allNodes.size());

        // 1) We group the same operation at the same level of the graph
        std::map<int, std::unordered_map<std::string, std::unordered_map<std::string, std::vector<std::shared_ptr<Node>>>>> typeOperationLevelNode;
        std::unordered_map<std::ptrdiff_t, std::shared_ptr<GroupNode>> ptrToLoadGroupMapping;
        std::unordered_map<std::string, std::shared_ptr<GroupNode>> ptrToSetGroupMapping;
        std::unordered_map<std::ptrdiff_t, std::shared_ptr<GroupNode>> ptrToStoreGroupMapping;
        for(std::shared_ptr<Node>& currentNode : allNodes){
            if(currentNode->getData()->isScalarOperation()){
                const auto& operation = currentNode->getData()->getScalar().getOperation();
                typeOperationLevelNode[distanceFromInputs[currentNode.get()]][currentNode->getData()->getScalar().type][operation.operation].emplace_back(currentNode);
            }
            else if(currentNode->getData()->isScalarLoadOrSet()){
                // We started from outputs
                assert(currentNode->getNbOutputs());
                if(currentNode->getData()->isScalarSet()){
                    const auto& set = currentNode->getData()->getScalar().getSet();
                    const std::string key = currentNode->getData()->getScalar().type + Instruction::ValueToString(set.value);
                    if(ptrToSetGroupMapping.find(key) == ptrToSetGroupMapping.end()){
                        std::vector<std::shared_ptr<Node>> listOfScalarNodes;
                        listOfScalarNodes.emplace_back(currentNode);
                        auto group = Autovec::GroupInstruction::make_group_set(listOfScalarNodes);

                        std::shared_ptr<GroupNode> groupNode(new GroupNode);
                        groupNode->getData() = std::move(group);
                        ptrToSetGroupMapping[key] = groupNode;
                        loadGroups.emplace_back(groupNode);
                    }
                    else{
                        ptrToSetGroupMapping[key]->getData()->nodes.emplace_back(currentNode);
                    }
                }
                else{
                    assert(currentNode->getData()->isScalarLoad());
                    const auto& load = currentNode->getData()->getScalar().getLoad();
                    if(ptrToLoadGroupMapping.find(load.ptr) == ptrToLoadGroupMapping.end()){
                        std::vector<std::shared_ptr<Node>> listOfScalarNodes;
                        listOfScalarNodes.emplace_back(currentNode);
                        auto group = Autovec::GroupInstruction::make_group_load(listOfScalarNodes);

                        std::shared_ptr<GroupNode> groupNode(new GroupNode);
                        groupNode->getData() = std::move(group);
                        ptrToLoadGroupMapping[load.ptr] = groupNode;
                        loadGroups.emplace_back(groupNode);
                    }
                    else{
                        ptrToLoadGroupMapping[load.ptr]->getData()->nodes.emplace_back(currentNode);
                    }
                }
            }
            else if(currentNode->getData()->isScalarStore()){
                if(currentNode->getNbInputs(0)){
                    const auto& store = currentNode->getData()->getScalar().getStore();
                    if(ptrToStoreGroupMapping.find(store.ptr) == ptrToStoreGroupMapping.end()){
                        std::vector<std::shared_ptr<Node>> listOfScalarNodes;
                        listOfScalarNodes.emplace_back(currentNode);
                        auto group = Autovec::GroupInstruction::make_group_store(listOfScalarNodes);

                        std::shared_ptr<GroupNode> groupNode(new GroupNode);
                        groupNode->getData() = std::move(group);
                        ptrToStoreGroupMapping[store.ptr] = groupNode;
                        storeGroups.emplace_back(groupNode);
                    }
                    else{
                        ptrToStoreGroupMapping[store.ptr]->getData()->nodes.emplace_back(currentNode);
                    }
                }
            }
            else{
                assert(0);
            }
        }
        std::unordered_map<Node*, std::shared_ptr<GroupNode>> nodeToGroupMapping;
        std::vector<std::shared_ptr<GroupNode>> operationGroups;
        // Access the operations by level
        for(auto& iterLevel : typeOperationLevelNode){
            // For the different types
            for(auto& iterType : iterLevel.second){
                // For the different operation
                for(auto& iterOperation : iterType.second){
                    // Skip reduction groups
                    if(iterOperation.first.rfind("Reduction ", 0) == 0)
                        continue;

                    // Create the groups
                    auto& listOfScalarNodes = iterOperation.second;
                    auto group = Autovec::GroupInstruction::make_group_operation(listOfScalarNodes);

                    std::shared_ptr<GroupNode> groupNode(new GroupNode);
                    groupNode->getData() = std::move(group);
                    // Keep track of the mapping
                    for(auto& iterNode : listOfScalarNodes){
                        nodeToGroupMapping[iterNode.get()] = groupNode;
                    }

                    operationGroups.emplace_back(groupNode);
                }
            }
        }

        // Adjust reduction operation groups
        if(useReduction){
            std::queue<std::shared_ptr<Node>> nodesToProceed;
            std::set<std::shared_ptr<Node>> alreadyDoneNodes;
            for(auto& root : outputs){
                nodesToProceed.push(root);
                alreadyDoneNodes.insert(root);
            }
            while(nodesToProceed.size()){
                std::shared_ptr<Node> currentNode = nodesToProceed.front();
                const auto& currentNodeData = currentNode->getData();
                nodesToProceed.pop();

                if(currentNodeData->isScalarOperation()
                    && (currentNodeData->getScalar().getOperation().operation.rfind("Reduction ", 0) == 0))
                {
                    const auto baseNode = currentNode;
                    const auto& groupType = currentNodeData->getScalar().type;
                    const auto& reductionOperation = currentNodeData->getScalar().getOperation().operation;
                    const auto groupOperation = reductionOperation.substr(std::string("Reduction ").length());

                    std::queue<std::shared_ptr<Node>> nextNodes;

                    // Identify reduction instructions group
                    std::deque<std::shared_ptr<Node>> reductionNodes;
                    assert(currentNode->getNbInputs(0) == 1);
                    nextNodes.push(currentNode->getInputs(0).front());
                    reductionNodes.emplace_front(currentNode);
                    alreadyDoneNodes.insert(currentNode);
                    do{
                        assert(currentNode->getNbInputs(1) == 1);
                        currentNode = currentNode->getInputs(1).front();

                        assert(alreadyDoneNodes.find(currentNode) == alreadyDoneNodes.end());
                        alreadyDoneNodes.insert(currentNode);

                        assert(currentNode->getData()->getScalar().type == groupType);
                        assert(currentNode->getData()->getScalar().getOperation().operation == reductionOperation);

                        reductionNodes.emplace_front(currentNode);
                        assert(currentNode->getNbInputs(0) == 1);
                        nextNodes.push(currentNode->getInputs(0).front());
                    }while (currentNode->getNbInputs(1));
                    assert(int(reductionNodes.size()) <= VEC_SIZE);

                    auto reductionGroup = Autovec::GroupInstruction::make_group_operation({reductionNodes.begin(), reductionNodes.end()});
                    std::shared_ptr<GroupNode> reductionGroupNode(new GroupNode);
                    reductionGroupNode->getData() = std::move(reductionGroup);
                    // Update the mapping
                    for(auto& iterNode : reductionNodes)
                        nodeToGroupMapping[iterNode.get()] = reductionGroupNode;
                    operationGroups.emplace_back(reductionGroupNode);

                    // Identify operations of the reduction tree
                    std::deque<std::shared_ptr<Node>> groupNodes;
                    do{
                        std::queue<std::shared_ptr<Node>> rightOpsNodes;
                        while(nextNodes.size()){
                            std::shared_ptr<Node> nextNode = nextNodes.front();
                            const auto& nextNodeData = nextNode->getData();
                            nextNodes.pop();

                            if(nextNodeData->isScalarOperation()
                                && nextNodeData->getScalar().type == groupType
                                && nextNodeData->getScalar().getOperation().operation == groupOperation
                                && nextNode->getNbOutputs() == 1)
                            {
                                groupNodes.emplace_front(nextNode);

                                for(auto& input : nextNode->getInputs(0)){
                                    if(alreadyDoneNodes.find(input) == alreadyDoneNodes.end())
                                    {
                                        alreadyDoneNodes.insert(input);
                                        nextNodes.push(input);
                                    }
                                }

                                // Process right inputs (other vectors) later
                                for(auto& input : nextNode->getInputs(1)){
                                    if(alreadyDoneNodes.find(input) == alreadyDoneNodes.end())
                                    {
                                        alreadyDoneNodes.insert(input);
                                        rightOpsNodes.push(input);
                                    }
                                }
                            }
                            else
                                nodesToProceed.push(nextNode);
                        }
                        nextNodes = std::move(rightOpsNodes);
                    }while(nextNodes.size());

                    // Vectorize operations in the tree
                    while(groupNodes.size()){
                        std::vector<std::shared_ptr<Node>> layerNodes;

                        const size_t limit = std::min<size_t>(VEC_SIZE, groupNodes.size());
                        for(size_t nodeIdx = 0; nodeIdx < limit; ++nodeIdx){
                            layerNodes.emplace_back(groupNodes.front());
                            groupNodes.pop_front();
                        }

                        auto group = Autovec::GroupInstruction::make_group_operation(layerNodes);

                        std::shared_ptr<GroupNode> groupNode(new GroupNode);
                        groupNode->getData() = std::move(group);
                        // Update the mapping and groups
                        for(auto& iterNode : layerNodes){
                            auto& oldGroup = nodeToGroupMapping[iterNode.get()];
                            oldGroup->getData()->removeNode(iterNode);
                            if(!oldGroup->getData()->nodes.size()){
                                operationGroups.erase(std::remove(operationGroups.begin(),
                                                                    operationGroups.end(), oldGroup),
                                                            operationGroups.end());
                            }

                            nodeToGroupMapping[iterNode.get()] = groupNode;
                        }

                        operationGroups.emplace_back(groupNode);
                    }

                    bool areOutputsStores = true;
                    for(const auto& output : baseNode->getOutputs())
                    {
                        if(!output->getData()->isScalarStore())
                            areOutputsStores = false;
                    }

                    // Keep store outputs as scalar stores in the future
                    if(areOutputsStores)
                    {
                        for(const auto& output : baseNode->getOutputs())
                        {
                            auto& oldGroup = ptrToStoreGroupMapping[output->getData()->getScalar().getStore().ptr];
                            oldGroup->getData()->removeNode(output);
                            if(!oldGroup->getData()->nodes.size()){
                                ptrToStoreGroupMapping.erase(output->getData()->getScalar().getStore().ptr);
                                if(oldGroup->getData()->isGroupLoadOrSet()){
                                    auto iter = std::find_if(loadGroups.begin(), loadGroups.end(),
                                      [&oldGroup](const auto& itest){ return itest.get() == oldGroup.get(); });
                                    assert(iter != loadGroups.end());
                                    loadGroups.erase(iter);
                                }
                                else if(oldGroup->getData()->isGroupStore()){
                                    auto iter = std::find_if(storeGroups.begin(), storeGroups.end(),
                                      [&oldGroup](const auto& itest){ return itest.get() == oldGroup.get(); });
                                    assert(iter != storeGroups.end());
                                    storeGroups.erase(iter);
                                }
                                else if(oldGroup->getData()->isGroupOperation()){
                                    auto iter = std::find_if(operationGroups.begin(), operationGroups.end(),
                                      [&oldGroup](const auto& itest){ return itest.get() == oldGroup.get(); });
                                    assert(iter != operationGroups.end());
                                    operationGroups.erase(iter);
                                }
                                else{
                                    assert(0);
                                }
                            }
                        }
                    }
                }
                else
                {
                    for(int idxInput = 0; idxInput < 2; ++idxInput){
                        for(auto& input : currentNode->getInputs(idxInput)){
                            if(alreadyDoneNodes.find(input) == alreadyDoneNodes.end())
                            {
                                alreadyDoneNodes.insert(input);
                                nodesToProceed.push(input);
                            }
                        }
                    }
                }
            }
        }

        // Connect the groups from store that are not linked to operations
        for(auto& iterGroup : storeGroups){
            auto& listOfScalarNodes = iterGroup->getData()->nodes;
            assert(listOfScalarNodes.size());

            std::set<std::shared_ptr<GroupNode>> linputs[1];
            const int endingLimite = 1;

            for(auto& iterNode : listOfScalarNodes){
                assert(iterNode->getNbInputs(0));
                assert(iterNode->getNbInputs(1) == 0);
                assert(iterNode->getNbOutputs() == 0);
                for(int idxInput = 0 ; idxInput < endingLimite ; ++idxInput){
                    assert(iterNode->getNbInputs(idxInput));

                    // Connect only with load or set
                    if(iterNode->getInputs(idxInput).front()->getData()->isScalarLoad()){
                        const auto& scalarLoad = iterNode->getInputs(idxInput).front()->getData()->getScalar().getLoad();
                        assert(ptrToLoadGroupMapping.find(scalarLoad.ptr) != ptrToLoadGroupMapping.end());
                        linputs[idxInput].insert(ptrToLoadGroupMapping[scalarLoad.ptr]);
                    }
                    else if(iterNode->getInputs(idxInput).front()->getData()->isScalarSet()){
                        const auto& scalarSet = iterNode->getInputs(idxInput).front()->getData()->getScalar().getSet();
                        const std::string key = iterNode->getInputs(idxInput).front()->getData()->getScalar().type + Instruction::ValueToString(scalarSet.value);
                        assert(ptrToSetGroupMapping.find(key) != ptrToSetGroupMapping.end());
                        linputs[idxInput].insert(ptrToSetGroupMapping[key]);
                    }
                }
            }
            // Connect them ("set" ensure that there is no repetition)
            for(int idxInput = 0 ; idxInput < 1 ; ++idxInput){
                for(auto input : linputs[idxInput]){
                    GroupNode::Connect(iterGroup, input, idxInput);
                }
            }
        }

        // Connect the groups with operations
        for(auto& iterGroup : operationGroups){
            auto& listOfScalarNodes = iterGroup->getData()->nodes;

            std::set<std::shared_ptr<GroupNode>> linputs[2];
            std::set<std::shared_ptr<GroupNode>> loutputs[2];

            assert(listOfScalarNodes.size());
            const auto& frontNodeData = listOfScalarNodes.front()->getData();
            const bool isReduction = frontNodeData->isScalarOperation()
                && frontNodeData->getScalar().getOperation().operation.rfind("Reduction ", 0) == 0;
            const int endingLimite = isReduction ? 1 : 2;

            for(auto& iterNode : listOfScalarNodes){
                assert(iterNode->getNbInputs(0) || iterNode->getNbInputs(1));
                for(int idxInput = 0 ; idxInput < endingLimite ; ++idxInput){
                    assert(iterNode->getNbInputs(idxInput));
                    
                    if(iterNode->getInputs(idxInput).front()->getData()->isScalarLoad()){
                        const auto& scalarLoad = iterNode->getInputs(idxInput).front()->getData()->getScalar().getLoad();
                        assert(ptrToLoadGroupMapping.find(scalarLoad.ptr) != ptrToLoadGroupMapping.end());
                        linputs[idxInput].insert(ptrToLoadGroupMapping[scalarLoad.ptr]);
                    }
                    else if(iterNode->getInputs(idxInput).front()->getData()->isScalarSet()){
                        const auto& scalarSet = iterNode->getInputs(idxInput).front()->getData()->getScalar().getSet();
                        const std::string key = iterNode->getInputs(idxInput).front()->getData()->getScalar().type + Instruction::ValueToString(scalarSet.value);
                        assert(ptrToSetGroupMapping.find(key) != ptrToSetGroupMapping.end());
                        linputs[idxInput].insert(ptrToSetGroupMapping[key]);
                    }
                    else if(iterNode->getInputs(idxInput).front()->getData()->isScalarOperation()){
                        // It is an operation, a group already covers it
                        assert(nodeToGroupMapping.find(iterNode->getInputs(idxInput).front().get()) != nodeToGroupMapping.end());
                        linputs[idxInput].insert(nodeToGroupMapping[iterNode->getInputs(idxInput).front().get()]);
                    }
                    else{
                        // A store cannot be a input
                        assert(0);
                    }
                }

                // Must have a output
                assert(iterNode->getOutputs().size());
                for(auto& output : iterNode->getOutputs()){
                    if(isReduction && nodeToGroupMapping[output.get()] == iterGroup)
                        continue;

                    if(output->getData()->isScalarStore()){
                        // Put it in a list to create a futur group
                        const auto& scalarStore = output->getData()->getScalar().getStore();

                        if(isReduction)
                        {

                            if(ptrToStoreGroupMapping.find(scalarStore.ptr) == ptrToStoreGroupMapping.end()
                                || !ptrToStoreGroupMapping[scalarStore.ptr]->getData()->hasNode(output))
                            {
                                // Create a new group for the store that will stay as scalar store
                                auto storeGroup = Autovec::GroupInstruction::make_group_store({output});

                                std::shared_ptr<GroupNode> groupNode(new GroupNode);
                                groupNode->getData() = std::move(storeGroup);
                                storeGroups.emplace_back(groupNode);

                                loutputs[0].insert(groupNode);
                            }
                            else
                            {
                                assert(ptrToStoreGroupMapping[scalarStore.ptr]->getData()->hasNode(output));
                                loutputs[0].insert(ptrToStoreGroupMapping[scalarStore.ptr]);
                            }
                        }
                        else
                        {
                            assert(ptrToStoreGroupMapping.find(scalarStore.ptr) != ptrToStoreGroupMapping.end());
                            assert(ptrToStoreGroupMapping[scalarStore.ptr]->getData()->hasNode(output));
                            loutputs[0].insert(ptrToStoreGroupMapping[scalarStore.ptr]);
                        }
                    }
                    else if(output->getData()->isScalarOperation()){
                        // It is an operation, a group already covers it
                        assert(nodeToGroupMapping.find(output.get()) != nodeToGroupMapping.end());
                        // Find if it is P1 or P2 (or both)
                        if(output->nodeInInputs(iterNode, 0)){
                            loutputs[0].insert(nodeToGroupMapping[output.get()]);
                        }
                        if(output->nodeInInputs(iterNode, 1)){
                            loutputs[1].insert(nodeToGroupMapping[output.get()]);
                        }
                    }
                    else{
                        // A load cannot be a output
                        assert(0);
                    }
                }
            }
            // Connect them ("set" ensure that there is no repetition)
            for(int idxInput = 0 ; idxInput < 2 ; ++idxInput){
                for(auto input : linputs[idxInput]){
                    GroupNode::Connect(iterGroup, input, idxInput);
                }
            }
            for(int idxPrev = 0 ; idxPrev < 2 ; ++idxPrev){
                for(auto prev : loutputs[idxPrev]){
                    GroupNode::Connect(prev, iterGroup, idxPrev);
                }
            }

        }

        // Sanity check
        {
            std::queue<std::shared_ptr<GroupNode>> groupNodesToProceed;
            // We go backward and start by groups from load
            for(auto& iterGroup : loadGroups){
                groupNodesToProceed.push(iterGroup);
            }

            std::set<GroupNode*> alreadyDone;
            while(groupNodesToProceed.size()){
                std::shared_ptr<GroupNode> currentNode = groupNodesToProceed.front();
                groupNodesToProceed.pop();

                if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                    alreadyDone.insert(currentNode.get());

                    for(auto& iterGroup : currentNode->getOutputs()){
                        assert(iterGroup->nodeInAnyInputs(currentNode));
                        groupNodesToProceed.push(iterGroup);
                    }

                    assert(currentNode->getData()->nodes.size());

                    // Replace scalar per vectorial
                    if(currentNode->getData()->isGroupLoadOrSet()){
                        assert(currentNode->getNbOutputs() >= 1);
                        assert(currentNode->getNbInputs(0) == 0);
                        assert(currentNode->getNbInputs(1) == 0);
                    }
                    else if(currentNode->getData()->isGroupStore()){
                        assert(currentNode->getNbOutputs() == 0);
                        assert(currentNode->getNbInputs(0) >= 1);
                        assert(currentNode->getNbInputs(1) == 0);
                    }
                    else if(currentNode->getData()->isGroupOperation()){
                        assert(currentNode->getNbOutputs() >= 1);
                        // we can not limit the number of inputs of a group operation node
                        //assert(currentNode->getNbInputs(0) == 1);
                        //assert(currentNode->getNbInputs(1) <= 1);
                    }
                    else{
                        assert(0);
                    }
                }
            }
        }
    }

    bool CreateReductionTrees(std::vector<std::shared_ptr<Node>>* ptrOutputs, size_t maxVectorInUse,
                              const int VEC_SIZE){
        auto outputs = (*ptrOutputs);
        assert(maxVectorInUse > 0);

        bool hasFoundReduction = false;
        
        // Identify groups starting from outputs
        std::queue<std::shared_ptr<Node>> nodesToProceed;
        std::set<Node*> visitedNodes;
        for(auto& root : outputs){
            nodesToProceed.push(root);
        }
        while(nodesToProceed.size()){
            std::shared_ptr<Node> node = nodesToProceed.front();
            const auto& nodeData = node->getData();
            nodesToProceed.pop();

            if(visitedNodes.find(node.get()) != visitedNodes.end())
                continue;
            else
                visitedNodes.insert(node.get());

            // Look for a commutative operation to start with
            if(nodeData->isScalarOperation() && (nodeData->getScalar().getOperation().operation == "+"
                || nodeData->getScalar().getOperation().operation == "*"))
            {
                auto& groupHead = node;
                const auto& groupType = groupHead->getData()->getScalar().type;
                const auto& groupOperation = groupHead->getData()->getScalar().getOperation().operation;

                // Sucessors/sources of the whole group stored from left to right
                std::deque<std::shared_ptr<Node>> inputsNodes;
                std::deque<std::shared_ptr<Node>> groupNodes;

                // Explore from head to form a group
                std::deque<std::shared_ptr<Node>> nextNodes;
                for(int idxInput = 1; idxInput >= 0; --idxInput){
                    for(auto& input : groupHead->getInputs(idxInput)){
                        nextNodes.emplace_front(input);
                    }
                }
                while(nextNodes.size()){
                    std::shared_ptr<Node> nextNode = nextNodes.front();
                    nextNodes.pop_front();

                    const auto& nextNodeData = nextNode->getData();

                    assert(visitedNodes.find(nextNode.get()) == visitedNodes.end()
                        || !nextNodeData->isScalarOperation()
                        || nextNode->getNbOutputs() > 1);

                    if(nextNodeData->isScalarOperation()
                        && nextNodeData->getScalar().type == groupType
                        && nextNodeData->getScalar().getOperation().operation == groupOperation
                        && nextNode->getNbOutputs() == 1)
                    {
                        groupNodes.emplace_back(nextNode);
                        visitedNodes.insert(nextNode.get());

                        for(int idxInput = 1; idxInput >= 0; --idxInput){
                            for(auto& input : nextNode->getInputs(idxInput)){
                                nextNodes.emplace_front(input);
                            }
                        }
                    }
                    else
                    {
                        inputsNodes.emplace_back(nextNode);
                        if(visitedNodes.find(nextNode.get()) == visitedNodes.end())
                            nodesToProceed.push(nextNode);
                    }
                }

                // Form a group if it's worth it (enough nodes) and reorder nodes to form reduction trees
                if(groupNodes.size() >= 2)
                {
                    hasFoundReduction = true;

                    // Regroup inputs of the same type
                    std::unordered_map<std::ptrdiff_t, std::deque<std::shared_ptr<Node>>> loadInputs;
                    std::unordered_map<std::string, std::deque<std::shared_ptr<Node>>> operationInputs;
                    for(auto& input : inputsNodes){
                        if(input->getData()->isScalarLoad()){
                            loadInputs[input->getData()->getScalar().getLoad().ptr].emplace_back(input);
                        }
                        else if(input->getData()->isScalarSet()){
                            loadInputs[-1].emplace_back(input);
                        }
                        else if(input->getData()->isScalarOperation()){
                            operationInputs[input->getData()->getScalar().getOperation().operation].emplace_back(input);
                        }
                    }
                    for(auto& iterLoadGroup : loadInputs){
                        if(iterLoadGroup.first != -1){
                            // Order loads by occurrences of their offset and then by offset
                            // Example: offsets 3,1,2,1,2,3 will be order 1,2,3,1,2,3
                            std::unordered_map<std::ptrdiff_t, int> offsetCount;
                            std::vector<std::pair<std::shared_ptr<Node>, int>> inputsPairs;
                            for(auto& input : iterLoadGroup.second)
                                inputsPairs.emplace_back(std::pair<std::shared_ptr<Node>, int>(
                                    input, offsetCount[input->getData()->getScalar().getLoad().offset]++));

                            std::sort(inputsPairs.begin(), inputsPairs.end(), [](const auto& s1, const auto& s2){
                                return s1.second < s2.second
                                    || (s1.second == s2.second
                                        && s1.first->getData()->getScalar().getLoad().offset < s2.first->getData()->getScalar().getLoad().offset);
                            });

                            iterLoadGroup.second.clear();
                            for(auto& inputPair : inputsPairs)
                                iterLoadGroup.second.emplace_back(inputPair.first);
                        }
                    }

                    // Prepare inputs
                    std::queue<std::shared_ptr<Node>> treeSources;
                    // First extract groups of VEC_SIZE size
                    for(auto& iterLoadGroup : loadInputs){
                        while(int(iterLoadGroup.second.size()) >= VEC_SIZE){
                            for(int idx = 0; idx < VEC_SIZE; ++idx){
                                treeSources.push(iterLoadGroup.second.front());
                                iterLoadGroup.second.pop_front();
                            }
                        }
                    }

                    for(auto& iterOperationGroup : operationInputs){
                        while(int(iterOperationGroup.second.size()) >= VEC_SIZE){
                            for(int idx = 0; idx < VEC_SIZE; ++idx){
                                treeSources.push(iterOperationGroup.second.front());
                                iterOperationGroup.second.pop_front();
                            }
                        }
                    }
                    // Then extract the rest
                    for(auto& iterLoadGroup : loadInputs){
                        for(auto& input : iterLoadGroup.second){
                            treeSources.push(input);
                        }
                    }
                    for(auto& iterOperationGroup : operationInputs){
                        for(auto& input : iterOperationGroup.second){
                            treeSources.push(input);
                        }
                    }
                    loadInputs.clear();
                    operationInputs.clear();

                    // Disconnect nodes to build tree
                    for(auto& groupNode : groupNodes){
                        for(int idxInput = 0; idxInput < 2; ++idxInput){
                            auto oldInputs = groupNode->getInputs(idxInput);
                            for(auto& input : oldInputs){
                                Autovec::Node::Disconnect(groupNode, input, idxInput);
                            }
                        }
                    }
                    for(int idxInput = 0; idxInput < 2; ++idxInput){
                        auto oldInputs = groupHead->getInputs(idxInput);
                        for(auto& input : oldInputs){
                            Autovec::Node::Disconnect(groupHead, input, idxInput);
                        }
                    }

                    // Build the tree if there's enough nodes to fill multiple vectors
                    std::queue<std::shared_ptr<Node>> treeInputs;
                    if(int(treeSources.size()) > VEC_SIZE)
                    {
                        const size_t vecCount = std::min<size_t>(maxVectorInUse, treeSources.size() / VEC_SIZE);

                        // Divide reduction sources for the multiple vectors
                        const size_t opsCount = treeSources.size() / VEC_SIZE - vecCount;
                        const size_t OpsPerVector = opsCount / vecCount;
                        const size_t vecLeftCount = opsCount - (OpsPerVector * vecCount);
                        for(size_t vecIdx = 0; vecIdx < vecCount; ++vecIdx){
                            const size_t vectorOpsCount = vecIdx < vecLeftCount ? OpsPerVector + 1 : OpsPerVector;

                            std::vector<std::shared_ptr<Node>> vecInputs(VEC_SIZE);
                            assert(int(treeSources.size()) >= VEC_SIZE);
                            for(int eltIdx = 0; eltIdx < VEC_SIZE; ++eltIdx){
                                vecInputs[eltIdx] = treeSources.front();
                                treeSources.pop();
                            }

                            for(size_t opIdx = 0; opIdx < vectorOpsCount; ++opIdx){
                                assert(int(treeSources.size()) >= VEC_SIZE);

                                for(int eltIdx = 0; eltIdx < VEC_SIZE; ++eltIdx){
                                    std::shared_ptr<Node> source = treeSources.front();
                                    treeSources.pop();
                                    std::shared_ptr<Node> operationNode = groupNodes.front();
                                    groupNodes.pop_front();

                                    Autovec::Node::Connect(operationNode, vecInputs[eltIdx], 0);
                                    Autovec::Node::Connect(operationNode, source, 1);
                                    vecInputs[eltIdx] = operationNode;
                                }
                            }

                            for(int eltIdx = 0; eltIdx < VEC_SIZE; ++eltIdx)
                                treeInputs.push(vecInputs[eltIdx]);
                        }
                        assert(int(treeSources.size()) < VEC_SIZE);

                        // Merge vectors
                        while(int(treeInputs.size()) > VEC_SIZE){
                            assert(int(treeInputs.size()) >= 2 * VEC_SIZE);

                            std::vector<std::shared_ptr<Node>> vecInputs(VEC_SIZE);
                            for(int eltIdx = 0; eltIdx < VEC_SIZE; ++eltIdx){
                                vecInputs[eltIdx] = treeInputs.front();
                                treeInputs.pop();
                            }
                            for(int eltIdx = 0; eltIdx < VEC_SIZE; ++eltIdx){
                                std::shared_ptr<Node> otherInput = treeInputs.front();
                                treeInputs.pop();
                                std::shared_ptr<Node> operationNode = groupNodes.front();
                                groupNodes.pop_front();

                                Autovec::Node::Connect(operationNode, vecInputs[eltIdx], 0);
                                Autovec::Node::Connect(operationNode, otherInput, 1);
                                treeInputs.push(operationNode);
                            }
                        }

                        // Adding remaining sources (vector with less than VEC_SIZE elements)
                        while(treeSources.size()){
                            std::shared_ptr<Node> source = treeSources.front();
                            treeSources.pop();
                            std::shared_ptr<Node> treeInput = treeInputs.front();
                            treeInputs.pop();
                            std::shared_ptr<Node> operationNode = groupNodes.front();
                            groupNodes.pop_front();

                            Autovec::Node::Connect(operationNode, treeInput, 0);
                            Autovec::Node::Connect(operationNode, source, 1);
                            treeInputs.push(operationNode);
                        }
                    }
                    else
                    {
                        treeInputs = treeSources;
                    }

                    // Special nodes that will be grouped into final reduction
                    const auto reductionType = groupHead->getData()->getScalar().type;
                    const auto reductionOp = std::string("Reduction ").append(groupHead->getData()->getScalar().getOperation().operation);

                    assert(int(treeInputs.size()) >= 2 && int(treeInputs.size()) <= VEC_SIZE);
                    std::shared_ptr<Node> lastReductionNode(new Node);
                    lastReductionNode->getData() = Instruction::make_scalar_operation(reductionType, reductionOp);
                    visitedNodes.insert(lastReductionNode.get());
                    Autovec::Node::Connect(lastReductionNode, treeInputs.front(), 0);
                    treeInputs.pop();

                    while(treeInputs.size()){
                        std::shared_ptr<Node> reductionNode(new Node);
                        reductionNode->getData() = Instruction::make_scalar_operation(reductionType, reductionOp);
                        visitedNodes.insert(reductionNode.get());

                        Autovec::Node::Connect(reductionNode, lastReductionNode, 1);
                        Autovec::Node::Connect(reductionNode, treeInputs.front(), 0);
                        treeInputs.pop();

                        lastReductionNode = reductionNode;
                    }

                    // Connect group's outputs
                    auto oldOutputs = groupHead->getOutputs();
                    for(auto groupOutput : oldOutputs)
                        for(int idxInput = 0; idxInput < 2; ++idxInput){
                            if(groupOutput->nodeInInputs(groupHead, idxInput))
                            {
                                Autovec::Node::Disconnect(groupOutput, groupHead, idxInput);
                                Autovec::Node::Connect(groupOutput, lastReductionNode, idxInput);
                            }
                        }
                }
            }
            else
            {
                for(int idxInput = 0; idxInput < 2; ++idxInput){
                    for(auto& input : node->getInputs(idxInput)){
                        if(visitedNodes.find(input.get()) == visitedNodes.end())
                        {
                            nodesToProceed.push(input);
                        }
                    }
                }
            }
        }

        return hasFoundReduction;
    }

    void RemoveDuplicate(const std::vector<std::shared_ptr<Node>>* ptrOutputs){
        auto inputs = GetInputs(*ptrOutputs);

        std::queue<std::shared_ptr<Node>> nodesToProceed;
        std::unordered_map<std::shared_ptr<Node>, int> countDeps;

        for(auto& lv : inputs){
            nodesToProceed.push(lv);
        }
        while(nodesToProceed.size()){
            std::shared_ptr<Node> node = nodesToProceed.front();
            const auto& nodeData = node->getData();
            nodesToProceed.pop();

            if(nodeData->isScalarLoadOrSet() || nodeData->isScalarOperation()){

                for(int idxOutput1 = 0 ; idxOutput1 < int(node->getOutputs().size()) ; ++idxOutput1){
                    int idxOutput2 = idxOutput1+1;
                    while(idxOutput2 < int(node->getOutputs().size())){
                        auto currentOutput = node->getOutputs()[idxOutput1];
                        auto otherOutput = node->getOutputs()[idxOutput2];
                        assert(currentOutput.get() != otherOutput.get());

                        if(!currentOutput->getData()->isScalarStore()
                                && currentOutput->getData()->getDescription() == otherOutput->getData()->getDescription()){

                            auto areEquivalent = [](const auto& inputs1, const auto& inputs2) -> bool {
                                if(inputs1.size() != inputs2.size()){
                                    return false;
                                }

                                if(inputs1 == inputs2){
                                    return true;
                                }

                                std::set<Node*> nodes1;
                                for(auto& nd : inputs1){
                                    nodes1.insert(nd.get());
                                }

                                for(auto& nd : inputs2){
                                    if(nodes1.find(nd.get()) == nodes1.end()){
                                        return false;
                                    }
                                }

                                return true;
                            };

                            const bool operationOrderMatters = (currentOutput->getData()->isScalarOperation()
                                                                && !currentOutput->getData()->getScalar().getOperation().isCommutative());
                            const bool isSame = (areEquivalent(currentOutput->getInputs(0), otherOutput->getInputs(0))
                                                 && areEquivalent(currentOutput->getInputs(1), otherOutput->getInputs(1)))
                                                || (!operationOrderMatters && areEquivalent(currentOutput->getInputs(0), otherOutput->getInputs(1))
                                                    && areEquivalent(currentOutput->getInputs(1), otherOutput->getInputs(0)));

                            if(isSame){
                                // Replace the node
                                auto oldOutputs = otherOutput->getOutputs();
                                for(auto output : oldOutputs){
                                    for(int idxInput = 0; idxInput < 2; ++idxInput){
                                        if(output->nodeInInputs(otherOutput, idxInput)){
                                            Autovec::Node::Disconnect(output, otherOutput, idxInput);
                                            Autovec::Node::Connect(output, currentOutput, idxInput);
                                        }
                                    }
                                }
                                assert(otherOutput->getOutputs().size() == 0);
                                for(int idxInput = 0; idxInput < 2; ++idxInput){
                                    auto oldInputs = otherOutput->getInputs(idxInput);
                                    for(auto input : oldInputs){
                                        // Could update "node" list too
                                        Autovec::Node::Disconnect(otherOutput, input, idxInput);
                                        assert(input->nodeInOutputs(currentOutput));
                                        assert(currentOutput->nodeInAnyInputs(input));
                                        assert(!operationOrderMatters || currentOutput->nodeInInputs(input, idxInput));
                                        assert(currentOutput->getNbOutputs());
                                    }
                                }
                                assert(node->getNbOutputs());
                            }
                            else{
                                idxOutput2 += 1;
                            }
                        }
                        else{
                            idxOutput2 += 1;
                        }
                    }
                }

                for(auto& output : node->getOutputs()){
                    countDeps[output] += 1;
                    if(countDeps[output] == (output->getNbInputsAll())){
                        nodesToProceed.push(output);
                    }
                }
            }
        }
    }

    void ConstantPropagation(std::vector<std::shared_ptr<Node>>* ptrOutputs){
        { // Have unique "set"
            auto inputs = GetInputs(*ptrOutputs);

            std::unordered_map<std::string, std::shared_ptr<Node>> alreadySeenNode;

            int idxLeaf = 0;
            while(idxLeaf < int(inputs.size())){
                auto node = inputs[idxLeaf];
                if(node->getData()->isSet()){ // Work for set only
                    if(alreadySeenNode.find(node->getData()->getDescription()) != alreadySeenNode.end()){
                        auto goodNode = alreadySeenNode[node->getData()->getDescription()];

                        assert(node->getInputs(0).size() == 0);
                        assert(node->getInputs(1).size() == 0);
                        auto copyOutputs = node->getOutputs();
                        for(auto output : copyOutputs){
                            for(int idxInput = 0; idxInput < 2; ++idxInput){
                                if(output->nodeInInputs(node, idxInput)){
                                    Autovec::Node::Disconnect(output, node, idxInput);
                                    Autovec::Node::Connect(output, goodNode, idxInput);
                                }
                            }
                        }
                        assert(node->getOutputs().size() == 0);

                        inputs.erase(inputs.begin() + idxLeaf);
                    }
                    else{
                        alreadySeenNode[node->getData()->getDescription()] = node;
                        idxLeaf += 1;
                    }
                }
                else{
                    idxLeaf += 1;
                }
            }
        }
        auto outputs = (*ptrOutputs);

        std::queue<std::shared_ptr<Node>> nodesToProceed;
        std::set<Node*> visitedNodes;

        for(auto& root : outputs){
            nodesToProceed.push(root);
            visitedNodes.insert(root.get());
        }
        while(nodesToProceed.size()){
            std::shared_ptr<Node> node = nodesToProceed.front();
            const auto& nodeData = node->getData();
            nodesToProceed.pop();

            if(nodeData->isScalarOperation())
            {
                const std::string operation = nodeData->getScalar().getOperation().operation;
                
                int inputToRemove = -1;
                for(int idxInput = 0; idxInput < 2 && inputToRemove == -1; ++idxInput)
                {
                    assert(node->getNbInputs(idxInput) == 1);

                    const auto& inputData = node->getInputs(idxInput).front()->getData();
                    if(inputData->isScalarSet()
                        && ((operation == "+" && CompareSetValues(inputData->getScalar().getSet().value, 0))
                            || (operation == "*" && CompareSetValues(inputData->getScalar().getSet().value, 1))))
                    {
                        inputToRemove = idxInput;
                    }
                }

                if(inputToRemove != -1)
                {
                    const int inputToKeep = inputToRemove == 0 ? 1 : 0;

                    auto oldInputs = node->getInputs(inputToKeep);
                    for(auto opInput : oldInputs){
                        auto oldOutputs = node->getOutputs();
                        for(auto opOutput : oldOutputs){
                            for(int idxInput = 0; idxInput < 2; ++idxInput){
                                if(opOutput->nodeInInputs(node, idxInput))
                                {
                                    Autovec::Node::Disconnect(node, opInput, inputToKeep);
                                    Autovec::Node::Disconnect(opOutput, node, idxInput);
                                    Autovec::Node::Connect(opOutput, opInput, idxInput);
                                }
                            }
                        }
                    }
                }
            }

            for(int idxInput = 0; idxInput < 2; ++idxInput){
                for(auto& input : node->getInputs(idxInput)){
                    if(visitedNodes.find(input.get()) == visitedNodes.end())
                    {
                        visitedNodes.insert(input.get());
                        nodesToProceed.push(input);
                    }
                }
            }
        }
    }

    void SplitLoadsNaive(std::vector<std::shared_ptr<GroupNode>>* inLoadGroups,
                         const int VEC_SIZE){
        auto& loadGroups = (*inLoadGroups);
        std::vector<std::shared_ptr<GroupNode>> extraLoadGroup;
        for(auto& loadGroupToSplit : loadGroups){
            assert(loadGroupToSplit->getData()->isGroupLoadOrSet());

            if(loadGroupToSplit->getData()->isGroupLoad()){
                std::sort(std::begin(loadGroupToSplit->getData()->nodes), std::end(loadGroupToSplit->getData()->nodes),
                            [](auto& n1, auto& n2){
                    assert(n1->getData()->getScalar().getLoad().ptr == n2->getData()->getScalar().getLoad().ptr);
                    return n1->getData()->getScalar().getLoad().offset < n2->getData()->getScalar().getLoad().offset;
                });
                std::unique(std::begin(loadGroupToSplit->getData()->nodes), std::end(loadGroupToSplit->getData()->nodes),
                            [](auto& n1, auto& n2){
                    return n1->getData()->getScalar().getLoad().offset == n2->getData()->getScalar().getLoad().offset;
                });
            }

            if(int(loadGroupToSplit->getData()->nodes.size()) > VEC_SIZE){
                int idxFirst= (int(loadGroupToSplit->getData()->nodes.size()-1)/VEC_SIZE)*VEC_SIZE;
                while(VEC_SIZE <= idxFirst){
                    int idxLast = int(loadGroupToSplit->getData()->nodes.size());
                    std::vector<std::shared_ptr<Node>> subLoads;
                    subLoads.resize(idxLast-idxFirst);
                    while(idxFirst < idxLast){
                        subLoads[idxLast-idxFirst-1] = loadGroupToSplit->getData()->nodes.back();
                        loadGroupToSplit->getData()->nodes.pop_back();
                        idxLast -= 1;
                    }
                    idxLast = idxFirst;
                    idxFirst -= VEC_SIZE;
                    assert(subLoads.size());
                    // Create the new load group
                    auto newGroup = (loadGroupToSplit->getData()->isGroupLoad()?
                                         Autovec::GroupInstruction::make_group_load(subLoads)
                                       : Autovec::GroupInstruction::make_group_set(subLoads));
                    std::shared_ptr<GroupNode> groupNode(new GroupNode);
                    groupNode->getData() = std::move(newGroup);

                    for(auto& output : loadGroupToSplit->getOutputs()){
                        if(TestIfListsConnected(output->getData()->nodes,
                                                subLoads, 0)){
                            GroupNode::Connect(output, groupNode, 0);
                        }
                        if(TestIfListsConnected(output->getData()->nodes,
                                                subLoads, 1)){
                            GroupNode::Connect(output, groupNode, 1);
                        }
                    }

                    assert(groupNode->getNbOutputs() >= 1);
                    extraLoadGroup.emplace_back(groupNode);
                }

                auto oldOutputs = loadGroupToSplit->getOutputs();
                for(auto output : oldOutputs){
                    assert(output->nodeInInputs(loadGroupToSplit,0)
                            || output->nodeInInputs(loadGroupToSplit,1));
                    if(!TestIfListsConnected(output->getData()->nodes,
                                            loadGroupToSplit->getData()->nodes, 0)
                            && output->nodeInInputs(loadGroupToSplit,0)){
                        GroupNode::Disconnect(output, loadGroupToSplit, 0);
                    }
                    if(!TestIfListsConnected(output->getData()->nodes,
                                            loadGroupToSplit->getData()->nodes, 1)
                            && output->nodeInInputs(loadGroupToSplit,1)){
                        GroupNode::Disconnect(output, loadGroupToSplit, 1);
                    }
                }
            }
        }
        loadGroups.insert(loadGroups.end(), extraLoadGroup.begin(), extraLoadGroup.end());
    }

    void SplitStoresNaive(std::vector<std::shared_ptr<GroupNode>>* inStoreGroups,
                          const int VEC_SIZE){
        auto& storeGroups = (*inStoreGroups);
        std::vector<std::shared_ptr<GroupNode>> extraStoreGroup;
        for(auto& storeGroupToSplit : storeGroups){
            std::sort(std::begin(storeGroupToSplit->getData()->nodes), std::end(storeGroupToSplit->getData()->nodes),
                        [](auto& n1, auto& n2){
                assert(n1->getData()->getScalar().getStore().ptr == n2->getData()->getScalar().getStore().ptr);
                return n1->getData()->getScalar().getStore().offset < n2->getData()->getScalar().getStore().offset;
            });
            std::unique(std::begin(storeGroupToSplit->getData()->nodes), std::end(storeGroupToSplit->getData()->nodes),
                        [](auto& n1, auto& n2){
                return n1->getData()->getScalar().getStore().offset == n2->getData()->getScalar().getStore().offset;
            });
            if(int(storeGroupToSplit->getData()->nodes.size()) > VEC_SIZE){
                int idxFirst= (int(storeGroupToSplit->getData()->nodes.size()-1)/VEC_SIZE)*VEC_SIZE;
                while(VEC_SIZE <= idxFirst){
                    int idxLast = int(storeGroupToSplit->getData()->nodes.size());
                    std::vector<std::shared_ptr<Node>> subStores;
                    subStores.resize(idxLast-idxFirst);
                    while(idxFirst < idxLast){
                        subStores[idxLast-idxFirst-1] = storeGroupToSplit->getData()->nodes.back();
                        storeGroupToSplit->getData()->nodes.pop_back();
                        idxLast -= 1;
                    }
                    idxLast = idxFirst;
                    idxFirst -= VEC_SIZE;
                    assert(subStores.size());
                    // Create the new store group
                    auto newGroup = Autovec::GroupInstruction::make_group_store(subStores);
                    std::shared_ptr<GroupNode> groupNode(new GroupNode);
                    groupNode->getData() = std::move(newGroup);

                    assert(storeGroupToSplit->getNbInputs(1) == 0);
                    for(auto& inputs : storeGroupToSplit->getInputs(0)){
                        if(TestIfListsConnected(subStores, inputs->getData()->nodes, 0)){
                            GroupNode::Connect(groupNode, inputs, 0);
                        }
                    }

                    assert(groupNode->getNbInputs(0) >= 1);
                    extraStoreGroup.emplace_back(groupNode);
                }

                assert(storeGroupToSplit->getNbInputs(1) == 0);
                auto oldInputs = storeGroupToSplit->getInputs(0);
                for(auto& input : oldInputs){
                    assert(storeGroupToSplit->nodeInInputs(input,0));
                    if(!TestIfListsConnected(storeGroupToSplit->getData()->nodes,
                                                input->getData()->nodes, 0)){
                        GroupNode::Disconnect(storeGroupToSplit, input, 0);
                    }
                }
            }
        }
        storeGroups.insert(storeGroups.end(), extraStoreGroup.begin(), extraStoreGroup.end());
    }

    void SplitOperationsNaive(std::vector<std::shared_ptr<GroupNode>>* inLoadGroups,
                              const int VEC_SIZE){
        auto& loadGroups = (*inLoadGroups);
        std::queue<std::shared_ptr<GroupNode>> groupNodesToProceed;
        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        int nbGroupOperations = 0;
        std::set<GroupNode*> alreadyDone;
        while(groupNodesToProceed.size()){
            std::shared_ptr<GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                for(auto& output : currentNode->getOutputs()){
                    groupNodesToProceed.push(output);
                }

                if(currentNode->getData()->isGroupOperation()){
                    nbGroupOperations+=1;
                    if(int(currentNode->getData()->nodes.size()) > VEC_SIZE){
                        int idxFirst= (int(currentNode->getData()->nodes.size()-1)/VEC_SIZE)*VEC_SIZE;
                        while(VEC_SIZE <= idxFirst){
                            int idxLast = int(currentNode->getData()->nodes.size());
                            std::vector<std::shared_ptr<Node>> subOperations;
                            subOperations.resize(idxLast-idxFirst);
                            while(idxFirst < idxLast){
                                subOperations[idxLast-idxFirst-1] = currentNode->getData()->nodes.back();
                                currentNode->getData()->nodes.pop_back();
                                idxLast -= 1;
                            }
                            idxLast = idxFirst;
                            idxFirst -= VEC_SIZE;
                            assert(subOperations.size());
                            // Create the new operation group
                            auto newGroup = Autovec::GroupInstruction::make_group_operation(subOperations);
                            std::shared_ptr<GroupNode> groupNode(new GroupNode);
                            groupNode->getData() = std::move(newGroup);
                            nbGroupOperations+=1;

                            for(auto& output : currentNode->getOutputs()){
                                if(TestIfListsConnected(output->getData()->nodes,
                                                        subOperations, 0)){
                                    GroupNode::Connect(output, groupNode, 0);
                                }
                                if(TestIfListsConnected(output->getData()->nodes,
                                                        subOperations, 1)){
                                    GroupNode::Connect(output, groupNode, 1);
                                }
                            }

                            for(int idxInput = 0 ; idxInput < 2 ; ++idxInput){
                                for(auto& inputs : currentNode->getInputs(idxInput)){
                                    if(TestIfListsConnected(subOperations, inputs->getData()->nodes, idxInput)){
                                        GroupNode::Connect(groupNode, inputs, idxInput);
                                    }
                                }
                            }

                            assert(groupNode->getNbInputs(0) >= 1);
                            assert(groupNode->getNbOutputs() >= 1);
                        }

                        auto oldOutputs = currentNode->getOutputs();
                        for(auto output : oldOutputs){
                            assert(output->nodeInInputs(currentNode,0)
                                    || output->nodeInInputs(currentNode,1));
                            if(!TestIfListsConnected(output->getData()->nodes,
                                                    currentNode->getData()->nodes, 0)
                                    && output->nodeInInputs(currentNode,0)){
                                GroupNode::Disconnect(output, currentNode, 0);
                            }
                            if(!TestIfListsConnected(output->getData()->nodes,
                                                    currentNode->getData()->nodes, 1)
                                    && output->nodeInInputs(currentNode,1)){
                                GroupNode::Disconnect(output, currentNode, 1);
                            }
                        }

                        for(int idxInput = 0 ; idxInput < 2 ; ++idxInput){
                            auto oldInputs = currentNode->getInputs(idxInput);
                            for(auto& input : oldInputs){
                                assert(currentNode->nodeInInputs(input,0)
                                        || currentNode->nodeInInputs(input,1));
                                if(!TestIfListsConnected(currentNode->getData()->nodes,
                                                            input->getData()->nodes, 0)
                                        && currentNode->nodeInInputs(input,0)){
                                    GroupNode::Disconnect(currentNode, input, 0);
                                }
                                if(!TestIfListsConnected(currentNode->getData()->nodes,
                                                            input->getData()->nodes, 1)
                                        && currentNode->nodeInInputs(input,1)){
                                    GroupNode::Disconnect(currentNode, input, 1);
                                }
                            }
                        }
                    }
                    else{
                        assert(currentNode->getNbInputs(0) >= 1);
                        assert(currentNode->getNbOutputs() >= 1);
                    }
                }
            }
        }
    }

    void SplitLoadsWithSplitConf(std::vector<std::shared_ptr<GroupNode>>* inLoadGroups, const std::vector<std::vector<int>>& splitConfig){
        std::vector<std::shared_ptr<Autovec::GroupNode>> loadGroupsSplitted;
        auto& loadGroups = (*inLoadGroups);
        int idxGroup = 0;

        // iterate over the groups of Load
        for(auto& loadGroupToSplit : loadGroups){
            assert(loadGroupToSplit->getData()->isGroupLoadOrSet());

            // sorting loads in the same group with the offset
            if(loadGroupToSplit->getData()->isGroupLoad()){
                std::sort(std::begin(loadGroupToSplit->getData()->nodes), std::end(loadGroupToSplit->getData()->nodes),
                            [](auto& n1, auto& n2){
                    assert(n1->getData()->getScalar().getLoad().ptr == n2->getData()->getScalar().getLoad().ptr);
                    return n1->getData()->getScalar().getLoad().offset < n2->getData()->getScalar().getLoad().offset;
                });
            }

            int idxFirst = 0;

            for(int pickedSize: splitConfig[idxGroup]){
                const int idxLast = idxFirst + pickedSize;
                assert(idxLast <= int(loadGroupToSplit->getData()->nodes.size()));
                std::vector<std::shared_ptr<Autovec::Node>> subLoads;
                for(int idxNode = idxFirst ; idxNode < idxLast ; ++idxNode){
                    subLoads.emplace_back(loadGroupToSplit->getData()->nodes[idxNode]);
                }
                idxFirst = idxLast;

                // Create the new load group
                auto newGroup = (loadGroupToSplit->getData()->isGroupLoad()?
                                                         Autovec::GroupInstruction::make_group_load(subLoads)
                                                       : Autovec::GroupInstruction::make_group_set(subLoads));
                std::shared_ptr<Autovec::GroupNode> groupNode(new Autovec::GroupNode);
                groupNode->getData() = std::move(newGroup);

                // The new groupNode created from the splited subLoads
                // Iterate over the outputs of the load group to split to connect them with the new one
                for(auto& output : loadGroupToSplit->getOutputs()){
                    if(TestIfListsConnected(output->getData()->nodes,
                                            subLoads, 0)){
                        Autovec::GroupNode::Connect(output, groupNode, 0);
                    }
                    if(TestIfListsConnected(output->getData()->nodes,
                                            subLoads, 1)){
                        Autovec::GroupNode::Connect(output, groupNode, 1);
                    }
                }
                assert(groupNode->getNbOutputs() >= 1);
                loadGroupsSplitted.emplace_back(groupNode);
            }
            assert(idxFirst == int(loadGroupToSplit->getData()->nodes.size()));
            loadGroupToSplit->getData()->nodes.clear();

            auto oldOutputs = loadGroupToSplit->getOutputs();
            for(auto output : oldOutputs){
                assert(output->nodeInInputs(loadGroupToSplit,0)
                        || output->nodeInInputs(loadGroupToSplit,1));
                if(!TestIfListsConnected(output->getData()->nodes,
                                        loadGroupToSplit->getData()->nodes, 0)
                        && output->nodeInInputs(loadGroupToSplit,0)){
                    Autovec::GroupNode::Disconnect(output, loadGroupToSplit, 0);
                }
                if(!TestIfListsConnected(output->getData()->nodes,
                                        loadGroupToSplit->getData()->nodes, 1)
                        && output->nodeInInputs(loadGroupToSplit,1)){
                    Autovec::GroupNode::Disconnect(output, loadGroupToSplit, 1);
                }
            }
            idxGroup += 1;
        }///## end loop over groups of loads
        loadGroups.clear();
        loadGroups.insert(loadGroups.begin(), loadGroupsSplitted.begin(), loadGroupsSplitted.end());
    }

    void SplitStoresWithSplitConf(std::vector<std::shared_ptr<Autovec::GroupNode>>* inStoreGroups,
                  const std::vector<std::vector<int>>& splitConfig,
                  const int inOffsetStore){
        auto& storeGroups = (*inStoreGroups);
        std::vector<std::shared_ptr<Autovec::GroupNode>> storeGroupsSplitted;

        int idxGroup = inOffsetStore;

        for(auto& storeGroupToSplit : storeGroups){
            std::sort(std::begin(storeGroupToSplit->getData()->nodes), std::end(storeGroupToSplit->getData()->nodes),
                        [](auto& n1, auto& n2){
                assert(n1->getData()->getScalar().getStore().ptr == n2->getData()->getScalar().getStore().ptr);
                return n1->getData()->getScalar().getStore().offset < n2->getData()->getScalar().getStore().offset;
            });


            int idxFirst = 0;

            for(int pickedSize: splitConfig[idxGroup]){
                const int idxLast = idxFirst + pickedSize;
                assert(idxLast <= int(storeGroupToSplit->getData()->nodes.size()));
                std::vector<std::shared_ptr<Autovec::Node>> subStores;
                for(int idxNode = idxFirst ; idxNode < idxLast; ++idxNode){
                    subStores.emplace_back(storeGroupToSplit->getData()->nodes[idxNode]);
                }
                idxFirst = idxLast;

                // Create the new load group
                auto newGroup = Autovec::GroupInstruction::make_group_store(subStores);
                std::shared_ptr<Autovec::GroupNode> groupNode(new Autovec::GroupNode);
                groupNode->getData() = std::move(newGroup);

                assert(storeGroupToSplit->getNbInputs(1) == 0);
                for(auto& inputs : storeGroupToSplit->getInputs(0)){
                    if(TestIfListsConnected(subStores, inputs->getData()->nodes, 0)){
                        Autovec::GroupNode::Connect(groupNode, inputs, 0);
                    }
                }

                assert(groupNode->getNbInputs(0) >= 1);
                storeGroupsSplitted.emplace_back(groupNode);
            }
            assert(idxFirst == int(storeGroupToSplit->getData()->nodes.size()));
            storeGroupToSplit->getData()->nodes.clear();

            assert(storeGroupToSplit->getNbInputs(1) == 0);
            auto oldInputs = storeGroupToSplit->getInputs(0);
            for(auto& input : oldInputs){
                assert(storeGroupToSplit->nodeInInputs(input,0));
                if(!TestIfListsConnected(storeGroupToSplit->getData()->nodes,
                                            input->getData()->nodes, 0)){
                    Autovec::GroupNode::Disconnect(storeGroupToSplit, input, 0);
                }
            }
            idxGroup += 1;
        }// end loop on groups of stores
        storeGroups.clear();
        storeGroups.insert(storeGroups.begin(), storeGroupsSplitted.begin(), storeGroupsSplitted.end());
    }

    void TreeToDot(std::vector<std::shared_ptr<Autovec::GroupNode>>& inInputs, const std::string outFilename,
                   const bool lightGraph = false){
        std::ofstream dotFile;
        dotFile.open (outFilename);
        if(!dotFile.is_open()){
            std::cout << "Error when tried to create file " << outFilename << ", abort..." << std::endl;
            return;
        }

        dotFile << "digraph G {\n";

        std::vector<std::shared_ptr<GroupNode>> allNodes = Autovec::GetAllNodesFromInputs(inInputs);

        std::unordered_map<const GroupNode*, int> currentNodeId;
        for(auto currentNode : allNodes){
            currentNodeId[currentNode.get()] = int(currentNodeId.size());
        }

        for(auto currentNode : allNodes){
            for(int idxInputType = 0 ; idxInputType < 2 ; ++idxInputType){
                for(auto& input : currentNode->getInputs(idxInputType)){
                    assert(currentNodeId.find(input.get()) != currentNodeId.end());
                    dotFile << "\t" << currentNodeId[input.get()]  << " -> " << currentNodeId[currentNode.get()] << "\n";
                }
            }

            std::ostringstream label;
            label << currentNode->getData()->getDescription();
            for(auto& subnd : currentNode->getData()->nodes){
                label << "\\l" << subnd->getData()->getDescription(lightGraph);
            }

            dotFile << "\t" << currentNodeId[currentNode.get()] << " [label=\"" << label.str() << "\"];\n";

        }

        dotFile << "}\n";

        dotFile.close();
    }

    void PermuteExtract(std::vector<std::shared_ptr<GroupNode>>* inLoadGroups,
                        const int VEC_SIZE){
        auto& loadGroups = (*inLoadGroups);
        // Do it naively, take the operation in order,
        // take the element in it in order of their source
        // Create the permutation/insertion/extraction
        // Order from loads to store by propagation.
        std::unordered_map<const GroupNode*, int> distanceFromInputs = GetMaxDistanceFromInputsGroups(*inLoadGroups);

        auto cmpGroupNode = [&distanceFromInputs](const std::shared_ptr<GroupNode>& n1, const std::shared_ptr<GroupNode>& n2){
            assert(distanceFromInputs.find(n1.get()) != distanceFromInputs.end());
            assert(distanceFromInputs.find(n2.get()) != distanceFromInputs.end());
            return distanceFromInputs[n1.get()] > distanceFromInputs[n2.get()];
        };
        std::priority_queue<std::shared_ptr<GroupNode>, std::vector<std::shared_ptr<GroupNode>>, decltype(cmpGroupNode)> groupNodesToProceed(cmpGroupNode);

        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        std::set<GroupNode*> alreadyDone;
        while(groupNodesToProceed.size()){
            std::shared_ptr<GroupNode> currentNode = groupNodesToProceed.top();
            groupNodesToProceed.pop();

            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());
                assert(int(currentNode->getData()->nodes.size()) <= VEC_SIZE);

                for(auto& output : currentNode->getOutputs()){
                    assert(distanceFromInputs.find(output.get()) != distanceFromInputs.end());
                    assert(distanceFromInputs[output.get()] > distanceFromInputs[currentNode.get()]);
                    assert(!output->getData()->nodes[0]->getData()->isVectorial());
                    groupNodesToProceed.push(output);
                }

                // Skip permute, extract, etc.
                if(currentNode->getData()->isGroupStore() || currentNode->getData()->isGroupOperation()){
                    assert(currentNode->getData()->isGroupOperation() || currentNode->getNbInputs(1) == 0);
                    assert(currentNode->getNbInputs(0) != 0);

                    // Check if store is going to stay scalar and in this case don't extract
                    if(currentNode->getData()->isGroupStore() && currentNode->getData()->nodes.size() == 1)
                    {
                        bool staysScalar = true;
                        const auto& input = currentNode->getData()->nodes.front()->getInputs(0).front();
                        if(input->getData()->isScalarOperation()
                            && input->getData()->getScalar().getOperation().operation.rfind("Reduction", 0) == 0)
                        {
                            for(const auto& reductionOutput : input->getOutputs())
                            {
                                if(!reductionOutput->getData()->isScalarStore())
                                    staysScalar = false;
                            }
                        }

                        if(staysScalar)
                            continue;
                    }

                    const auto& frontNodeData = currentNode->getData()->nodes.front()->getData();
                    const bool isReduction = frontNodeData->isScalarOperation()
                        && frontNodeData->getScalar().getOperation().operation.rfind("Reduction ", 0) == 0;
                    int startingLimite = 0;
                    int endingLimite = (currentNode->getNbInputs(1) && !isReduction ? 1 : 0);

                    // If one of the source is unique we will use its ordering
                    // for example operation(load(x), load(y)) has two unique sources
                    // but operation([load(x), load(z), [v0, v1]) has two sources for both input
                    // thus in the second case we cannot directly obtain an ordering
                    if(currentNode->getNbInputs(0) == 1 || currentNode->getNbInputs(1) == 1){
                        const int workingIndex = (currentNode->getNbInputs(0) == 1 ? 0 : 1);

                        bool uniqueInputPerNode = true;
                        {
                            std::set<Node*> inputs;
                            for(const auto& cn : currentNode->getData()->nodes){
                                assert(cn->getInputs(workingIndex).size());
                                if(inputs.find(cn->getInputs(workingIndex).front().get())
                                        != inputs.end()){
                                    uniqueInputPerNode = false;
                                    break;
                                }
                                inputs.insert(cn->getInputs(workingIndex).front().get());
                            }
                        }
                        if(uniqueInputPerNode){
                            // Simply reorder to match other's actions
                            std::unordered_map<Node*, int> otherActionsOrder;
                            // Keep the order of input actions
                            for(auto& p1 : currentNode->getInputs(workingIndex).front()->getData()->nodes){
                                assert(otherActionsOrder.find(p1.get()) == otherActionsOrder.end());
                                otherActionsOrder[p1.get()] = int(otherActionsOrder.size());
                            }
                            // Sort the current node to match this order
                            std::sort(std::begin(currentNode->getData()->nodes),
                                        std::end(currentNode->getData()->nodes),
                                        [&](auto& n1, auto& n2){
                                assert(otherActionsOrder.find(n1->getInputs(workingIndex).front().get()) != otherActionsOrder.end());
                                assert(otherActionsOrder.find(n2->getInputs(workingIndex).front().get()) != otherActionsOrder.end());
                                return otherActionsOrder[n1->getInputs(workingIndex).front().get()]
                                        < otherActionsOrder[n2->getInputs(workingIndex).front().get()];
                            });
                        }
                        else{
                            // Simply reorder to match other's actions
                            std::unordered_map<Node*, int> otherActionsOrder;
                            // Keep the order of input actions
                            for(auto& p1 : currentNode->getInputs(workingIndex).front()->getData()->nodes){
                                assert(otherActionsOrder.find(p1.get()) == otherActionsOrder.end());
                                otherActionsOrder[p1.get()] = int(otherActionsOrder.size());
                            }

                            std::vector<std::ptrdiff_t> permutIndices(VEC_SIZE);
                            std::fill(permutIndices.begin(), permutIndices.end(), -1);
                            int idxNode = 0;

                            for(const auto& cn : currentNode->getData()->nodes){
                                assert(otherActionsOrder.find(cn->getInputs(workingIndex).front().get()) != otherActionsOrder.end());
                                permutIndices[idxNode] = otherActionsOrder[cn->getInputs(workingIndex).front().get()];
                                idxNode += 1;
                            }

                            std::shared_ptr<Node> newNode(new Node);
                            newNode->getData() = Instruction::make_vectorial_operation_with_indices(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                                        "Permute", permutIndices);


                            auto newGroup = Autovec::GroupInstruction::make_group_selects(std::vector<std::shared_ptr<Node>>{newNode});
                            std::shared_ptr<GroupNode> groupNode(new GroupNode);
                            groupNode->getData() = std::move(newGroup);
                            auto p1Group = currentNode->getInputs(workingIndex).front();
                            GroupNode::Disconnect(currentNode, p1Group, workingIndex);
                            GroupNode::Connect(currentNode, groupNode, workingIndex);
                            GroupNode::Connect(groupNode, p1Group, 0);
                        }

                        // Consider current index is done only if the number of inputesors is equal to the nodes in the input group node
                        // Otherwise we need to extract the subset from the input group node (current index not done)
                        if(workingIndex == 0){
                            startingLimite = (currentNode->getData()->nodes.size() < currentNode->getInputs(workingIndex).front()->getData()->nodes.size() ? 0 : 1 );
                            //startingLimite = 1;
                        }
                        else{
                            endingLimite = (currentNode->getData()->nodes.size() < currentNode->getInputs(workingIndex).front()->getData()->nodes.size() ? 1 : 0 );
                            //endingLimite = 0;
                        }
                    }

                    // Now for input, we will permute/extract to macth the order of the current operation/load
                    // this order might have been implies by the previous section
                    for(int workingIndex = startingLimite ; workingIndex <= endingLimite ; ++workingIndex){
                        // Create one permutated vector per input and merge them
                        std::unordered_map<Node*, std::queue<int>> currentActionsOrder;
                        int order = 0;
                        for(auto& nd : currentNode->getData()->nodes){
                            currentActionsOrder[nd->getInputs(workingIndex).front().get()].push(order);
                            order++;
                        }

                        auto currentNodeInputs = currentNode->getInputs(workingIndex);
                        int predictNbExtract = 0;
                        for(auto p1Group : currentNodeInputs){
                            std::vector<std::ptrdiff_t> extractIndices(VEC_SIZE);
                            std::fill(extractIndices.begin(), extractIndices.end(), -1);

                            bool atLeastOneSet  = false;
                            bool extractInOrder = true;

                            for(int idxAction = 0 ; idxAction < int(p1Group->getData()->nodes.size()) ; ++idxAction){
                                if(currentActionsOrder.find(p1Group->getData()->nodes[idxAction].get()) != currentActionsOrder.end()){
                                    // Consider the case that a node is used more than once
                                    auto copy = currentActionsOrder[p1Group->getData()->nodes[idxAction].get()];
                                    if(!copy.empty()){
                                        const int idx = copy.front();
                                        copy.pop();
                                        extractIndices[idx] = idxAction;
                                        extractInOrder &= (idx == idxAction);
                                        atLeastOneSet = true;
                                    }
                                }
                            }
                            assert(atLeastOneSet);

                            // If the values are not in order (even if there is one source)
                            // or if there are more than one source, we know we need extract
                            if((currentNodeInputs.size() > 1 || !extractInOrder)){
                                predictNbExtract += 1;
                            }
                        }
                        const bool thereWillBeMerge = (predictNbExtract > 1);

                        std::vector<std::shared_ptr<GroupNode>> extractGroups;
                        for(auto p1Group : currentNodeInputs){
                            std::vector<std::ptrdiff_t> extractIndices(VEC_SIZE);
                            std::fill(extractIndices.begin(), extractIndices.end(), -1);

                            bool extractInOrder = true;
                            bool atLeastOneSet  = false;

                            for(int idxAction = 0 ; idxAction < int(p1Group->getData()->nodes.size()) ; ++idxAction){
                                if(currentActionsOrder.find(p1Group->getData()->nodes[idxAction].get()) != currentActionsOrder.end()){
                                    // Consider the case that a node is used more than once
                                    while(!currentActionsOrder[p1Group->getData()->nodes[idxAction].get()].empty()){
                                        const int idx = currentActionsOrder[p1Group->getData()->nodes[idxAction].get()].front();
                                        currentActionsOrder[p1Group->getData()->nodes[idxAction].get()].pop();
                                        extractIndices[idx] = idxAction;
                                        extractInOrder &= (idx == idxAction);
                                        atLeastOneSet  = true;
                                    }
                                }
                            }
                            assert(atLeastOneSet);

                            // If the values are not in order (even if there is one source)
                            // or if there are more than one source, we know we need extract
                            if((currentNodeInputs.size() > 1 || !extractInOrder)){
                                const bool doNotNeedExtract = ((p1Group->getData()->nodes.front()->getData()->isScalarOperation()
                                                            && p1Group->getData()->nodes.front()->getData()->getScalar().getOperation().operation.rfind("Reduction ", 0) == 0)
                                                            || p1Group->getData()->nodes.front()->getData()->isSet())
                                                            && !thereWillBeMerge;

                                if(doNotNeedExtract){
                                    extractGroups.emplace_back(p1Group);
                                }
                                else{
                                    std::shared_ptr<Node> newNode(new Node);
                                    newNode->getData() = Instruction::make_vectorial_operation_with_indices(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                                                "Extract", extractIndices);


                                    auto newGroup = Autovec::GroupInstruction::make_group_selects(std::vector<std::shared_ptr<Node>>{newNode});
                                    std::shared_ptr<GroupNode> groupNode(new GroupNode);
                                    groupNode->getData() = std::move(newGroup);
                                    GroupNode::Disconnect(currentNode, p1Group, workingIndex);
                                    GroupNode::Connect(currentNode, groupNode, workingIndex);
                                    GroupNode::Connect(groupNode, p1Group, 0);

                                    extractGroups.emplace_back(groupNode);
                                }
                            }
                        }

                        // Check that all current actions nodes are visited 
                        for(auto& nd : currentNode->getData()->nodes){
                            assert(currentActionsOrder[nd->getInputs(workingIndex).front().get()].empty());
                        }

                        if(extractGroups.size()){
                            while(extractGroups.size() != 1){
                                std::vector<std::shared_ptr<GroupNode>> intermediateExtractGroups;

                                for(int idxMerge = 0 ; idxMerge < int(extractGroups.size()-extractGroups.size()%2) ; idxMerge += 2){
                                    std::shared_ptr<Node> newNode(new Node);
                                    newNode->getData() = Instruction::make_vectorial_operation(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                                                "Merge");

                                    auto newGroup = Autovec::GroupInstruction::make_group_selects(std::vector<std::shared_ptr<Node>>{newNode});
                                    std::shared_ptr<GroupNode> groupNode(new GroupNode);
                                    groupNode->getData() = std::move(newGroup);
                                    GroupNode::Disconnect(currentNode, extractGroups[idxMerge], workingIndex);
                                    GroupNode::Disconnect(currentNode, extractGroups[idxMerge+1], workingIndex);

                                    GroupNode::Connect(currentNode, groupNode, workingIndex);
                                    GroupNode::Connect(groupNode, extractGroups[idxMerge], 0);
                                    GroupNode::Connect(groupNode, extractGroups[idxMerge+1], 1);

                                    intermediateExtractGroups.emplace_back(groupNode);
                                }
                                if(extractGroups.size()%2){
                                    intermediateExtractGroups.emplace_back(extractGroups.back());
                                }

                                extractGroups = std::move(intermediateExtractGroups);
                            }
                        }
                    }
                    assert(currentNode->getNbInputs(0) == 1);
                    assert((currentNode->getData()->isGroupStore() && currentNode->getNbOutputs() == 0 )
                            || (currentNode->getData()->isGroupOperation() && currentNode->getNbOutputs() >= 1 ));
                }
                else{
                        assert(currentNode->getData()->isGroupLoadOrSet());
                }
            }
        }
    }
    
    std::vector<std::shared_ptr<GroupNode>> GetOperationsGroups(const std::vector<std::shared_ptr<GroupNode>>& loadGroups){
        std::queue<std::shared_ptr<Autovec::GroupNode>> groupNodesToProceed;
        std::vector<std::shared_ptr<GroupNode>> operations;

        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        std::set<Autovec::GroupNode*> alreadyDone;
        while(groupNodesToProceed.size()){
            std::shared_ptr<Autovec::GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            // Current node not in alreadyDone
            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                // Add outputs of current node
                for(auto& output : currentNode->getOutputs()){
                    groupNodesToProceed.push(output);
                }
                if(currentNode->getData()->isGroupOperation()){
                    operations.emplace_back(currentNode);
                }
            }
        }
        return operations;
    }

    int CheckInputsInSameGroup(const std::vector<std::shared_ptr<Autovec::Node>>& inputs1,
                            const std::vector<std::shared_ptr<Autovec::Node>>& inputs2,
                            const std::vector<std::shared_ptr<Autovec::GroupNode>>& inputsGroups){
        int nbCommons = 0;
        for(auto& currentSource : inputsGroups){
            bool findIn1 = false;
            for(auto& s1 : inputs1){
                if(find(currentSource->getData()->nodes.begin(), currentSource->getData()->nodes.end(), s1) 
                    != currentSource->getData()->nodes.end()){
                    findIn1 = true;
                    break;
                }
            }
            if(findIn1){
            bool findIn2 = false;
            for(auto& s2 : inputs2){
                if(find(currentSource->getData()->nodes.begin(), currentSource->getData()->nodes.end(), s2) 
                    != currentSource->getData()->nodes.end()){
                    findIn2 = true;
                    break;
                }
            }
            if(findIn2){
                nbCommons += 1;
            }
            }
        }
        return nbCommons;
    }

    std::vector<std::shared_ptr<Autovec::GroupNode>> Intersection(const std::vector<std::shared_ptr<Autovec::Node>>& destination1, 
                                                            const std::vector<std::shared_ptr<Autovec::Node>>& destination2,
                                                            const std::vector<std::shared_ptr<Autovec::GroupNode>>& outputsGroups){
        std::vector<std::shared_ptr<Autovec::GroupNode>> mutualDestinations;
        std::shared_ptr<Autovec::GroupNode> dst1, dst2;

        for(auto& currentDestination : outputsGroups){
            for(int i=0; i < (int)destination1.size(); i++){
                if(find(currentDestination->getData()->nodes.begin(), currentDestination->getData()->nodes.end(), destination1[i]) 
                        != currentDestination->getData()->nodes.end()){
                    dst1 = currentDestination;
                    for(int j=0; j < (int)destination2.size(); j++){
                        if(find(currentDestination->getData()->nodes.begin(), currentDestination->getData()->nodes.end(), destination2[0]) 
                                != currentDestination->getData()->nodes.end()){
                            dst2 = currentDestination;
                            if(dst1 == dst2){
                            mutualDestinations.emplace_back(currentDestination);
                            }
                        }
                    }
                }
            }
        }
        return mutualDestinations;
    }

    double CalculateScore(const std::shared_ptr<Autovec::Node>& A, const std::shared_ptr<Autovec::Node>& B,
                          const std::shared_ptr<Autovec::GroupNode>& group, const int VEC_SIZE){
        double score = 0;

        if(A != B){
            // Check if they have same sources
            // Increment score respectively if (A.src1 == B.src1) and (A.src2 == B.src2)
            if(A->getNbInputs(0)>0 && B->getNbInputs(0)>0){
                score += CheckInputsInSameGroup(A->getInputs(0), B->getInputs(0), group->getInputs(0));
            }
            if(A->getNbInputs(1)>0 && B->getNbInputs(1)>0){
                score += CheckInputsInSameGroup(A->getInputs(1), B->getInputs(1), group->getInputs(1));
            }
            
        }else{
            score += A->getNbInputs(0) + A->getNbInputs(1);
        }

        // Are used at the same place (destination)
        std::vector<std::shared_ptr<Autovec::GroupNode>> destinations;
        destinations = Intersection(A->getOutputs(), B->getOutputs(), group->getOutputs());
        for(auto& d : destinations){
        if(d->getData()->isGroupStore()){
            score += 1;
        }
        if(d->getData()->isGroupOperation()){
            if(int(d->getData()->nodes.size()) <= VEC_SIZE){
                score += 1;
            }else{
                score += (double) 1/(int)d->getData()->nodes.size();
            }
        }
    }
        return score;
    }

    void PrintMatrix(const std::vector<std::vector<double>>&  M, int n){
        std::cout << "Info: Scores Matrix " << n << "x" << n << " for operations groups" << std::endl;
        for(int i=0; i<n; i++){
            std::cout << "  |    " ;
            for(int j=0; j<n; j++){
                std::cout << M[i][j] << "\t";
            }
            std::cout << "|" << std::endl;
        }
    }

    void PrintRes(const std::vector<std::vector<int>>& inSplit, const std::vector<std::vector<double>>& inScores){
        std::cout << "Info: Results of groups to split according to Scores Matrix" << std::endl;
        for(int idxVec = 0 ; idxVec < int(inSplit.size()) ; ++idxVec){
            std::cout << "------------------------------" << std::endl;
            std::cout << "Vec: " << idxVec << " of size " << inSplit[idxVec].size() << std::endl;
            std::cout << "Indices: ";
            for(auto& idx : inSplit[idxVec] ){
                std::cout << idx << " ";
            }
            std::cout << std::endl;


            for(int idx1 = 0 ; idx1 < int(inSplit[idxVec].size()) ; ++idx1){
                for(int idx2 = 0 ; idx2 < int(inSplit[idxVec].size()) ; ++idx2){
                    std::cout << inScores[inSplit[idxVec][idx1]][inSplit[idxVec][idx2]] << " ";
                }
                std::cout << std::endl;
            }
        }
        std::cout << "------------------------------" << std::endl;
    }

    std::vector<std::vector<int>> GetSplitOperationsConfig(const std::vector<std::vector<int>>& splittedMatrix){
        std::vector<std::vector<int>> splitConfigs;
        for(int idxVec = 0 ; idxVec < int(splittedMatrix.size()) ; ++idxVec){
            std::vector<int> splitConfig;
            for(auto& idx : splittedMatrix[idxVec] ){
                splitConfig.emplace_back(idx);
            }
            splitConfigs.emplace_back(splitConfig);
        }
        return splitConfigs;
    }

    std::vector<std::vector<double>> GenerateOperationsMatrix(const std::shared_ptr<Autovec::GroupNode>& operations, const int& numberOp,
                                                              const int VEC_SIZE){
        std::vector<std::vector<double>> scoresMat(numberOp, std::vector<double>(numberOp, 0.0));

        for(int i = 0; i<numberOp; i++){
            for(int j = 0; j<numberOp; j++){
                scoresMat[i][j] = CalculateScore(operations->getData()->nodes[i], operations->getData()->nodes[j], operations, VEC_SIZE);
            }
        }

        return scoresMat;   
    }

    int SearchPositionInGroup(const std::shared_ptr<Autovec::Node>& node, const std::vector<std::shared_ptr<Autovec::GroupNode>>& groups){
        for(auto& group : groups){
            int position = 0;
            for(auto& currentNode : group->getData()->nodes){
                if(currentNode == node){
                    return position;
                }
            position+=1;
            }
        }
        return -1;
    }

    std::shared_ptr<Autovec::GroupNode> SearchGroupInInputs(const std::shared_ptr<Autovec::Node>& node, const std::vector<std::shared_ptr<Autovec::GroupNode>>& groups){
        for(auto& group : groups){
            for(auto& currentNode : group->getData()->nodes){
                if(currentNode == node){
                    return group;
                }
            }
        }
        return std::shared_ptr<Autovec::GroupNode>();
    }

    std::vector<Operation> GenerateOperationStructure(const std::shared_ptr<Autovec::GroupNode>& operationGroup, const std::vector<int>& split, 
                                                        std::unordered_map<std::shared_ptr<Autovec::GroupNode>, int>* addrToId){
        std::vector<Operation> structeredOperations;

        for(int opIdx=0; opIdx < int(operationGroup->getData()->nodes.size()); opIdx++){
            auto inputs0 = operationGroup->getData()->nodes[opIdx]->getInputs(0);
            auto inputs1 = operationGroup->getData()->nodes[opIdx]->getInputs(1);

            assert(inputs0.size() == inputs1.size());
            for(int idx=0; idx<int(inputs0.size()); idx++){
                auto grp0 = SearchGroupInInputs(inputs0[idx], operationGroup->getInputs(0));
                if(addrToId->find(grp0) == addrToId->end()){
                    (*addrToId)[grp0] = int(addrToId->size());
                }
                auto grp1 = SearchGroupInInputs(inputs1[idx], operationGroup->getInputs(1));
                if(addrToId->find(grp1) == addrToId->end()){
                    (*addrToId)[grp1] = int(addrToId->size());
                }
                std::vector<std::pair<int,int>> idxsDep;
                idxsDep.emplace_back(std::make_pair((*addrToId)[grp0], SearchPositionInGroup(inputs0[idx], operationGroup->getInputs(0))));
                idxsDep.emplace_back(std::make_pair((*addrToId)[grp1], SearchPositionInGroup(inputs1[idx], operationGroup->getInputs(1))));
                structeredOperations.emplace_back(Operation{split[opIdx],idxsDep});
            }
        }

        return structeredOperations;
    }

    void SplitOperationsWithGivenIndices(std::vector<std::shared_ptr<GroupNode>>* inLoadGroups, const std::shared_ptr<Autovec::GroupNode>& operationGroup, 
                                        const std::vector<std::vector<int>>& splits, std::unordered_map<std::shared_ptr<Autovec::Node>, int>* mappingNodes,
                                        std::unordered_map<std::shared_ptr<GroupNode> , std::vector<int>>* addrGroupOpToSplit,
                                         const int VEC_SIZE){
        auto& loadGroups = (*inLoadGroups);
        std::queue<std::shared_ptr<Autovec::GroupNode>> groupNodesToProceed;

        std::unordered_map<std::shared_ptr<Autovec::GroupNode>, std::vector<Operation>> structeredOperations;

        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        std::set<Autovec::GroupNode*> alreadyDone;
        bool found = false;
        while(groupNodesToProceed.size() && !found){
            std::shared_ptr<Autovec::GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            // current node is not in already done
            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                // get outputs of current node (load)
                for(auto& output : currentNode->getOutputs()){
                    groupNodesToProceed.push(output);
                }

                if(currentNode->getData()->isGroupOperation() && currentNode == operationGroup){
                    found = true;
                    assert(currentNode->getNbInputs(0) >= 1);

                    for(std::vector<int> split : splits){

                        // Example: for a split configuration { 0 7 6 5 4 3 2 1 } 
                        // We have to extract this indices from the non splitted operation group

                        // First create the subGroup
                        std::vector<std::shared_ptr<Autovec::Node>> subGroup(VEC_SIZE);
                        int size = 0;

                        // We have to loop on nodes in operation group and if its index is in the list add it
                        for(int idxNode=0; idxNode<int(currentNode->getData()->nodes.size()); idxNode++){
                            // idxNode is the index of a node in the group operation to split
                            std::vector<int>::iterator it;
                            // find if the index of the node is in the current sub split 
                            it = std::find(split.begin(), split.end(), idxNode);
                            if(it != split.end()){
                                // extract the position of the index of the node
                                int index = int(it - split.begin());
                                // add to the sub group in this same position
                                subGroup[index] = currentNode->getData()->nodes[idxNode];
                                mappingNodes->emplace(std::make_pair(operationGroup->getData()->nodes[idxNode], *it));
                                size++;
                            }
                        }
                        subGroup.resize(size);
                        subGroup.shrink_to_fit();

                        // Create the new operation group
                        auto newGroup = Autovec::GroupInstruction::make_group_operation(subGroup);
                        std::shared_ptr<Autovec::GroupNode> groupNode(new Autovec::GroupNode);
                        groupNode->getData() = std::move(newGroup);

                        for(auto& output : currentNode->getOutputs()){
                            if(TestIfListsConnected(output->getData()->nodes,
                                                    subGroup, 0)){
                                Autovec::GroupNode::Connect(output, groupNode, 0);
                            }
                            if(TestIfListsConnected(output->getData()->nodes,
                                                    subGroup, 1)){
                                Autovec::GroupNode::Connect(output, groupNode, 1);
                            }
                        }

                        for(int idxInput = 0 ; idxInput < 2 ; ++idxInput){
                            for(auto& inputs : currentNode->getInputs(idxInput)){
                                if(TestIfListsConnected(subGroup, inputs->getData()->nodes, idxInput)){
                                    Autovec::GroupNode::Connect(groupNode, inputs, idxInput);
                                }
                            }
                        }

                        assert(groupNode->getNbInputs(0) >= 1);
                        assert(groupNode->getNbOutputs() >= 1);
                        
                        // Add the mapping between the group node and its indices from split configuration
                        (*addrGroupOpToSplit)[groupNode] = split;
                    }

                    auto oldOutputs = currentNode->getOutputs();
                    for(auto output : oldOutputs){
                        assert(output->nodeInInputs(currentNode,0)
                            || output->nodeInInputs(currentNode,1));
                        // the if test was "not listsConnected" changed to if "listsConnected"
                        if(TestIfListsConnected(output->getData()->nodes,
                                                currentNode->getData()->nodes, 0)
                                && output->nodeInInputs(currentNode,0)){
                            Autovec::GroupNode::Disconnect(output, currentNode, 0);
                        }
                        if(TestIfListsConnected(output->getData()->nodes,
                                                currentNode->getData()->nodes, 1)
                                && output->nodeInInputs(currentNode,1)){
                            Autovec::GroupNode::Disconnect(output, currentNode, 1);
                        }
                    }

                    for(int idxInput = 0 ; idxInput < 2 ; ++idxInput){
                        auto oldInputs = currentNode->getInputs(idxInput);
                        for(auto& input : oldInputs){
                            assert(currentNode->nodeInInputs(input,0)
                                || currentNode->nodeInInputs(input,1));
                            if(TestIfListsConnected(currentNode->getData()->nodes,
                                                    input->getData()->nodes, 0)
                                    && currentNode->nodeInInputs(input,0)){
                                Autovec::GroupNode::Disconnect(currentNode, input, 0);
                            }
                            if(TestIfListsConnected(currentNode->getData()->nodes,
                                                    input->getData()->nodes, 1)
                                    && currentNode->nodeInInputs(input,1)){
                                Autovec::GroupNode::Disconnect(currentNode, input, 1);
                            }
                        }
                    }
                    assert(currentNode->getNbInputs() == 0);
                }
            }
        }
    }

    void FixOperationsOrder(std::vector<std::shared_ptr<Autovec::GroupNode>> *loadGroups, std::shared_ptr<Autovec::GroupNode> operationGroup, 
                            const std::vector<Operation>& orderedOperations, const std::unordered_map<std::shared_ptr<Autovec::Node>, int>& mappingNodes){
        std::queue<std::shared_ptr<Autovec::GroupNode>> groupNodesToProceed;

        std::unordered_map<std::shared_ptr<Autovec::GroupNode>, std::vector<Operation>> structeredOperations;

        // We go backward and start by groups from load
        for(auto& iterGroup : *loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        std::set<Autovec::GroupNode*> alreadyDone;
        bool found = false;
        while(groupNodesToProceed.size() && !found){
            std::shared_ptr<Autovec::GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            // current node is not in already done
            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                // get outputs of current node (load)
                for(auto& output : currentNode->getOutputs()){
                    groupNodesToProceed.push(output);
                }

                if(currentNode->getData()->isGroupOperation() && currentNode == operationGroup){
                    found = true;
                    assert(operationGroup->getData()->nodes.size() == orderedOperations.size());
                    int currentPosition = 0;
                    for(auto operationNode : operationGroup->getData()->nodes){
                        auto currentNodeIt = mappingNodes.find(operationNode);
                        if(currentNodeIt != mappingNodes.end()){
                            int nodeId = currentNodeIt->second;
                            int newPosition = 0;
                            for(auto currentOperation :  orderedOperations){
                                if(nodeId == currentOperation.idOperation){
                                    iter_swap(currentNode->getData()->nodes.begin() + currentPosition, currentNode->getData()->nodes.begin() + newPosition);
                                }
                                newPosition+=1;
                            }
                        }
                        currentPosition+=1;
                    }
                }
            }
        }
    }

    using SplitFunc = std::function<std::vector<std::vector<int>>(std::vector<std::vector<double>>,int,bool)>;

    void SplitOperationsScoreMatrix(std::vector<std::shared_ptr<GroupNode>>* loadGroups, 
                                    const bool inVerbose, SplitFunc SplitStrategy, bool parameter,
                                    const int VEC_SIZE){
        std::vector<std::shared_ptr<Autovec::GroupNode>> operationGroups;
        
        // Get the list of group operations not splitted yet, grouped by same operation at the same level
        operationGroups = GetOperationsGroups(*loadGroups);
        
        if(inVerbose){
            std::cout << "Info: There is " << operationGroups.size() << " group operations to split" << std::endl;
        }

        std::unordered_map<std::shared_ptr<Autovec::GroupNode>, std::vector<Operation>> structeredOperationsGroups;
        std::unordered_map<std::shared_ptr<Autovec::Node>, int> mappingNodes;
        std::unordered_map<std::shared_ptr<Autovec::GroupNode>, int> addrGroupOpToId;
        std::unordered_map<std::shared_ptr<GroupNode>, std::vector<int>> addrGroupOpToSplit;

        // for each operations group generate a matrix
        for(auto& operationGroup : operationGroups){
            std::vector<std::vector<double>> matrix;
            int numberOp = (int) operationGroup->getData()->nodes.size();   
            matrix = GenerateOperationsMatrix(operationGroup, numberOp, VEC_SIZE);
            
            if(inVerbose){
                PrintMatrix(matrix, numberOp);
            }

            // Stategy to extract split config from the scores' matrix
            std::vector<std::vector<int>> splittedMatrix = SplitStrategy(matrix, VEC_SIZE, parameter);
            std::vector<std::vector<int>> splits = GetSplitOperationsConfig(splittedMatrix);

            SplitOperationsWithGivenIndices(loadGroups, operationGroup, splits, &mappingNodes, &addrGroupOpToSplit, VEC_SIZE);

            if(inVerbose){
                PrintRes(splittedMatrix, matrix);
            }
        }

        // Update the list of group operations AFTER SPLIT
        operationGroups = GetOperationsGroups(*loadGroups);

        // Don't apply any split optimizations on reduction operations
        operationGroups.erase(std::remove_if(operationGroups.begin(),
                                             operationGroups.end(),
                                             [](auto& group){ return group->getData()->nodes.front()->getData()->getScalar().getOperation().operation.rfind("Reduction ", 0) == 0; }),
                                operationGroups.end());

        for(auto& operationGroup : operationGroups){
            structeredOperationsGroups[operationGroup] = GenerateOperationStructure(operationGroup, addrGroupOpToSplit[operationGroup], &addrGroupOpToId);
        }

        assert(structeredOperationsGroups.size() == operationGroups.size());
        for(auto& [groupNode, operations]: structeredOperationsGroups){
            if(inVerbose){
                std::cout << "Info: Finding the order for minimum extracts" << std::endl;
                std::cout << "Input:" << std::endl;
                printOperations(operations);
                std::cout << "=> need " << countExtract(operations) << " extracts." << std::endl;
            }

            auto res = GenerateOrder(operations);
            assert(res.size() == operations.size());

            if(inVerbose){
                std::cout << "Output:" << std::endl;
                printOperations(res);
                std::cout << "=> need " << countExtract(res) << " extracts." << std::endl;
            }

            FixOperationsOrder(loadGroups, groupNode, res, mappingNodes);

            if(inVerbose){
                std::cout << "Info: After fixing the operations order\n";
                int currentPosition = 0;
                for(auto operationNode : groupNode->getData()->nodes){
                    std::cout << "The node (" << mappingNodes.find(operationNode)->second << ") new current position is " << currentPosition << std::endl;
                    currentPosition+=1;
                }
            }
        }
    }

    std::vector<std::shared_ptr<Node>> const VectorialGraph(const std::vector<std::shared_ptr<GroupNode>>& loadGroups,
                                                            const int VEC_SIZE){
        std::vector<std::shared_ptr<Node>> newOutputs;
        std::unordered_map<GroupNode*, std::shared_ptr<Node>> groupNodeToNode;
        std::queue<std::shared_ptr<GroupNode>> groupNodesToProceed;
        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        std::set<GroupNode*> alreadyDone;
        while(groupNodesToProceed.size()){
            std::shared_ptr<GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                for(auto& iterGroup : currentNode->getOutputs()){
                    assert(iterGroup->nodeInAnyInputs(currentNode));
                    groupNodesToProceed.push(iterGroup);
                }

                assert(currentNode->getData()->nodes.size());

                // Replace scalar per vectorial
                if(currentNode->getData()->isGroupLoad()){
                    assert(currentNode->getNbOutputs() >= 1);
                    assert(currentNode->getNbInputs(0) == 0);
                    assert(currentNode->getNbInputs(1) == 0);
                    assert(currentNode->getData()->nodes.size());
                    assert(currentNode->getData()->nodes.front()->getData()->isScalar());
                    std::shared_ptr<Node> newNode(new Node);

                    std::ptrdiff_t ptr = currentNode->getData()->nodes.front()->getData()->getScalar().getLoad().ptr;

                    std::vector<std::ptrdiff_t> offset(VEC_SIZE);
                    std::fill(offset.begin(), offset.end(), -1);
                    for(int idxNode = 0 ; idxNode < int(currentNode->getData()->nodes.size()) ; ++idxNode){
                        offset[idxNode] = currentNode->getData()->nodes[idxNode]->getData()->getScalar().getLoad().offset;
                    }

                    newNode->getData() = Instruction::make_vectorial_load(currentNode->getData()->nodes.front()->getData()->getScalar().type, ptr, offset);

                    groupNodeToNode[currentNode.get()] = newNode;
                }
                else if(currentNode->getData()->isGroupSet()){
                    assert(currentNode->getNbOutputs() >= 1);
                    assert(currentNode->getNbInputs(0) == 0);
                    assert(currentNode->getNbInputs(1) == 0);
                    assert(currentNode->getData()->nodes.size());
                    assert(currentNode->getData()->nodes.front()->getData()->isScalar());
                    std::shared_ptr<Node> newNode(new Node);
                    newNode->getData() = Instruction::make_vectorial_set(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                         currentNode->getData()->nodes.front()->getData()->getScalar().getSet().value);
                    groupNodeToNode[currentNode.get()] = newNode;
                }
                else if(currentNode->getData()->isGroupStore()){
                    assert(currentNode->getNbOutputs() == 0);
                    assert(currentNode->getNbInputs(0) == 1);
                    assert(currentNode->getNbInputs(1) == 0);
                    assert(currentNode->getData()->nodes.size());
                    assert(currentNode->getData()->nodes.front()->getData()->isScalar());

                    bool staysScalar = false;
                    const auto& input = currentNode->getData()->nodes.front()->getInputs(0).front();
                    if(currentNode->getData()->nodes.size() == 1
                        && input->getData()->isScalarOperation()
                        && input->getData()->getScalar().getOperation().operation.rfind("Reduction", 0) == 0)
                    {
                        staysScalar = true;
                        for(const auto& reductionOutput : input->getOutputs())
                        {
                            if(!reductionOutput->getData()->isScalarStore())
                                staysScalar = false;
                        }
                    }

                    const auto firstNodeData = currentNode->getData()->nodes.front()->getData();
                    std::shared_ptr<Node> newNode(new Node);
                    std::ptrdiff_t ptr = firstNodeData->getScalar().getStore().ptr;
                    auto type = firstNodeData->getScalar().type;

                    if(!staysScalar)
                    {
                        std::vector<std::ptrdiff_t> offset(VEC_SIZE);
                        std::fill(offset.begin(), offset.end(), -1);
                        for(int idxNode = 0 ; idxNode < int(currentNode->getData()->nodes.size()) ; ++idxNode){
                            offset[idxNode] = currentNode->getData()->nodes[idxNode]->getData()->getScalar().getStore().offset;
                        }

                        newNode->getData() = Instruction::make_vectorial_store(type, ptr, offset);
                    }
                    else
                    {
                        newNode->getData() = Instruction::make_scalar_store(type, ptr, firstNodeData->getScalar().getStore().offset);
                    }
                    groupNodeToNode[currentNode.get()] = newNode;
                }
                else if(currentNode->getData()->isGroupOperation()){
                    assert(currentNode->getNbOutputs() >= 1);
                    assert(currentNode->getNbInputs(0) == 1);
                    assert(currentNode->getNbInputs(1) <= 1);
                    assert(currentNode->getData()->nodes.size());
                    std::shared_ptr<Node> newNode(new Node);
                    if(currentNode->getData()->nodes.front()->getData()->isScalar()){
                        if(currentNode->getData()->nodes.front()->getData()->isScalarOperation()
                            && currentNode->getData()->nodes.front()->getData()->getScalar().getOperation().operation.rfind("Reduction", 0) == 0)
                        {
                            newNode->getData() = Instruction::make_vectorial_reduction_operation(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                                                    currentNode->getData()->nodes.front()->getData()->getScalar().getOperation().operation,
                                                                                                    currentNode->getData()->nodes.size());
                        }
                        else
                        {
                            newNode->getData() = Instruction::make_vectorial_operation(currentNode->getData()->nodes.front()->getData()->getScalar().type,
                                                                                        currentNode->getData()->nodes.front()->getData()->getScalar().getOperation().operation);
                        }
                    }
                    else{
                        newNode->getData() = currentNode->getData()->nodes.front()->getData();
                    }
                    groupNodeToNode[currentNode.get()] = newNode;
                }
                else{
                    assert(0);
                }
            }
        }
        // Connect the vec nodes and fill newOutputs
        assert(groupNodesToProceed.size() == 0);
        // We go backward and start by groups from load
        for(auto& iterGroup : loadGroups){
            groupNodesToProceed.push(iterGroup);
        }

        alreadyDone.clear();
        while(groupNodesToProceed.size()){
            std::shared_ptr<GroupNode> currentNode = groupNodesToProceed.front();
            groupNodesToProceed.pop();

            if(alreadyDone.find(currentNode.get()) == alreadyDone.end()){
                alreadyDone.insert(currentNode.get());

                for(auto& iterGroup : currentNode->getOutputs()){
                    assert(iterGroup->nodeInAnyInputs(currentNode));
                    groupNodesToProceed.push(iterGroup);
                }

                assert(currentNode->getData()->nodes.size());

                // Replace scalar per vectorial
                if(currentNode->getData()->isGroupLoadOrSet()){
                    assert(currentNode->getNbOutputs() >= 1);
                    assert(currentNode->getNbInputs(0) == 0);
                    assert(currentNode->getNbInputs(1) == 0);
                }
                else if(currentNode->getData()->isGroupStore()){
                    assert(currentNode->getNbOutputs() == 0);
                    assert(currentNode->getNbInputs(0) == 1);
                    assert(currentNode->getNbInputs(1) == 0);
                    assert(groupNodeToNode.find(currentNode->getInputs(0).front().get()) != groupNodeToNode.end());
                    Node::Connect(groupNodeToNode[currentNode.get()], groupNodeToNode[currentNode->getInputs(0).front().get()], 0);
                    newOutputs.emplace_back(groupNodeToNode[currentNode.get()]);
                }
                else if(currentNode->getData()->isGroupOperation()){
                    assert(currentNode->getNbOutputs() >= 1);
                    assert(currentNode->getNbInputs(0) == 1);
                    assert(currentNode->getNbInputs(1) <= 1);
                    assert(groupNodeToNode.find(currentNode->getInputs(0).front().get()) != groupNodeToNode.end());
                    Node::Connect(groupNodeToNode[currentNode.get()], groupNodeToNode[currentNode->getInputs(0).front().get()], 0);
                    if(currentNode->getNbInputs(1)){
                        assert(groupNodeToNode.find(currentNode->getInputs(1).front().get()) != groupNodeToNode.end());
                        Node::Connect(groupNodeToNode[currentNode.get()], groupNodeToNode[currentNode->getInputs(1).front().get()], 1);
                    }
                }
                else{
                    assert(0);
                }
            }
        }
        return newOutputs;
    }

    std::vector<std::shared_ptr<Node>> VectorizeWithSplitConf(std::vector<std::shared_ptr<GroupNode>>* loadGroups,
                                            std::vector<std::shared_ptr<GroupNode>>* storeGroups,
                                            const std::vector<std::vector<int>>& splitConfig,
                                            const Config& config){
        // get number of load groups before splitting them
        const int nbLoadGroups = int((*loadGroups).size());

        SplitLoadsWithSplitConf(loadGroups, splitConfig);
        SplitStoresWithSplitConf(storeGroups, splitConfig, nbLoadGroups);

        switch (config.splitOperations)
        {
        case WithoutOrder :
            if(config.verbose){
                std::cout << "2) Do naive split operations.\n";
            }
            SplitOperationsNaive(loadGroups, config.vecsize);
            break;

        case VecTogetherTrue :
            if(config.verbose){
                std::cout << "2) Do VecTogetherTrue split operations.\n";
            }
            SplitOperationsScoreMatrix(loadGroups, config.verbose, GetSubOperationsVecTogether, true, config.vecsize);
            break;

        case VecTogetherFalse :
            if(config.verbose){
                std::cout << "2) Do VecTogetherFalse split operations.\n";
            }
            SplitOperationsScoreMatrix(loadGroups, config.verbose, GetSubOperationsVecTogether, false, config.vecsize);
            break;

        case VecOneByOneTrue :
            if(config.verbose){
                std::cout << "2) Do VecOneByOneTrue split operations.\n";
            }
            SplitOperationsScoreMatrix(loadGroups, config.verbose, GetSubOperations1by1, true, config.vecsize);
            break;

        case VecOneByOneFalse :
            if(config.verbose){
                std::cout << "2) Do VecOneByOneFalse split operations.\n";
            }
            SplitOperationsScoreMatrix(loadGroups, config.verbose, GetSubOperations1by1, false, config.vecsize);
            break;

        default:
            if(config.verbose){
                std::cout << "<> Error: no valid configuration for splitting operations.\n";
            }
            break;
        }

        // Now we have actions of correct size, but we need to
        // ensure coherency between the indices and add permutation/extraction/insertion
        PermuteExtract(loadGroups, config.vecsize);

        // Convert each group into a vec instruction
        // Return the new graph
        return VectorialGraph(*loadGroups, config.vecsize);
    }

    std::vector<std::shared_ptr<Node>> VectorizeGreedy(std::vector<std::shared_ptr<Node>>* outputs,
                                                           const Config& inConfig){

        if(inConfig.constPropagationEnabled){
            ConstantPropagation(outputs);
        }

        if(inConfig.removeDuplicate){
            RemoveDuplicate(outputs);
        }

        std::vector<SplitOperations> splitOps;
        if(inConfig.splitOperations == GreedySplitOp){
            splitOps = std::vector<SplitOperations>{WithoutOrder, VecTogetherTrue, VecTogetherFalse, VecOneByOneTrue, VecOneByOneFalse};
        }
        else{
            splitOps.push_back((SplitOperations)inConfig.splitOperations);
        }

        std::vector<std::vector<int>> bestSplitConfig;
        int bestSplitNbNodes = std::numeric_limits<int>::max();
        std::vector<std::shared_ptr<Node>> bestVecOutputs;

        std::vector<std::shared_ptr<Node>> copyOutputs = DuplicateGraph(*outputs);

        Config config = inConfig;

        std::vector<bool> reductionStates;
        if(config.reductionMode == EnableReduction){
            reductionStates.push_back(true);
        }
        else if(config.reductionMode == DisableReduction){
            reductionStates.push_back(false);
        }
        else{
            reductionStates.push_back(false);
            reductionStates.push_back(true);
        }

        for(auto useReduc : reductionStates){ // Watch out must be false then true
            if(config.verbose){
                std::cout << "Use reduction = " << (useReduc?"TRUE":"FALSE") << std::endl;
            }
            config.reductionMode = (useReduc ? EnableReduction : DisableReduction);

            if(config.reductionMode == EnableReduction){// Watch out will transform the graph
                const bool foundReduction = CreateReductionTrees(&copyOutputs, 1, config.vecsize);

                // Skip retrying the same configurations if there was no reduction found
                if(!foundReduction && reductionStates.size() == 2){
                    if(config.verbose){
                        std::cout << "No reduction found, configurations won't change in this mode" << std::endl;
                    }                    
                    continue;
                }
            }

            for(auto splitOp : splitOps){
                if(config.verbose){
                    std::cout << "Use splitOp = " << splitOp << std::endl;
                }
                config.splitOperations = splitOp;

                std::vector<std::shared_ptr<GroupNode>> loadGroups;
                std::vector<std::shared_ptr<GroupNode>> storeGroups;

                InitGroups(copyOutputs, &loadGroups, &storeGroups, useReduc, config.vecsize);

                std::vector<int> groupSizes;
                for(auto& loadGroupToSplit : loadGroups){
                    groupSizes.emplace_back(int(loadGroupToSplit->getData()->nodes.size()));
                }

                for(auto& loadGroupToSplit : storeGroups){
                    groupSizes.emplace_back(int(loadGroupToSplit->getData()->nodes.size()));
                }

                SplitsGenerator splitsGenerator(groupSizes, inConfig.vecsize);

                do{
                    auto currentSplit = splitsGenerator.getSplitForAllGroups();
                    std::vector<std::shared_ptr<Node>> possibleOutputs;
                    possibleOutputs =  VectorizeWithSplitConf(&loadGroups, &storeGroups, currentSplit, config);
                    int currentNbNodes = GetNbNodes(possibleOutputs);
                    if(currentNbNodes < bestSplitNbNodes){
                        bestSplitConfig = currentSplit;
                        bestSplitNbNodes = currentNbNodes;
                        bestVecOutputs = possibleOutputs;
                        *outputs = copyOutputs;
                    }
                    if(config.verbose){
                        std::cout << "A possible Split Config: ";
                        for(auto& v : currentSplit){
                            std::cout << " [";
                            for(auto& n : v){
                                std::cout << " " << n;
                            }
                            std::cout << " ] ";
                        }
                        std::cout << "\n -- The graph will contain " << currentNbNodes << " Vectorial Node\n" << std::endl;
                    }
                    loadGroups.clear();
                    storeGroups.clear();
                    InitGroups(copyOutputs, &loadGroups, &storeGroups, useReduc, config.vecsize);
                } while(splitsGenerator.nextPermutation());
            }
        }
        return bestVecOutputs;
    }

    // Vectorize function that gets as input a configuration and with switch case put adequate scenario to do the vectorization
    std::vector<std::shared_ptr<Autovec::Node>> VectorizeNaive(std::vector<std::shared_ptr<Node>>* outputs, const Config& config){
        std::vector<std::shared_ptr<GroupNode>> loadGroups;
        std::vector<std::shared_ptr<GroupNode>> storeGroups;

        if(config.constPropagationEnabled){
            ConstantPropagation(outputs);
        }

        if(config.removeDuplicate){
            RemoveDuplicate(outputs);
        }

        if(config.reductionMode == EnableReduction){
            CreateReductionTrees(outputs, 1, config.vecsize);
        }
        InitGroups(*outputs, &loadGroups, &storeGroups, config.reductionMode == EnableReduction, config.vecsize);

        if(config.verbose){
            std::cout << "1) Do naive split loads and stores.\n";
        }
        SplitLoadsNaive(&loadGroups, config.vecsize);
        SplitStoresNaive(&storeGroups, config.vecsize);


        switch (config.splitOperations)
        {
        case WithoutOrder :
            if(config.verbose){
                std::cout << "2) Do naive split operations.\n";
            }
            SplitOperationsNaive(&loadGroups, config.vecsize);
            break;

        case VecTogetherTrue :
            if(config.verbose){
                std::cout << "2) Do VecTogetherTrue split operations.\n";
            }
            SplitOperationsScoreMatrix(&loadGroups, config.verbose, GetSubOperationsVecTogether, true, config.vecsize);
            break;

        case VecTogetherFalse :
            if(config.verbose){
                std::cout << "2) Do VecTogetherFalse split operations.\n";
            }
            SplitOperationsScoreMatrix(&loadGroups, config.verbose, GetSubOperationsVecTogether, false, config.vecsize);
            break;

        case VecOneByOneTrue :
            if(config.verbose){
                std::cout << "2) Do VecOneByOneTrue split operations.\n";
            }
            SplitOperationsScoreMatrix(&loadGroups, config.verbose, GetSubOperations1by1, true, config.vecsize);
            break;

        case VecOneByOneFalse :
            if(config.verbose){
                std::cout << "2) Do VecOneByOneFalse split operations.\n";
            }
            SplitOperationsScoreMatrix(&loadGroups, config.verbose, GetSubOperations1by1, false, config.vecsize);
            break;

        default:
            if(config.verbose){
                std::cout << "<> Error: no valid configuration for splitting operations.\n";
            }
            break;
        }

        // Now we have actions of correct size, but we need to
        // ensure coherency between the indices and add permutation/extraction/insertion
        if(config.verbose){
            std::cout << "3) Do permute extract needed to ensure coherency.\n\n";
        }
        PermuteExtract(&loadGroups, config.vecsize);

        // Convert each group into a vec instruction
        // Return the new graph
        return VectorialGraph(loadGroups, config.vecsize);

    }

    // Vectorize function that gets as input a configuration and with switch case put adequate scenario to do the vectorization
    std::vector<std::shared_ptr<Autovec::Node>> Vectorize(std::vector<std::shared_ptr<Node>>* outputs, const Config& config){
        switch (config.splitLoadsAndStores)
        {
        case Naive :
            return VectorizeNaive(outputs, config);
            break;        
        case GreedyWithConfig :
            return VectorizeGreedy(outputs, config);
            break;
        default:
            if(config.verbose){
                std::cout << "<> Error: no valid configuration for splitting loads and stores.\n";
            }
            break;
        }
        return std::vector<std::shared_ptr<Autovec::Node>>();
    }
}

/////////////////////////////////////////////////
/// AVX512 functions
/////////////////////////////////////////////////

void GenerateAVX512(std::vector<std::shared_ptr<Node>> inOutputs, std::stringstream& avxCode,
                    const bool minRegisters, const int VEC_SIZE){
    assert(VEC_SIZE <= 8);
    std::unordered_map<std::string,std::string> scalarTypeToVecType;
    scalarTypeToVecType["int"] = "__m512i";
    scalarTypeToVecType["short"] = "__m512i";
    scalarTypeToVecType["long int"] = "__m512i";
    scalarTypeToVecType["float"] = "__m512";
    scalarTypeToVecType["double"] = "__m512d";

    std::unordered_map<std::string,std::string> unariOpToStr;
    unariOpToStr["-"] = "_mm512_neg_pd";
    unariOpToStr["+"] = "";

    std::unordered_map<std::string,std::string> binaryOpToStr;
    binaryOpToStr["-"] = "_mm512_sub_pd";
    binaryOpToStr["+"] = "_mm512_add_pd";
    binaryOpToStr["*"] = "_mm512_mul_pd";
    binaryOpToStr["/"] = "_mm512_div_pd";

    std::unordered_map<std::string, std::string> reductionOpToStr;
    reductionOpToStr["Reduction +"] = "_mm512_mask_reduce_add_pd";
    reductionOpToStr["Reduction *"] = "_mm512_mask_reduce_mul_pd";

    auto maskNTrueBits = [](long int nbBits) -> unsigned long int {
        return ~(~(0UL)<<nbBits);
    };

    auto maskTrueBitsFromIndices = [](const auto& inContainer) -> unsigned long int {
        unsigned long int mask = 0;

        unsigned long int idx = 0;
        for(const auto& value : inContainer){
            if(value >= 0){
                mask |= (1 << idx);
            }
            ++idx;
        }

        return mask;
    };

    auto ContainerIsContiguous = [](const auto& inVecOfIndices) -> bool{
        if(std::size(inVecOfIndices) == 0){
            return true;
        }

        auto iter = std::begin(inVecOfIndices);
        const auto iterEnd = std::end(inVecOfIndices);

        auto previous = iter;
        ++iter;

        for(; iter != iterEnd ; ++iter, ++previous){
            if((*iter) != -1){
                if((*previous)+1 != (*iter)){
                    return false;
                }
            }
        }

        return true;
    };

    auto CleanOffetSize = [](const auto& inOffset){
        return std::count_if(std::begin(inOffset), std::end(inOffset), [](auto&& c){return c >= 0;});
    };

    // Do it from the inputs
    std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);
    {
        auto maxDistanceFromInputs = Autovec::GetMaxDistanceFromOutputs(inOutputs);
        std::sort(allNodes.begin(), allNodes.end(),
                  [&](auto& n1, auto& n2){
            assert(maxDistanceFromInputs.find(n1.get()) != maxDistanceFromInputs.end());
            assert(maxDistanceFromInputs.find(n2.get()) != maxDistanceFromInputs.end());
            return maxDistanceFromInputs[n1.get()] > maxDistanceFromInputs[n2.get()];
        });
    }
    //
    {
        auto nodesToOps = ConvertNodesToOps(allNodes);
        auto nbPushPop = MinimizeRegisters::GetNbPushPop(nodesToOps, 32);
        std::cout << "Nb push = " << std::get<0>(nbPushPop) << std::endl;
        std::cout << "Nb pop  = " << std::get<1>(nbPushPop) << std::endl;
        if(minRegisters){
            if((std::get<0>(nbPushPop) + std::get<1>(nbPushPop)) == 0){
                std::cout << "Already good..." << std::endl;
            }
            else{
                std::cout << "Permute..." << std::endl;
                auto permute = MinimizeRegisters::Permute(nodesToOps);
                auto permutedOps = MinimizeRegisters::ApplyPermutation(nodesToOps, permute);
                auto newNbPushPop = MinimizeRegisters::GetNbPushPop(permutedOps, 32);
                std::cout << "Nb push = " << std::get<0>(newNbPushPop) << std::endl;
                std::cout << "Nb pop  = " << std::get<1>(newNbPushPop) << std::endl;
                if(std::get<0>(nbPushPop) + std::get<1>(nbPushPop)
                        > std::get<0>(newNbPushPop) + std::get<1>(newNbPushPop)){
                    allNodes = MinimizeRegisters::ApplyPermutation(allNodes, permute);
                }
            }
        }
    }

    int cptLoad = 0;
    int cptVar = 0;

    std::unordered_map<Node*, std::string> nodeToNameMapping;

    for(auto currentNode : allNodes){
        assert(currentNode->getData());
        {
            if(currentNode->getNbInputs(0) == 0){
                assert(currentNode->getNbInputs(1) == 0);
                assert(currentNode->getData()->isScalarLoadOrSet() || currentNode->getData()->isVectorialLoadOrSet());
                const std::string currentName = std::string("ld_").append(std::to_string(cptLoad++));
                nodeToNameMapping[currentNode.get()] = currentName;
            }
            else{
                if(currentNode->getNbOutputs()){
                    assert(currentNode->getData()->isScalarOperation() || currentNode->getData()->isVectorialOperation());
                    const std::string currentName = std::string("tmp_").append(std::to_string(cptVar++));
                    nodeToNameMapping[currentNode.get()] = currentName;
                }
                else{
                    assert(currentNode->getData()->isScalarStore() || currentNode->getData()->isVectorialStore());
                }
            }
        }

        if(currentNode->getData()->isScalarStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            auto store = currentNode->getData()->getScalar().getStore();
            avxCode << "tab" << store.ptr << "[" << store.offset << "] = "
                       << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ";" << std::endl;
        }
        else if(currentNode->getData()->isVectorialStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            auto store = currentNode->getData()->getVectorial().getStore();
            const long int realOffsetSize = CleanOffetSize(store.offset);
            if(ContainerIsContiguous(store.offset)){
                // Store all in one instruction if possible
                if(realOffsetSize == VEC_SIZE){
                    avxCode << "_mm512_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ");" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/2){
                    avxCode << "_mm512_mask_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << maskNTrueBits(realOffsetSize) << ", "
                              << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ");" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/2){
                    avxCode << "_mm256_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << "_mm512_castpd512_pd256( " << nodeToNameMapping[currentNode->getInputs(0).front().get()] << "));" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/4){
                    avxCode << "_mm256_mask_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << maskNTrueBits(realOffsetSize) << ", "
                              << "_mm512_castpd512_pd256( " << nodeToNameMapping[currentNode->getInputs(0).front().get()] << "));" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/4){
                    avxCode << "_mm_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << "_mm512_castpd512_pd128( " << nodeToNameMapping[currentNode->getInputs(0).front().get()] << "));" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/8){
                    avxCode << "_mm_mask_storeu_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << maskNTrueBits(realOffsetSize) << ", "
                              << "_mm512_castpd512_pd128( " << nodeToNameMapping[currentNode->getInputs(0).front().get()] << "));" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/8){
                    avxCode << "_mm_storel_pd( tab" << store.ptr << "+" << store.offset[0] << ", "
                              << "_mm512_castpd512_pd128( " << nodeToNameMapping[currentNode->getInputs(0).front().get()] << "));" << std::endl;
                }
                else{
                    avxCode << "store - Invalid size " << realOffsetSize << " for tab " << store.ptr << std::endl;
                }
            }
            else{
                // Scatter
                avxCode << "_mm512_mask_i64scatter_pd( tab" << store.ptr << ", "
                          << maskNTrueBits(realOffsetSize) << ", "
                          << " _mm512_set_epi64(" << ContainerToStrRev(store.offset) << "), "
                          << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ", sizeof(double));" << std::endl;
            }
        }
        else if(currentNode->getData()->isScalarLoad()){
            auto load = currentNode->getData()->getScalar().getLoad();
            avxCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = " << load.ptr << "[" << load.offset << "];" << std::endl;
        }
        else if(currentNode->getData()->isScalarSet()){
            auto set = currentNode->getData()->getScalar().getSet();
            avxCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = " << Instruction::ValueToString(set.value) << ";" << std::endl;
        }
        else if(currentNode->getData()->isVectorialLoad()){
            auto load = currentNode->getData()->getVectorial().getLoad();
            avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                      << nodeToNameMapping[currentNode.get()] << " = "; // ...
            const long int realOffsetSize = CleanOffetSize(load.offset);
            if(ContainerIsContiguous(load.offset)){
                // Load all in one instruction if possible
                if(realOffsetSize == VEC_SIZE){
                    avxCode << "_mm512_loadu_pd( tab" << load.ptr << "+" << load.offset[0] << ");" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/2){
                    avxCode << "_mm512_maskz_loadu_pd( " << maskNTrueBits(realOffsetSize) << ", "
                              << "tab" << load.ptr << "+" << load.offset[0] << ");" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/2){
                    avxCode << "_mm512_castpd256_pd512(_mm256_loadu_pd( tab" << load.ptr << "+" << load.offset[0] << "));" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/4){
                    avxCode << "_mm512_castpd256_pd512(_mm256_maskz_loadu_pd( " << maskNTrueBits(realOffsetSize) << ", "
                              << "tab" << load.ptr << "+" << load.offset[0] << "));" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/4){
                    avxCode << "_mm512_castpd128_pd512(_mm_loadu_pd( tab" << load.ptr << "+" << load.offset[0]  << "));" << std::endl;
                }
                else if(realOffsetSize > VEC_SIZE/8){
                    avxCode << "_mm512_castpd128_pd512(_mm_maskz_loadu_pd( " << maskNTrueBits(realOffsetSize) << ", "
                              << "tab" << load.ptr << "+" << load.offset[0] << "));" << std::endl;
                }
                else if(realOffsetSize == VEC_SIZE/8){
                    avxCode << "_mm512_set1_pd( tab" << load.ptr << "[" << load.offset[0] << "]);" << std::endl;
                }
                else{
                    avxCode << "load - Invalid size " << realOffsetSize << " for tab " << load.ptr << std::endl;
                }
            }
            else{
                // Scatter
                avxCode << "_mm512_mask_i64gather_pd( _mm512_setzero_pd(), "
                          << maskNTrueBits(realOffsetSize) << ", "
                          << " _mm512_set_epi64(" << ContainerToStrRev(load.offset) << "),"
                          << "tab" << load.ptr << ", sizeof(double));" << std::endl;
            }
        }
        else if(currentNode->getData()->isVectorialSet()){
            auto set = currentNode->getData()->getVectorial().getSet();
            avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                      << nodeToNameMapping[currentNode.get()] << " = ";
            avxCode << "_mm512_set1_pd( " << Instruction::ValueToString(set.value) << ");" << std::endl;
        }
        else if(currentNode->getData()->isScalarOperation()){
            auto operation = currentNode->getData()->getScalar().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            if(currentNode->getInputs(1).size()){
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                avxCode << currentNode->getData()->getScalar().type << " "  << nodeToNameMapping[currentNode.get()] << " = "
                          << nodeToNameMapping[currentNode->getInputs(0).front().get()] << operation.operation
                          << nodeToNameMapping[currentNode->getInputs(1).front().get()] << ";" << std::endl;
            }
            else{
                avxCode << currentNode->getData()->getScalar().type << " "  << nodeToNameMapping[currentNode.get()] << " = "
                          << operation.operation << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ";" << std::endl;
            }
        }
        else if(currentNode->getData()->isVectorialOperation()){
            auto operation = currentNode->getData()->getVectorial().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            if(currentNode->getInputs(1).size()){
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                if(operation.operation == "Merge"){
                    //? assert(operation.useIndices);
                    avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << "_mm512_or_pd("
                              << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << ", " << nodeToNameMapping[currentNode->getInputs(1).front().get()]
                              << ");" << std::endl;
                }
                else{
                    if(binaryOpToStr.find(operation.operation) == binaryOpToStr.end()){
                        binaryOpToStr[operation.operation] = "???";
                    }
                    avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << binaryOpToStr[operation.operation] << "(" << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                             << ", " << nodeToNameMapping[currentNode->getInputs(1).front().get()] << ");" << std::endl;
                }
            }
            else{
                if(operation.operation == "Extract"){
                    assert(operation.useIndices);
                    avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << "_mm512_mask_permutexvar_pd(_mm512_setzero_pd(), "
                              << maskTrueBitsFromIndices(operation.indices) << ", "
                              << "_mm512_set_epi64(" << ContainerToStrRev(operation.indices) << "),"
                              << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << ");" << std::endl;
                }
                else if(operation.operation == "Permute"){
                    assert(operation.useIndices);
                    avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << "_mm512_mask_permutexvar_pd(_mm512_setzero_pd(), "
                              << maskTrueBitsFromIndices(operation.indices) << ", "
                              << "_mm512_set_epi64(" << ContainerToStrRev(operation.indices) << "),"
                              << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << ");" << std::endl;
                }
                else if(operation.operation.rfind("Reduction ", 0) == 0){
                    assert(int(operation.reductionCount) <= VEC_SIZE);

                    if(reductionOpToStr.find(operation.operation) == reductionOpToStr.end()){
                        reductionOpToStr[operation.operation] = "???";
                    }

                    const bool isOutputVectorial = currentNode->getOutputs().front()->getData()->isVectorial();
                    for(const auto & output : currentNode->getOutputs())
                    {
                        assert(output->getData()->isVectorial() == isOutputVectorial);
                    }

                    // Reduction can produce either a scalar nor a vector to match its inputs
                    if(isOutputVectorial)
                    {
                        avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << "_mm512_set1_pd("
                              << reductionOpToStr[operation.operation] << "("
                              << ((1 << operation.reductionCount) - 1)
                              << ", " << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << "));" << std::endl;
                    }
                    else
                    {
                        avxCode << currentNode->getData()->getVectorial().type << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << reductionOpToStr[operation.operation] << "("
                              << ((1 << operation.reductionCount) - 1)
                              << ", " << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << ");" << std::endl;
                    }
                }
                else{
                    if(unariOpToStr.find(operation.operation) == unariOpToStr.end()){
                        unariOpToStr[operation.operation] = "???";
                    }
                    avxCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                              << nodeToNameMapping[currentNode.get()] << " = "
                              << unariOpToStr[operation.operation] << "(" << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                              << ");" << std::endl;
                }
            }
        }
        else{
            assert(0);
        }
    }
}



void GenerateAVX512Asm(std::vector<std::shared_ptr<Node>> inOutputs, std::stringstream& avxCode,
                    const bool minRegisters, const int VEC_SIZE,
                       const std::string funcName, const bool addPrototype){
    assert(VEC_SIZE <= 8);
    std::unordered_map<std::string, std::string> scalarOperations;
    scalarOperations["+"] = "vaddsd";
    scalarOperations["-"] = "vsubsd";
    scalarOperations["*"] = "vmulsd";
    scalarOperations["/"] = "vdivsd";

    std::unordered_map<std::string,std::string> binaryOpToStr;
    binaryOpToStr["+"] = "vaddpd";
    binaryOpToStr["-"] = "vsubpd";
    binaryOpToStr["*"] = "vmulpd";
    binaryOpToStr["/"] = "vdivpd";

    auto maskNTrueBits = [](long int nbBits) -> unsigned long int {
        return ~(~(0UL)<<nbBits);
    };

    auto maskTrueBitsFromIndices = [](const auto& inContainer) -> unsigned long int {
        unsigned long int mask = 0;

        unsigned long int idx = 0;
        for(const auto& value : inContainer){
            if(value >= 0){
                mask |= (1 << idx);
            }
            ++idx;
        }

        return mask;
    };

    auto ContainerIsContiguous = [](const auto& inVecOfIndices) -> bool{
        if(std::size(inVecOfIndices) == 0){
            return true;
        }

        auto iter = std::begin(inVecOfIndices);
        const auto iterEnd = std::end(inVecOfIndices);

        auto previous = iter;
        ++iter;

        for(; iter != iterEnd ; ++iter, ++previous){
            if((*iter) != -1){
                if((*previous)+1 != (*iter)){
                    return false;
                }
            }
        }

        return true;
    };

    auto CleanOffetSize = [](const auto& inOffset){
        return std::count_if(std::begin(inOffset), std::end(inOffset), [](auto&& c){return c >= 0;});
    };

    // Do it from the inputs
    std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);
    {
        auto maxDistanceFromInputs = Autovec::GetMaxDistanceFromOutputs(inOutputs);
        std::sort(allNodes.begin(), allNodes.end(),
                  [&](auto& n1, auto& n2){
            assert(maxDistanceFromInputs.find(n1.get()) != maxDistanceFromInputs.end());
            assert(maxDistanceFromInputs.find(n2.get()) != maxDistanceFromInputs.end());
            return maxDistanceFromInputs[n1.get()] > maxDistanceFromInputs[n2.get()];
        });
    }

    std::vector<std::unordered_map<int,int>> registers;
    std::unordered_map<int, Node*> idsToNode;
    std::vector<std::vector<int>> pushDetails;
    std::vector<std::vector<int>> popDetails;
    std::unordered_map<int,int> stack;
    {
        auto nodesToOpsWithDetails = ConvertNodesToOpsWithDetails(allNodes);
        auto nodesToOps = std::get<0>(nodesToOpsWithDetails);
        auto nbPushPop = MinimizeRegisters::GetNbPushPop(nodesToOps, 32);
        std::cout << "Nb push = " << std::get<0>(nbPushPop) << std::endl;
        std::cout << "Nb pop  = " << std::get<1>(nbPushPop) << std::endl;
        registers = std::get<4>(nbPushPop);
        pushDetails = std::get<2>(nbPushPop);
        popDetails = std::get<3>(nbPushPop);
        idsToNode = ReverseMap(std::get<1>(nodesToOpsWithDetails));
        if(minRegisters){
            if((std::get<0>(nbPushPop) + std::get<1>(nbPushPop)) == 0){
                std::cout << "Already good..." << std::endl;
            }
            else{
                std::cout << "Permute..." << std::endl;
                auto permute = MinimizeRegisters::Permute(nodesToOps);
                auto permutedOps = MinimizeRegisters::ApplyPermutation(nodesToOps, permute);
                auto newNbPushPop = MinimizeRegisters::GetNbPushPop(permutedOps, 32);
                std::cout << "Nb push = " << std::get<0>(newNbPushPop) << std::endl;
                std::cout << "Nb pop  = " << std::get<1>(newNbPushPop) << std::endl;
                if(std::get<0>(nbPushPop) + std::get<1>(nbPushPop)
                        > std::get<0>(newNbPushPop) + std::get<1>(newNbPushPop)){
                    allNodes = MinimizeRegisters::ApplyPermutation(allNodes, permute);
                    registers = std::get<4>(newNbPushPop);
                    pushDetails = std::get<2>(newNbPushPop);
                    popDetails = std::get<3>(newNbPushPop);
                }
            }
        }
    }

    const std::string StartLine = "\"";
    const std::string EndLine = ";\\n\"\n";
    const std::string EndLineLabel = "\\n\"\n";

    std::unordered_map<std::ptrdiff_t,std::string> ptrToReg;
    ptrToReg[0] = "rdi";
    ptrToReg[1] = "rsi";
    ptrToReg[2] = "rdx";
    ptrToReg[3] = "rcx";
    ptrToReg[4] = "r8";
    ptrToReg[5] = "r9";

    auto createMaskCode = [&StartLine, &EndLine](const unsigned long int mask){// TODO create only if needed or reuse
        if(mask == 0){
            return StartLine + ("kxorw  %k0, %k0, %k1") + EndLine;
        }
        else if(mask == 255){
            return StartLine + ("kxnorw  %k0, %k0, %k1") + EndLine;
        }
        return StartLine + ("movb    $" + std::to_string(mask) + ", %al") + EndLine
               + StartLine + ("kmovd   %eax, %k1") + EndLine;
    };

    auto MemAcc = [](const std::string reg, const std::ptrdiff_t offset){
        assert(reg != "");
        if(offset == 0){
            return "(%" + reg + ")";
        }
        else{
            // Only double currently
            const int sizeType = 8;
            return std::to_string(offset*sizeType) + "(%" + reg + ")";
        }
    };


    static int KeyCpt = 0;

    auto PreambuleVal = [](auto val) -> std::pair<std::string,std::string> {
        /*
.LCPI1_0:
        .quad   0x4058c00000000000              # double 99
.LCPI1_1:
        .quad   0x3ff0000000000000              # double 1
*/
        const std::string key = "LCPI" + std::to_string(KeyCpt++);;

        std::string res = "." + key + ":\n";
        const int hexlength = sizeof(val)*2;
        const char* digits = "0123456789ABCDEF";
        std::string buffer(hexlength,'0');
        for(int idxVal = 0 ; idxVal < hexlength ; idxVal += 2){
            buffer[idxVal] = digits[((const unsigned char*)(&val))[idxVal/2]  & 0x0f];
            buffer[idxVal+1] = digits[((const unsigned char*)(&val))[idxVal/2]>>4  & 0x0f];
        }
        res.append("        .quad    0x" + buffer + " # " + std::to_string(val));

        return std::make_pair(key, res);
    };

    auto PreambuleVec = [](auto&& container) -> std::pair<std::string,std::string> {
        /*
.LCPI0_0:
        .quad   1                               # 0x1
        .quad   2                               # 0x2
        .quad   0                               # 0x0
        .quad   1                               # 0x1
        .quad   2                               # 0x2
        .quad   0                               # 0x0
        .quad   1                               # 0x1
        .quad   2                               # 0x2
.LCPI0_2:
        .quad   0                               # 0x0
        .quad   3                               # 0x3
        .quad   6                               # 0x6
        .zero   8
*/
        const std::string key = "LCPI" + std::to_string(KeyCpt++);

        const bool inRemoveNegative = false;

        std::string res = "\"." + key + ":\"\n";
        auto iter = std::begin(container);
        const auto lastIter = std::end(container);
        while(iter != lastIter){
            const auto& val = (*iter);
            if(inRemoveNegative == false || val >= 0){
                res.append("\"        .quad    " + std::to_string(val) + "\\n\"\n");
            }
            ++iter;
        }

        return std::make_pair(key, res);
    };

    std::ostringstream asmPreambule;
    std::ostringstream asmCode;

    int offsetStack = -512/8;

    for(int idxNode = 0 ; idxNode < int(allNodes.size()) ; ++idxNode){
        auto currentNode = allNodes[idxNode];

        assert(currentNode->getData());
        {
            if(currentNode->getNbInputs(0) == 0){
                assert(currentNode->getNbInputs(1) == 0);
                assert(currentNode->getData()->isScalarLoadOrSet() || currentNode->getData()->isVectorialLoadOrSet());
            }
            else{
                if(currentNode->getNbOutputs()){
                    assert(currentNode->getData()->isScalarOperation() || currentNode->getData()->isVectorialOperation());
                }
                else{
                    assert(currentNode->getData()->isScalarStore() || currentNode->getData()->isVectorialStore());
                }
            }
        }

        assert(idxNode < int(registers.size()));
        std::unordered_map<int,int> nodeIdToRegMapping = registers[idxNode];
        // Will not include the temporary registers
        // Push/pop in stack and update related structures
        for(auto idPush : pushDetails[idxNode]){
            assert(stack.find(idPush) == stack.end()
                   || std::find(popDetails[idxNode].begin(),
                                popDetails[idxNode].end(),
                                idPush) != popDetails[idxNode].end());
//            if(stack.find(idPush) == stack.end()){
                stack[idPush] = offsetStack;
                offsetStack -= 512/8;

                assert(nodeIdToRegMapping.find(idPush) != nodeIdToRegMapping.end());
                asmCode << StartLine << "vmovupd %zmm" << nodeIdToRegMapping[idPush]
                        << ", " << stack[idPush] << "(%rsp)" << EndLine;
                nodeIdToRegMapping.erase(idPush);
//            }
        }
        for(auto idPop : popDetails[idxNode]){
            assert(stack.find(idPop) != stack.end());
            assert(nodeIdToRegMapping.find(idPop) != nodeIdToRegMapping.end());
            asmCode << StartLine << "vmovupd " << stack[idPop] << "(%rsp)"
                    << ", %zmm" << nodeIdToRegMapping[idPop] << EndLine;
//            if(std::find(pushDetails[idxNode].begin(),
//                         pushDetails[idxNode].end(),
//                         idPop) == pushDetails[idxNode].end()){
                stack.erase(idPop);
//            }
        }
        std::unordered_map<Node*,int> nodeToRegMapping;
        for(auto mapping : nodeIdToRegMapping){
            if(mapping.first >= 0){
                assert(idsToNode.find(mapping.first) != idsToNode.end());
                nodeToRegMapping[idsToNode[mapping.first]] = mapping.second;
            }
        }
        std::unordered_map<Node*,int> nodeToStackMapping;
        for(auto mapping : stack){
            assert(mapping.first >= 0);
            assert(idsToNode.find(mapping.first) != idsToNode.end());
            nodeToStackMapping[idsToNode[mapping.first]] = mapping.second;
        }

        auto getReference = [&](Node* nd, const std::string regType) -> std::string {
            if(nodeToRegMapping.find(nd) != nodeToRegMapping.end()){
                return "%" + regType + "mm" + std::to_string(nodeToRegMapping[nd]);
            }
            else{
                assert(nodeToStackMapping.find(nd) != nodeToStackMapping.end());
                return std::to_string(nodeToStackMapping[nd]) + "(%rsp)";
            }
        };

        if(currentNode->getData()->isScalarStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToRegMapping.find(currentNode->getInputs(0).front().get()) != nodeToRegMapping.end());
            auto store = currentNode->getData()->getScalar().getStore();
            asmCode << StartLine << "vmovsd %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                    << ", " << MemAcc(ptrToReg[store.ptr],store.offset) << EndLine;
        }
        else if(currentNode->getData()->isVectorialStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToRegMapping.find(currentNode->getInputs(0).front().get()) != nodeToRegMapping.end());
            auto store = currentNode->getData()->getVectorial().getStore();
            const long int realOffsetSize = CleanOffetSize(store.offset);
            if(ContainerIsContiguous(store.offset)){
                // Store all in one instruction if possible
                if(realOffsetSize == VEC_SIZE){
                    asmCode << StartLine << "vmovupd %zmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/2){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd %zmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/2){
                    asmCode << StartLine << "vmovupd %ymm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/4){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd %ymm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/4){
                    asmCode << StartLine << "vmovupd %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/8){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/8){
                    asmCode << StartLine << "vmovsd %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", " << MemAcc(ptrToReg[store.ptr],store.offset[0]) << EndLine;
                }
                else{
                    asmCode << "store - Invalid size " << realOffsetSize << " for tab " << store.ptr << std::endl;
                }
            }
            else{
                // Scatter
                assert(nodeIdToRegMapping.find(-1) != nodeIdToRegMapping.end());
                assert(nodeIdToRegMapping[-1] != nodeToRegMapping[currentNode->getInputs(0).front().get()]);
                auto keyValue = PreambuleVec(store.offset);
                asmPreambule << keyValue.second;
                // TODO use vmovapd
                asmCode << createMaskCode(maskNTrueBits(realOffsetSize))
                        << StartLine << "vmovupd ." << keyValue.first << "(%rip), %zmm" << nodeIdToRegMapping[-1] << EndLine
                        << StartLine << "vscatterqpd  %zmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                        << ", (%" << ptrToReg[store.ptr] << ",%zmm" << nodeIdToRegMapping[-1] << ",8) {%k1}" << EndLine;
            }
        }
        else if(currentNode->getData()->isScalarLoad()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto load = currentNode->getData()->getScalar().getLoad();
            asmCode << StartLine << "vmovsd  " << MemAcc(ptrToReg[load.ptr],load.offset)
                    << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
        }
        else if(currentNode->getData()->isScalarSet()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto set = currentNode->getData()->getScalar().getSet();
            assert(set.value.index() == 0 || set.value.index() == 1);
            auto keyValue = (set.value.index() == 0?
                                 PreambuleVal(std::get<0>(set.value)):
                                 PreambuleVal((double)std::get<1>(set.value)));
            asmPreambule << keyValue.second;
            asmCode << StartLine << "vmovsd ." << keyValue.first << "(%rip), %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
        }
        else if(currentNode->getData()->isVectorialLoad()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto load = currentNode->getData()->getVectorial().getLoad();
            const long int realOffsetSize = CleanOffetSize(load.offset);
            if(ContainerIsContiguous(load.offset)){
                // Load all in one instruction if possible
                if(realOffsetSize == VEC_SIZE){
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %zmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/2){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %zmm" << nodeToRegMapping[currentNode.get()] << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/2){
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %ymm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/4){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %ymm" << nodeToRegMapping[currentNode.get()] << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/4){
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else if(realOffsetSize > VEC_SIZE/8){
                    asmCode << createMaskCode(maskNTrueBits(realOffsetSize));
                    asmCode << StartLine << "vmovupd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %xmm" << nodeToRegMapping[currentNode.get()] << " {%k1}" << EndLine;
                }
                else if(realOffsetSize == VEC_SIZE/8){
                    asmCode << StartLine << "vmovsd " << MemAcc(ptrToReg[load.ptr],load.offset[0])
                            << " , %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else{
                    asmCode << "load - Invalid size " << realOffsetSize << " for tab " << load.ptr << std::endl;
                }
            }
            else{
                // Gather
                assert(nodeIdToRegMapping.find(-1) != nodeIdToRegMapping.end());
                auto keyValue = PreambuleVec(load.offset);
                asmPreambule << keyValue.second;
                // TODO use vmovapd
                asmCode << createMaskCode(maskNTrueBits(realOffsetSize))
                       << StartLine << "vmovupd ." << keyValue.first << "(%rip), %zmm" << nodeIdToRegMapping[-1] << EndLine
                       << StartLine << "vgatherqpd (%" << ptrToReg[load.ptr] << ", %zmm" << nodeIdToRegMapping[-1] << ",8)"
                       << " , %zmm" << nodeToRegMapping[currentNode.get()] << " {%k1}" << EndLine;
            }
        }
        else if(currentNode->getData()->isVectorialSet()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto set = currentNode->getData()->getVectorial().getSet();
            assert(set.value.index() == 0 || set.value.index() == 1);
            auto keyValue = (set.value.index() == 0?
                                 PreambuleVal(std::get<0>(set.value)):
                                 PreambuleVal((double)std::get<1>(set.value)));
            asmCode << StartLine << "vbroadcastsd ." << keyValue.first
                    << "(%rip), %zmm" << nodeToRegMapping[currentNode.get()] << EndLine;
        }
        else if(currentNode->getData()->isScalarOperation()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto operation = currentNode->getData()->getScalar().getOperation();
            assert(currentNode->getInputs(0).size());
            if(currentNode->getInputs(1).size()){
                assert(nodeToRegMapping.find(currentNode->getInputs(1).front().get()) != nodeToRegMapping.end());

                asmCode << StartLine << scalarOperations[operation.operation]
                        << " " << getReference(currentNode->getInputs(0).front().get(), "x")
                        << " , %xmm" << nodeToRegMapping[currentNode->getInputs(1).front().get()]
                        << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
            }
            else{
                assert(currentNode->getData()->getScalar().type == "double");
                if(operation.operation == "-"){
                    assert(nodeToRegMapping.find(currentNode->getInputs(0).front().get()) != nodeToRegMapping.end());
                    asmCode << StartLine << "vxorpd %xmm" << nodeToRegMapping[currentNode.get()] << ", %xmm"
                            << nodeToRegMapping[currentNode.get()] << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                    asmCode << StartLine << "vsubsd %xmm" << nodeToRegMapping[currentNode.get()]
                            << " , %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                            << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else{
                    asmCode << StartLine << "vmovsd " << getReference(currentNode->getInputs(0).front().get(), "x")
                            << ", " << nodeToRegMapping[currentNode.get()] << EndLine;
                }
            }
        }
        else if(currentNode->getData()->isVectorialOperation()){
            assert(nodeToRegMapping.find(currentNode.get()) != nodeToRegMapping.end());
            auto operation = currentNode->getData()->getVectorial().getOperation();
            assert(currentNode->getInputs(0).size());
            if(currentNode->getInputs(1).size()){
                assert(nodeToRegMapping.find(currentNode->getInputs(1).front().get()) != nodeToRegMapping.end());
                if(operation.operation == "Merge"){
                    asmCode << StartLine << "vpord "
                              << " " << getReference(currentNode->getInputs(0).front().get(), "z")
                              << ", %zmm" << nodeToRegMapping[currentNode->getInputs(1).front().get()]
                              << ", %zmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
                else{
                    if(binaryOpToStr.find(operation.operation) == binaryOpToStr.end()){
                        binaryOpToStr[operation.operation] = "???";
                    }
                    asmCode << StartLine << binaryOpToStr[operation.operation] 
                              << " " << getReference(currentNode->getInputs(0).front().get(), "z")
                              << ", %zmm" << nodeToRegMapping[currentNode->getInputs(1).front().get()]
                              << ", %zmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                }
            }
            else{
                if(operation.operation == "Extract"
                        || operation.operation == "Permute"){
                    assert(operation.useIndices);
                    auto keyValue = PreambuleVec(operation.indices);
                    asmPreambule << keyValue.second;

                    // TODO use vmovapd
                    assert(nodeIdToRegMapping.find(-1) != nodeIdToRegMapping.end());
                    asmCode << StartLine << "vmovupd ." << keyValue.first << "(%rip), %zmm" << nodeIdToRegMapping[-1] << EndLine;

                    std::string extra;
                    if(maskTrueBitsFromIndices(operation.indices) != 255){
                        asmCode << StartLine << "vxorpd  %xmm" << nodeToRegMapping[currentNode.get()] << ", %xmm"
                                << nodeToRegMapping[currentNode.get()] << ", %xmm" << nodeToRegMapping[currentNode.get()]
                                << EndLine;
                        asmCode << createMaskCode(maskTrueBitsFromIndices(operation.indices));
                        extra = " {%k1} ";
                    }

                    asmCode << StartLine << "vpermt2pd "
                            << getReference(currentNode->getInputs(0).front().get(), "z") << ", %zmm"
                            << nodeIdToRegMapping[-1] << ", %zmm"
                            << nodeToRegMapping[currentNode.get()] << extra << EndLine;
                }
                else if(operation.operation.rfind("Reduction ", 0) == 0){
                    assert(nodeIdToRegMapping.find(-1) != nodeIdToRegMapping.end());
                    assert(int(operation.reductionCount) <= VEC_SIZE);

                    const bool isOutputVectorial = currentNode->getOutputs().front()->getData()->isVectorial();
                    for(const auto & output : currentNode->getOutputs())
                    {
                        assert(output->getData()->isVectorial() == isOutputVectorial);
                    }
                    assert(nodeToRegMapping.find(currentNode->getInputs(0).front().get()) != nodeToRegMapping.end());

                    if(operation.operation == "Reduction +"){
                        asmCode << StartLine << "vextractf64x4   $1, %zmm"
                                << nodeToRegMapping[currentNode->getInputs(0).front().get()] << ", %ymm"
                                << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vaddpd  %ymm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %ymm" << nodeIdToRegMapping[-1]
                                << ", %ymm" << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vextractf128    $1, %ymm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()] << EndLine;
                        asmCode << StartLine << "vaddpd  %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vpermilpd       $1, %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()] << EndLine;
                        asmCode << StartLine << "vaddsd  %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                    }
                    else if(operation.operation == "Reduction *"){
                        asmCode << StartLine << "vextractf64x4   $1, %zmm"
                                << nodeToRegMapping[currentNode->getInputs(0).front().get()] << ", %ymm"
                                << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vmulpd  %ymm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %ymm" << nodeIdToRegMapping[-1]
                                << ", %ymm" << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vextractf128    $1, %ymm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()] << EndLine;
                        asmCode << StartLine << "vmulpd  %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeIdToRegMapping[-1] << EndLine;
                        asmCode << StartLine << "vpermilpd       $1, %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()] << EndLine;
                        asmCode << StartLine << "vmulsd  %xmm" << nodeToRegMapping[currentNode->getInputs(0).front().get()]
                                << ", %xmm" << nodeIdToRegMapping[-1]
                                << ", %xmm" << nodeToRegMapping[currentNode.get()] << EndLine;
                    }
                    else{
                        assert(0);
                    }

                    // Reduction can produce either a scalar nor a vector to match its inputs
                    if(isOutputVectorial)
                    {
                        asmCode << StartLine << "vbroadcastsd  %xmm"
                                << nodeToRegMapping[currentNode.get()] << ", %zmm"
                                << nodeToRegMapping[currentNode.get()] << EndLine;
                    }
                }
                else {
                    asmCode << StartLine << "??" << EndLine;
                }
            }
        }
        else{
            assert(0);
        }
    }

    if(addPrototype){
        avxCode << "extern \"C\" void " << funcName << "(";
        {
            //std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);
            std::set<std::ptrdiff_t> arrayIdx;
            for(auto nd : allNodes){
                if(nd->getData()->isScalarLoad()){
                    arrayIdx.insert(nd->getData()->getScalar().getLoad().ptr);
                }
                else if(nd->getData()->isVectorialLoad()){
                    arrayIdx.insert(nd->getData()->getVectorial().getLoad().ptr);
                }
                else if(nd->getData()->isScalarStore()){
                    arrayIdx.insert(nd->getData()->getScalar().getStore().ptr);
                }
                else if(nd->getData()->isVectorialStore()){
                    arrayIdx.insert(nd->getData()->getVectorial().getStore().ptr);
                }
            }
            int nbInputArrays = 0;
            for(auto index : arrayIdx){
                if(nbInputArrays){
                    avxCode << ", ";
                }
                avxCode << "double *tab" << index;
                nbInputArrays += 1;
            }
        }
        avxCode << ");\n";
    }

    avxCode << " __asm__(\n"
            << asmPreambule.str()
            << StartLine << ".global " << funcName << EndLineLabel
            << StartLine << funcName << ":" << EndLineLabel
            << asmCode.str()
            << StartLine << "vzeroupper" << EndLine
            << StartLine << "retq" << EndLine
            << ");\n";

}

void GenerateARM_SVE(std::vector<std::shared_ptr<Node>> inOutputs, std::stringstream &arm_sveCode,
                     const bool minRegisters, const int VEC_SIZE)
{
    std::unordered_map<std::string, std::string> scalarTypeToVecType;
    scalarTypeToVecType["int"] = "svint32_t";
    scalarTypeToVecType["short"] = "svint16_t";
    scalarTypeToVecType["long int"] = "svint64_t";
    scalarTypeToVecType["float"] = "svfloat32_t";
    scalarTypeToVecType["double"] = "svfloat64_t";

    std::unordered_map<std::string, std::string> unariOpToStr;
    unariOpToStr["-"] = "svneg_f64_x";
    unariOpToStr["+"] = "";

    std::unordered_map<std::string, std::string> binaryOpToStr;
    binaryOpToStr["-"] = "svsub_f64_x(svptrue_b64(), ";
    binaryOpToStr["+"] = "svadd_f64_x(svptrue_b64(), ";
    binaryOpToStr["*"] = "svmul_f64_x(svptrue_b64(), ";
    binaryOpToStr["/"] = "svdiv_f64_x(svptrue_b64(), ";

    std::unordered_map<std::string, std::string> reductionOpToStr;
    reductionOpToStr["Reduction +"] = "svadda";
    reductionOpToStr["Reduction *"] = "svmula";

    auto ContainerIsContiguous = [](const auto &inVecOfIndices) -> bool
    {
        if (std::size(inVecOfIndices) == 0)
        {
            return true;
        }

        auto iter = std::begin(inVecOfIndices);
        const auto iterEnd = std::end(inVecOfIndices);

        auto previous = iter;
        ++iter;

        for (; iter != iterEnd; ++iter, ++previous)
        {
            if ((*iter) != -1)
            {
                if ((*previous) + 1 != (*iter))
                {
                    return false;
                }
            }
        }

        return true;
    };

    auto CleanOffetSize = [](const auto &inOffset)
    {
        return std::count_if(std::begin(inOffset), std::end(inOffset), [](auto &&c)
                             { return c >= 0; });
    };
    // Do it from the inputs
    std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);
    {
        auto maxDistanceFromInputs = Autovec::GetMaxDistanceFromOutputs(inOutputs);
        std::sort(allNodes.begin(), allNodes.end(),
                  [&](auto& n1, auto& n2){
            assert(maxDistanceFromInputs.find(n1.get()) != maxDistanceFromInputs.end());
            assert(maxDistanceFromInputs.find(n2.get()) != maxDistanceFromInputs.end());
            return maxDistanceFromInputs[n1.get()] > maxDistanceFromInputs[n2.get()];
        });
    }
    //
    {
        auto nodesToOps = ConvertNodesToOps(allNodes);
        auto nbPushPop = MinimizeRegisters::GetNbPushPop(nodesToOps, 32);
        std::cout << "Nb push = " << std::get<0>(nbPushPop) << std::endl;
        std::cout << "Nb pop  = " << std::get<1>(nbPushPop) << std::endl;
        if(minRegisters){
            if((std::get<0>(nbPushPop) + std::get<1>(nbPushPop)) == 0){
                std::cout << "Already good..." << std::endl;
            }
            else{
                std::cout << "Permute..." << std::endl;
                auto permute = MinimizeRegisters::Permute(nodesToOps);
                auto permutedOps = MinimizeRegisters::ApplyPermutation(nodesToOps, permute);
                auto newNbPushPop = MinimizeRegisters::GetNbPushPop(permutedOps, 32);
                std::cout << "Nb push = " << std::get<0>(newNbPushPop) << std::endl;
                std::cout << "Nb pop  = " << std::get<1>(newNbPushPop) << std::endl;
                if(std::get<0>(nbPushPop) + std::get<1>(nbPushPop)
                        > std::get<0>(newNbPushPop) + std::get<1>(newNbPushPop)){
                    allNodes = MinimizeRegisters::ApplyPermutation(allNodes, permute);
                }
            }
        }
    }

    int cptLoad = 0;
    int cptVar = 0;

    std::unordered_map<Node *, std::string> nodeToNameMapping;

    for (auto currentNode : allNodes)
    {
        assert(currentNode->getData());
        {
            if(currentNode->getNbInputs(0) == 0){
                assert(currentNode->getNbInputs(1) == 0);
                assert(currentNode->getData()->isScalarLoadOrSet() || currentNode->getData()->isVectorialLoadOrSet());
                const std::string currentName = std::string("ld_").append(std::to_string(cptLoad++));
                nodeToNameMapping[currentNode.get()] = currentName;
            }
            else{
                if(currentNode->getNbOutputs()){
                    assert(currentNode->getData()->isScalarOperation() || currentNode->getData()->isVectorialOperation());
                    const std::string currentName = std::string("tmp_").append(std::to_string(cptVar++));
                    nodeToNameMapping[currentNode.get()] = currentName;
                }
                else{
                    assert(currentNode->getData()->isScalarStore() || currentNode->getData()->isVectorialStore());
                }
            }
        }
        if (currentNode->getData()->isScalarStore())
        {
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            auto store = currentNode->getData()->getScalar().getStore();
            arm_sveCode << "tab" << store.ptr << "[" << store.offset << "] = "
                        << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ";" << std::endl;
        }
        else if (currentNode->getData()->isVectorialStore())
        {
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            auto store = currentNode->getData()->getVectorial().getStore();
            const long int realOffsetSize = CleanOffetSize(store.offset);
            if (ContainerIsContiguous(store.offset))
            {
                arm_sveCode << "svst1_f64( svwhilelt_b64(0," << realOffsetSize << "), tab" << store.ptr << "+" << store.offset[0] << ","
                            << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ");" << std::endl;
            }
            else
            {
                arm_sveCode << "svst1_scatter_s64index_f64( svwhilelt_b64(0," << realOffsetSize << "), tab" << store.ptr << "+" << store.offset[0] << ", "
                            << "svld1_s64(svptrue_b64(), (const long[]){" << ContainerToStr(store.offset) << "})," << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ");" << std::endl;
            }
        }
        else if (currentNode->getData()->isScalarLoad())
        {
            auto load = currentNode->getData()->getScalar().getLoad();
            arm_sveCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = Load(" << load.ptr << ", " << load.offset << ");" << std::endl;
        }
        else if(currentNode->getData()->isScalarSet()){
            auto set = currentNode->getData()->getScalar().getSet();
            arm_sveCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = " << Instruction::ValueToString(set.value) << ";" << std::endl;
        }
        else if (currentNode->getData()->isVectorialLoad())
        {
            auto load = currentNode->getData()->getVectorial().getLoad();
            arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                        << nodeToNameMapping[currentNode.get()] << " = ";
            const long int realOffsetSize = CleanOffetSize(load.offset);
            if (load.ptr == -1)
            {
                arm_sveCode << "svdup_n_f64(0);";
            }
            else if (ContainerIsContiguous(load.offset))
            {
                arm_sveCode << "svld1_f64( svwhilelt_b64(0," << realOffsetSize << "), tab" << load.ptr << "+" << load.offset[0] << ");" << std::endl;
            }
            else
            {
                arm_sveCode << "svld1_gather_u64index_f64( svcmpge_n_s64(svptrue_b64(), svld1_s64(svptrue_b64(), (const long[]){"
                            << ContainerToStr(load.offset)
                            << "}), 0), tab" << load.ptr << ", svld1_u64(svptrue_b64(), (const unsigned long[]){" << ContainerToStr(load.offset, true, true) << "}));" << std::endl;
            }
        }
        else if(currentNode->getData()->isVectorialSet()){
            auto set = currentNode->getData()->getVectorial().getSet();
            arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                      << nodeToNameMapping[currentNode.get()] << " = ";
            arm_sveCode << "svdup_n_f64( " << Instruction::ValueToString(set.value) << ");" << std::endl;
        }
        else if (currentNode->getData()->isScalarOperation())
        {
            auto operation = currentNode->getData()->getScalar().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            if (currentNode->getInputs(1).size())
            {
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                arm_sveCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = "
                            << nodeToNameMapping[currentNode->getInputs(0).front().get()] << operation.operation
                            << nodeToNameMapping[currentNode->getInputs(1).front().get()] << ";" << std::endl;
            }
            else
            {
                arm_sveCode << currentNode->getData()->getScalar().type << " " << nodeToNameMapping[currentNode.get()] << " = "
                            << operation.operation << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ";" << std::endl;
            }
        }
        else if (currentNode->getData()->isVectorialOperation())
        {
            auto operation = currentNode->getData()->getVectorial().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            if (currentNode->getInputs(1).size())
            {
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                if (operation.operation == "Merge")
                {
                    arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                                << nodeToNameMapping[currentNode.get()] << " = "
                                << "svreinterpret_f64(svorr_z( svptrue_b64(), svreinterpret_u64("
                                << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                                << "), svreinterpret_u64("
                                << nodeToNameMapping[currentNode->getInputs(1).front().get()]
                                << ")));" << std::endl;
                }
                else
                {
                    if (binaryOpToStr.find(operation.operation) == binaryOpToStr.end())
                    {
                        binaryOpToStr[operation.operation] = "???";
                    }
                    arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                                << nodeToNameMapping[currentNode.get()] << " = "
                                << binaryOpToStr[operation.operation] << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                                << ", " << nodeToNameMapping[currentNode->getInputs(1).front().get()] << ");" << std::endl;
                }
            }
            else if (operation.operation == "Extract" || operation.operation == "Permute"){
                assert(operation.useIndices);
                arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                            << nodeToNameMapping[currentNode.get()] << " = "
                            << "svtbl_f64("
                            << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                            << ", svld1_u64(svptrue_b64(), (const unsigned long[]){"
                            << ContainerToStr(operation.indices, true, true) << "}));" << std::endl;
            }
            else if(operation.operation.rfind("Reduction ", 0) == 0){
                assert(int(operation.reductionCount) <= VEC_SIZE);

                if(reductionOpToStr.find(operation.operation) == reductionOpToStr.end()){
                    reductionOpToStr[operation.operation] = "???";
                }

                const bool isOutputVectorial = currentNode->getOutputs().front()->getData()->isVectorial();
                for(const auto & output : currentNode->getOutputs())
                {
                    assert(output->getData()->isVectorial() == isOutputVectorial);
                }

                // Reduction can produce either a scalar nor a vector to match its inputs
                if(isOutputVectorial)
                {
                    arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                          << nodeToNameMapping[currentNode.get()] << " = "
                          << "svdup_n_f64("
                          << reductionOpToStr[operation.operation] << "("
                          << "svwhilelt_b64(0," << ((1 << operation.reductionCount) - 1) << "),";
                    if(operation.operation == "Reduction +"){
                        arm_sveCode << "svdup_n_f64(0), ";
                    }
                    else{
                        arm_sveCode << "svdup_n_f64(1), ";
                    }
                    arm_sveCode << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                          << "));" << std::endl;
                }
                else
                {
                    arm_sveCode << currentNode->getData()->getVectorial().type << " "
                          << nodeToNameMapping[currentNode.get()] << " = "
                          << reductionOpToStr[operation.operation] << "("
                          << "svwhilelt_b64(0," << ((1 << operation.reductionCount) - 1) << "),";
                    if(operation.operation == "Reduction +"){
                        arm_sveCode << "svdup_n_f64(0), ";
                    }
                    else{
                        arm_sveCode << "svdup_n_f64(1), ";
                    }
                    arm_sveCode << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                          << "));" << std::endl;
                }
            }
            else{
                if(unariOpToStr.find(operation.operation) == unariOpToStr.end()){
                    unariOpToStr[operation.operation] = "???";
                }
                arm_sveCode << scalarTypeToVecType[currentNode->getData()->getVectorial().type] << " "
                          << nodeToNameMapping[currentNode.get()] << " = "
                          << unariOpToStr[operation.operation] << "(" << nodeToNameMapping[currentNode->getInputs(0).front().get()]
                          << ");" << std::endl;
            }
        }
        else
        {
            assert(0);
        }
    }
}

/////////////////////////////////////////////////
/// Tree processing
/////////////////////////////////////////////////

void Print3AC(std::vector<std::shared_ptr<Node>>& inOutputs){
    // Do it from the inputs
    std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);
    {
        auto maxDistanceFromInputs = Autovec::GetMaxDistanceFromOutputs(inOutputs);
        std::sort(allNodes.begin(), allNodes.end(),
                  [&](auto& n1, auto& n2){
            assert(maxDistanceFromInputs.find(n1.get()) != maxDistanceFromInputs.end());
            assert(maxDistanceFromInputs.find(n2.get()) != maxDistanceFromInputs.end());
            return maxDistanceFromInputs[n1.get()] > maxDistanceFromInputs[n2.get()];
        });
    }

    int cptLoad = 0;
    int cptVar = 0;

    std::unordered_map<Node*, std::string> nodeToNameMapping;

    for(auto currentNode : allNodes){
        assert(currentNode->getData());
        {
            if(currentNode->getNbInputs(0) == 0){
                assert(currentNode->getNbInputs(1) == 0);
                assert(currentNode->getData()->isScalarLoadOrSet() || currentNode->getData()->isVectorialLoadOrSet());
                const std::string currentName = std::string("ld_").append(std::to_string(cptLoad++));
                nodeToNameMapping[currentNode.get()] = currentName;
            }
            else{
                if(currentNode->getNbOutputs()){
                    assert(currentNode->getData()->isScalarOperation() || currentNode->getData()->isVectorialOperation());
                    const std::string currentName = std::string("tmp_").append(std::to_string(cptVar++));
                    nodeToNameMapping[currentNode.get()] = currentName;
                }
                else{
                    assert(currentNode->getData()->isScalarStore() || currentNode->getData()->isVectorialStore());
                }
            }
        }

        if(currentNode->getData()->isScalarStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            std::cout << "Store(" << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ");" << std::endl;
        }
        else if(currentNode->getData()->isVectorialStore()){
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            auto store = currentNode->getData()->getVectorial().getStore();
            std::cout << "Store(" << nodeToNameMapping[currentNode->getInputs(0).front().get()] << ", " << store.ptr << ", ("
                      << ContainerToStr(store.offset) << "));" << std::endl;
        }
        else if(currentNode->getData()->isScalarLoad()){
            auto load = currentNode->getData()->getScalar().getLoad();
            std::cout << nodeToNameMapping[currentNode.get()] << " = Load(" << load.ptr << ", " << load.offset << ");" << std::endl;
        }
        else if(currentNode->getData()->isScalarSet()){
            auto set = currentNode->getData()->getScalar().getSet();
            std::cout << nodeToNameMapping[currentNode.get()] << " = Set(" << Instruction::ValueToString(set.value) << ");" << std::endl;
        }
        else if(currentNode->getData()->isVectorialLoad()){
            auto load = currentNode->getData()->getVectorial().getLoad();
            std::cout << nodeToNameMapping[currentNode.get()] << " = vLoad(" << load.ptr << ", ("
                      << ContainerToStr(load.offset) << "));" << std::endl;
        }
        else if(currentNode->getData()->isVectorialSet()){
            auto set = currentNode->getData()->getVectorial().getSet();
            std::cout << nodeToNameMapping[currentNode.get()] << " = vSet(" << Instruction::ValueToString(set.value) << ");" << std::endl;
        }
        else if(currentNode->getData()->isScalarOperation()){
            auto operation = currentNode->getData()->getScalar().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            std::cout << nodeToNameMapping[currentNode.get()] << " = Operation(" << operation.operation << ", " << nodeToNameMapping[currentNode->getInputs(0).front().get()];
            if(currentNode->getInputs(1).size()){
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                std::cout << ", " << nodeToNameMapping[currentNode->getInputs(1).front().get()];
            }
            std::cout << ");" << std::endl;
        }
        else if(currentNode->getData()->isVectorialOperation()){
            auto operation = currentNode->getData()->getVectorial().getOperation();
            assert(currentNode->getInputs(0).size());
            assert(nodeToNameMapping.find(currentNode->getInputs(0).front().get()) != nodeToNameMapping.end());
            std::cout << nodeToNameMapping[currentNode.get()] << " = Operation(" << operation.operation << ", " << nodeToNameMapping[currentNode->getInputs(0).front().get()];
            if(currentNode->getInputs(1).size()){
                assert(nodeToNameMapping.find(currentNode->getInputs(1).front().get()) != nodeToNameMapping.end());
                std::cout << ", " << nodeToNameMapping[currentNode->getInputs(1).front().get()];
            }
            if(operation.useIndices){
                std::cout << ", " << ContainerToStr(operation.indices);
            }
            std::cout << ");" << std::endl;
        }
        else{
            assert(0);
        }
    }
}

void Print3AC(std::shared_ptr<Node> inRoot){
    std::vector<std::shared_ptr<Node>> outputs{inRoot};
    Print3AC(outputs);
}

void TreeToDot(std::vector<std::shared_ptr<Autovec::Node>>& inOutputs, const std::string outFilename,
               const bool lightGraph = false){
    std::ofstream dotFile;
    dotFile.open (outFilename);
    if(!dotFile.is_open()){
        std::cout << "Error when tried to create file " << outFilename << ", abort..." << std::endl;
        return;
    }

    dotFile << "digraph G {\n";

    std::vector<std::shared_ptr<Node>> allNodes = Autovec::GetAllNodes(inOutputs);

    std::unordered_map<const Node*, int> currentNodeId;
    for(auto currentNode : allNodes){
        currentNodeId[currentNode.get()] = int(currentNodeId.size());
    }

    for(auto currentNode : allNodes){
        for(int idxInputType = 0 ; idxInputType < 2 ; ++idxInputType){
            for(auto& input : currentNode->getInputs(idxInputType)){
                assert(currentNodeId.find(input.get()) != currentNodeId.end());
                dotFile << "\t" << currentNodeId[input.get()]  << " -> " << currentNodeId[currentNode.get()] << "\n";
            }
        }

        dotFile << "\t" << currentNodeId[currentNode.get()] << " [label=\"" << currentNode->getData()->getDescription(lightGraph) << "\"];\n";
    }

    dotFile << "}\n";

    dotFile.close();
}
}
