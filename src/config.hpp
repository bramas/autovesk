#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <vector>
#include <iostream>
#include <string>
#include <algorithm>

enum SplitLoadsAndStores {Naive, GreedyWithConfig};
enum SplitOperations {WithoutOrder, VecTogetherTrue, VecTogetherFalse, VecOneByOneTrue, VecOneByOneFalse, GreedySplitOp};
enum Reduction {DisableReduction, EnableReduction, GreedyReduction};
enum ConstantPropagation {DisableConstantPropagation, EnableConstantPropagation};
enum ExportFormat {AVX512, AVX512ASM, ARMSVE};

struct Config
{
    static void PrintHelp(){
        std::cout << "[HELP] ## Here are the possible params you can pass:\n";
        std::cout << "[HELP] # format to export:\n";
        std::cout << "[HELP] -export [avx512*, avx512asm, armsve]\n";
        std::cout << "[HELP] # The strategy to split the loads/stores:\n";
        std::cout << "[HELP] -split-ls [naive, greedy*]\n";
        std::cout << "[HELP] # The strategy to split the operations (greedy is possible only if split-ls is greedy):\n";
        std::cout << "[HELP] -split-op [no-order*, together-true, together-false, one-true, one-false, greedy**]\n";
        std::cout << "[HELP] # If reduction should be used or not (greedy is possible only if split-ls is greedy):\n";
        std::cout << "[HELP] -reduction [enable*, disable, greedy**]\n";
        std::cout << "[HELP] # If duplicate code is removed or not:\n";
        std::cout << "[HELP] -remove-duplicate [enable*, disable]\n";
        std::cout << "[HELP] # If constant propagation should be used or not:\n";
        std::cout << "[HELP] -constant-propagation [enable*, disable]\n";
        std::cout << "[HELP] # If the number of used registers should be minimized or not:\n";
        std::cout << "[HELP] -min-registers [enable*, disable]\n";
        std::cout << "[HELP] # Enable verbose output:\n";
        std::cout << "[HELP] -verbose\n";
        std::cout << "[HELP] # Vector size:\n";
        std::cout << "[HELP] -vecsize [INT]\n";
        std::cout << "[HELP] \n";
        std::cout << "[HELP] * are the default.\n";
        std::cout << std::endl;
    }

    bool valid;
    SplitLoadsAndStores splitLoadsAndStores;
    SplitOperations splitOperations;
    Reduction reductionMode;
    bool constPropagationEnabled;
    bool removeDuplicate;
    bool verbose;
    bool minimizeRegisters;
    int vecsize;
    ExportFormat format;

    Config(){
        valid = true;
        splitLoadsAndStores = SplitLoadsAndStores::GreedyWithConfig;
        splitOperations = SplitOperations::GreedySplitOp;
        reductionMode = GreedyReduction;
        constPropagationEnabled = true;
        removeDuplicate = true;
        verbose = false;
        minimizeRegisters = true;
        vecsize = 8;
        format = ExportFormat::AVX512;
    }

    Config(int argc, char** argv) : Config(){
        int idxParam = 1;
        while(idxParam < argc){
            const std::string arg = argv[idxParam];
            if(arg == "-split-ls"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -split-ls should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysLs{"naive", "greedy"};
                    const long int keyPos = std::distance(keysLs.begin(), std::find(keysLs.begin(), keysLs.end(), argv[idxParam+1]));
                    if(keyPos == int(keysLs.size())){
                        std::cout << "[ERROR] -split-ls cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    splitLoadsAndStores = (SplitLoadsAndStores)keyPos;
                    if(splitLoadsAndStores == Naive){
                        reductionMode = EnableReduction;
                        splitOperations = WithoutOrder;
                    }
                    else{
                        reductionMode = GreedyReduction;
                        splitOperations = GreedySplitOp;
                    }
                    idxParam += 2;
                }
            }
            else if(arg == "-export"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -export should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysLs{"avx512", "avx512asm", "armsve"};
                    const long int keyPos = std::distance(keysLs.begin(), std::find(keysLs.begin(), keysLs.end(), argv[idxParam+1]));
                    if(keyPos == int(keysLs.size())){
                        std::cout << "[ERROR] -export cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    format = (ExportFormat)keyPos;
                    idxParam += 2;
                }
            }
            else if(arg == "-split-op"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -split-op should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysOp{"no-order", "together-true", "together-false", "one-true", "one-false", "greedy"};
                    const long int keyPos = std::distance(keysOp.begin(), std::find(keysOp.begin(), keysOp.end(), argv[idxParam+1]));
                    if(keyPos == int(keysOp.size())){
                        std::cout << "[ERROR] -split-op cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    splitOperations = (SplitOperations)keyPos;
                    if(splitLoadsAndStores == Naive && splitOperations == GreedySplitOp){
                        std::cout << "[ERROR] -split-op greedy is possible only if split-ls is greedy\n.";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    idxParam += 2;
                }
            }
            else if(arg == "-reduction"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -reduction should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysRed{"disable", "enable", "greedy"};
                    const long int keyPos = std::distance(keysRed.begin(), std::find(keysRed.begin(), keysRed.end(), argv[idxParam+1]));
                    if(keyPos == int(keysRed.size())){
                        std::cout << "[ERROR] -reduction cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    reductionMode = (Reduction)keyPos;
                    if(splitLoadsAndStores == Naive && reductionMode == GreedyReduction){
                        std::cout << "[ERROR] -reduction greedy is possible only if split-ls is greedy\n.";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    idxParam += 2;
                }
            }
            else if(arg == "-remove-duplicate"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -remove-duplicate should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysRed{"disable", "enable"};
                    const long int keyPos = std::distance(keysRed.begin(), std::find(keysRed.begin(), keysRed.end(), argv[idxParam+1]));
                    if(keyPos == int(keysRed.size())){
                        std::cout << "[ERROR] -remove-duplicate cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    removeDuplicate = keyPos;
                    idxParam += 2;
                }
            }
            else if(arg == "-constant-propagation"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -constant-propagation should be followed by a parameter.\n";
                    PrintHelp();
                    valid = false;
                    return;
                }
                else{
                    const std::vector<std::string> keysRed{"disable", "enable"};
                    const long int keyPos = std::distance(keysRed.begin(), std::find(keysRed.begin(), keysRed.end(), argv[idxParam+1]));
                    if(keyPos == int(keysRed.size())){
                        std::cout << "[ERROR] -constant-propagation cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        valid = false;
                        return;
                    }
                    constPropagationEnabled = keyPos;
                    idxParam += 2;
                }
            }
            else if(arg == "-min-registers"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -min-registers should be followed by a parameter.\n";
                    PrintHelp();
                    minimizeRegisters = true;
                    return;
                }
                else{
                    const std::vector<std::string> keysRed{"disable", "enable"};
                    const long int keyPos = std::distance(keysRed.begin(), std::find(keysRed.begin(), keysRed.end(), argv[idxParam+1]));
                    if(keyPos == int(keysRed.size())){
                        std::cout << "[ERROR] -min-registers cannot be followed by " << argv[idxParam+1] << ".\n";
                        PrintHelp();
                        minimizeRegisters = true;
                        return;
                    }
                    minimizeRegisters = keyPos;
                    idxParam += 2;
                }
            }
            else if(arg == "-vecsize"){
                if(idxParam+1 == argc){
                    std::cout << "[ERROR] -vecsize should be followed by a parameter.\n";
                    PrintHelp();
                    return;
                }
                else{
                    vecsize = atoi(argv[idxParam+1]);
                    idxParam += 2;
                }
            }
            else if(arg == "-verbose"){
                verbose = true;
                idxParam += 1;
            }
            else /*if(arg == "--hep")*/{
                PrintHelp();
                valid = false;
                return;
            }
        }
    }

    Config(SplitLoadsAndStores inSplitLoadsAndStores, SplitOperations inSplitOperations,
           Reduction inReduction, ConstantPropagation constPropagation, bool inVerbose,
           int inVecsize){
        splitLoadsAndStores = inSplitLoadsAndStores;
        splitOperations = inSplitOperations;
        reductionMode = inReduction;
        constPropagationEnabled = (constPropagation == EnableConstantPropagation);
        verbose = inVerbose;
        vecsize = inVecsize;
        format = ExportFormat::AVX512;
    }

    void printConfig() const{
        std::cout << "[INFO] ## Current config:\n";
        std::cout << "[INFO] # The strategy to split the loads/stores:\n";
        const std::vector<std::string> keysLs{"naive", "greedy"};
        std::cout << "[INFO] -split-ls " << keysLs[int(splitLoadsAndStores)] << "\n";
        const std::vector<std::string> keysExport{"avx512", "avx512asm", "armsve"};
        std::cout << "[INFO] -export " << keysExport[int(format)] << "\n";
        std::cout << "[INFO] # The strategy to split the operations:\n";
        const std::vector<std::string> keysOp{"no-order", "together-true", "together-false", "one-true", "one-false", "greedy"};
        std::cout << "[INFO] -split-op " << keysOp[int(splitOperations)] << "\n";
        std::cout << "[INFO] # If reduction should use or not:\n";
        const std::vector<std::string> keysRed{"disable", "enable", "greedy"};
        std::cout << "[INFO] -reduction " << keysRed[int(reductionMode)] << "\n";
        const std::vector<std::string> keysRemoveDup{"disable", "enable"};
        std::cout << "[INFO] -remove-duplicate " << keysRemoveDup[int(removeDuplicate)] << "\n";
        const std::vector<std::string> keysCstProp{"disable", "enable"};
        std::cout << "[INFO] -constant-propagation " << keysCstProp[int(constPropagationEnabled)] << "\n";
        const std::vector<std::string> keysMinRegisters{"disable", "enable"};
        std::cout << "[INFO] -min-registers " << keysMinRegisters[int(minimizeRegisters)] << "\n";
        std::cout << "[INFO] -vecsize " << vecsize << "\n";
        std::cout << "[INFO] # Enable verbose output:\n";
        std::cout << "[INFO] " << (verbose?"verbose":"silent") << "\n";
        std::cout << "[INFO] \n";
        std::cout << std::endl;
    }

};
#endif
