#ifndef SPLIT_CONFIG_HPP
#define SPLIT_CONFIG_HPP
#include<vector>
#include <iostream>
#include <algorithm>
#include <set>
#include <queue>

class SplitsGenerator{
    std::vector<int> groupSizes; // For example 4, 4, 6, if there are 4 loads, 4 loads and 6 stores
    const int vecSize; // VEC_SIZE
    std::vector<int> currentStateIndexes;
    std::vector<std::vector<int>> currentState;

    public:
    SplitsGenerator(std::vector<int>& inGroupSizes, const int inVecSize): groupSizes{inGroupSizes}, vecSize{inVecSize} {
        currentState.resize(groupSizes.size());
        currentStateIndexes.resize(groupSizes.size());
        
        for(int idxGroup = 0 ; idxGroup < int(groupSizes.size()) ; ++idxGroup){
            if(groupSizes[idxGroup] == 0){
                currentStateIndexes[idxGroup] = -1;
            }
            else{
                const int firstVec = groupSizes[idxGroup]%vecSize;
                if(firstVec){
                    currentState[idxGroup].emplace_back(firstVec);
                    currentStateIndexes[idxGroup] = 0;
                }
                else{
                    currentStateIndexes[idxGroup] = -1;
                }
                int remainValues = groupSizes[idxGroup]-firstVec;
                while(remainValues){
                    currentState[idxGroup].emplace_back(vecSize);
                    remainValues -= vecSize;
                }
            }
        }
    }

    const std::vector<int>& getSplitForGroup(const int inGroupIdx) const{
        return currentState[inGroupIdx];

    } // Return the split for the current group, for example 4, 2 (one group of 4 instructions, and one group of 2)

    const std::vector<std::vector<int>>& getSplitForAllGroups() const{
        return currentState;
    }

    bool nextPermutation(){
        int idxGroup = 0;
        while(idxGroup < int(groupSizes.size())){
            if(int(currentState[idxGroup].size()) <= 1 || currentStateIndexes[idxGroup] == -1){
              idxGroup += 1;
            }
            else if(currentStateIndexes[idxGroup]+1 == int(currentState[idxGroup].size())){
                std::swap(currentState[idxGroup][0], currentState[idxGroup][currentStateIndexes[idxGroup]]);
                currentStateIndexes[idxGroup] = 0;
                idxGroup += 1;
            }
            else{
                std::swap(currentState[idxGroup][currentStateIndexes[idxGroup]],
                          currentState[idxGroup][currentStateIndexes[idxGroup]+1]);
                currentStateIndexes[idxGroup] += 1;
                break;
            }
        }
        return idxGroup != int(groupSizes.size());
    }

    std::vector<std::vector<std::vector<int>>> getAllSplits(){
        std::vector<std::vector<std::vector<int>>> allSplits;
        
        do{
            std::vector<std::vector<int>> currentSplit = getSplitForAllGroups();
            allSplits.emplace_back(currentSplit);
        } while(nextPermutation());

        return allSplits;
    }

};
#endif