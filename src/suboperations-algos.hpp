#ifndef SUBOPERATIONS_ALGOS_HPP
#define SUBOPERATIONS_ALGOS_HPP
#include <vector>
#include <cmath>
#include <limits>
#include <cassert>
#include <queue>
#include <utility>
#include <algorithm> 
#include "global.hpp"

////////////////////////////////////////////////////////////////////////////

std::vector<std::vector<int>> GetSubOperations1by1(const std::vector<std::vector<double>>& inScores,
                                                   const int VEC_SIZE,
                                                   [[maybe_unused]] const bool mergeScore = true){
    assert(VEC_SIZE >= 2);
    const int nbVectors = (int(inScores.size()) + VEC_SIZE - 1)/VEC_SIZE;

    std::vector<int> nodesToProceed(inScores.size());
    for(int idxNode = 0 ; idxNode < int(inScores.size()) ; ++idxNode){
        nodesToProceed[idxNode] = idxNode;
    }

    if(nbVectors == 1){
        return std::vector<std::vector<int>>{nodesToProceed};
    }

    std::vector<std::vector<int>> subOperations(nbVectors);
    [[maybe_unused]] const int margin = nbVectors*VEC_SIZE - int(inScores.size());

    // Find nbVectors more far nodes
    {
        double worstScore = std::numeric_limits<double>::max();
        int idxBest1 = -1, idxBest2 = -1;
        for(int idx1 = 0 ; idx1 < int(nodesToProceed.size()-1) ; ++idx1){
            for(int idx2 = idx1+1 ; idx2 < int(nodesToProceed.size()) ; ++idx2){
                const int node1 = nodesToProceed[idx1];
                const int node2 = nodesToProceed[idx2];
                if(worstScore > inScores[node1][node2]){
                    worstScore = inScores[node1][node2];
                    idxBest1 = idx1;
                    idxBest2 = idx2;
                }
            }
        }
        assert(idxBest1 != -1 && idxBest2 != -1);

        subOperations[0].push_back(nodesToProceed[idxBest1]);
        subOperations[1].push_back(nodesToProceed[idxBest2]);

        assert( idxBest1 < idxBest2 );
        std::swap(nodesToProceed[idxBest2], nodesToProceed.back());
        nodesToProceed.pop_back();
        std::swap(nodesToProceed[idxBest1], nodesToProceed.back());
        nodesToProceed.pop_back();
    }

    for(int idxVec = 2 ; idxVec < nbVectors ; ++idxVec){
        double worstScore = std::numeric_limits<double>::max();
        int idxBest1 = -1;
        for(int idx1 = 0 ; idx1 < int(nodesToProceed.size()) ; ++idx1){
            double currentScore = 0;
            const int node1 = nodesToProceed[idx1];
            for(int idxVecOther = 0 ; idxVecOther < idxVec ; ++idxVecOther){
                for(int idx2 = 0 ; idx2 < int(subOperations[idxVecOther].size()) ; ++idx2){
                    const int node2 = subOperations[idxVecOther][idx2];
                    currentScore += inScores[node1][node2];
                }
            }
            if(worstScore > currentScore){
                worstScore = currentScore;
                idxBest1 = idx1;
            }
        }
        assert(idxBest1 != -1);

        subOperations[idxVec].push_back(nodesToProceed[idxBest1]);
        std::swap(nodesToProceed[idxBest1], nodesToProceed.back());
        nodesToProceed.pop_back();
    }

    while(nodesToProceed.size()){
        double bestScore = -1;
        int idxBest1 = -1;
        int idxBestVec = -1;
        for(int idx1 = 0 ; idx1 < int(nodesToProceed.size()) ; ++idx1){
            const int node1 = nodesToProceed[idx1];
            for(int idxVec = 0 ; idxVec < nbVectors ; ++idxVec){
                if(int(subOperations[idxVec].size()) != VEC_SIZE){
                    double currentScore = 0;
                    for(int idx2 = 0 ; idx2 < int(subOperations[idxVec].size()) ; ++idx2){
                        const int node2 = subOperations[idxVec][idx2];
                        currentScore += inScores[node1][node2];
                    }
                    if(bestScore < currentScore){
                        bestScore = currentScore;
                        idxBest1 = idx1;
                        idxBestVec = idxVec;
                    }
                }
            }
        }
        assert(idxBest1 != -1 && idxBestVec != -1);

        subOperations[idxBestVec].push_back(nodesToProceed[idxBest1]);
        std::swap(nodesToProceed[idxBest1], nodesToProceed.back());
        nodesToProceed.pop_back();
    }
    return subOperations;
}


std::vector<std::vector<int>> GetSubOperationsVecTogether(const std::vector<std::vector<double>>& inScores,
                                                          const int VEC_SIZE,
                                                          const bool heavyFirst = false){
    assert(VEC_SIZE >= 2);
    const int nbVectors = (int(inScores.size()) + VEC_SIZE - 1)/VEC_SIZE;
    [[maybe_unused]] const int sizeLastVector = (nbVectors*VEC_SIZE) - int(inScores.size());

    std::queue<std::vector<int>> partitionsCurrent;
    {
        std::vector<int> nodesToProceed(inScores.size());
        for(int idxNode = 0 ; idxNode < int(inScores.size()) ; ++idxNode){
            nodesToProceed[idxNode] = idxNode;
        }

        if(nbVectors == 1){
            return std::vector<std::vector<int>>{nodesToProceed};
        }
        partitionsCurrent.push(nodesToProceed);
    }

    while(int(partitionsCurrent.size()) != nbVectors){
        assert(partitionsCurrent.size());
        std::vector<int> nodesToProceed(partitionsCurrent.front());
        partitionsCurrent.pop();
        if(int(nodesToProceed.size()) <= VEC_SIZE){
            partitionsCurrent.push(nodesToProceed);
            continue;
        }

        const int totalVecInPartition = (int(nodesToProceed.size()) + VEC_SIZE - 1)/VEC_SIZE;
        assert(totalVecInPartition > 1);
        const int margin = totalVecInPartition * VEC_SIZE - int(nodesToProceed.size());

        const int sizeMaxFirstPartition = (totalVecInPartition/2)*VEC_SIZE;
        [[maybe_unused]] const int sizeMinFirstPartition = sizeMaxFirstPartition - margin;
        const int sizeMaxSecondPartition = ((totalVecInPartition+1)/2)*VEC_SIZE;
        [[maybe_unused]] const int sizeMinSecondPartition = sizeMaxSecondPartition-margin;

        // First find lowest connection
        int idxBest1 = -1, idxBest2 = -1;
        double lowestWeight = std::numeric_limits<double>::max();
        for(int idx1 = 0 ; idx1 < int(nodesToProceed.size()-1) ; ++idx1){
            for(int idx2 = idx1+1 ; idx2 < int(nodesToProceed.size()) ; ++idx2){
                const int node1 = nodesToProceed[idx1];
                const int node2 = nodesToProceed[idx2];
                if(lowestWeight > inScores[node1][node2]){
                    lowestWeight = inScores[node1][node2];
                    idxBest1 = idx1;
                    idxBest2 = idx2;
                }
            }
        }
        assert(idxBest1 != -1 && idxBest2 != -1);

        std::vector<int> p1(1, nodesToProceed[idxBest1]);
        std::vector<int> p2(1, nodesToProceed[idxBest2]);

        assert( idxBest1 < idxBest2 );
        std::swap(nodesToProceed[idxBest2], nodesToProceed.back());
        nodesToProceed.pop_back();
        std::swap(nodesToProceed[idxBest1], nodesToProceed.back());
        nodesToProceed.pop_back();

        if(heavyFirst){
            std::priority_queue<std::pair<double, int>> heaviest;
            for(int idxNode = 0 ; idxNode < int(nodesToProceed.size()) ; ++idxNode){
                double totalScore = 0;
                for(int idxOther = 0 ; idxOther < int(nodesToProceed.size()) ; ++idxOther){
                    if(idxNode != idxOther){
                        const int node1 = nodesToProceed[idxNode];
                        const int node2 = nodesToProceed[idxOther];
                        totalScore += inScores[node1][node2];
                    }
                }
                heaviest.push(std::make_pair(totalScore, nodesToProceed[idxNode]));
            }
            nodesToProceed.clear();

            while(heaviest.size()){
                if(int(p1.size()) == sizeMaxFirstPartition){
                    assert(int(p2.size()) != sizeMaxSecondPartition);
                    p2.push_back(heaviest.top().second);
                }
                else if(int(p2.size()) == sizeMaxSecondPartition){
                    assert(int(p1.size()) != sizeMaxFirstPartition);
                    p1.push_back(heaviest.top().second);
                }
                else{
                    // Agregate highest connection to minimize edge cut
                    const int node1 = heaviest.top().second;
                    double cutP1 = 0;
                    for(int idx1 = 0 ; idx1 < int(p1.size()) ; ++idx1){
                        const int node2 = p1[idx1];
                        cutP1 += inScores[node1][node2];
                    }
                    double cutP2 = 0;
                    for(int idx2 = 0 ; idx2 < int(p2.size()) ; ++idx2){
                        const int node2 = p2[idx2];
                        cutP2 += inScores[node1][node2];
                    }
                    if(cutP1 < cutP2 || (cutP1 == cutP2 && (sizeMaxSecondPartition - p2.size()) > (sizeMaxFirstPartition - p1.size()))){
                        p2.push_back(node1);
                    }
                    else{
                        p1.push_back(node1);
                    }
                }
                heaviest.pop();
            }
        }
        else{
            while(nodesToProceed.size()){
                if(int(p1.size()) == sizeMaxFirstPartition){
                    assert(int(p2.size()) != sizeMaxSecondPartition);
                    p2.push_back(nodesToProceed.back());
                    nodesToProceed.pop_back();
                }
                else if(int(p2.size()) == sizeMaxSecondPartition){
                    assert(int(p1.size()) != sizeMaxFirstPartition);
                    p1.push_back(nodesToProceed.back());
                    nodesToProceed.pop_back();
                }
                else{
                    int bestNode = -1;
                    double bestScoreP1 = -1;
                    double bestScoreP2 = -1;
                    for(int idxNode = 0 ; idxNode < int(nodesToProceed.size()) ; ++idxNode){
                        const int node1 = nodesToProceed[idxNode];
                        double scoreP1 = 0;
                        for(int idx1 = 0 ; idx1 < int(p1.size()) ; ++idx1){
                            const int node2 = p1[idx1];
                            scoreP1 += inScores[node1][node2];
                        }
                        double scoreP2 = 0;
                        for(int idx2 = 0 ; idx2 < int(p2.size()) ; ++idx2){
                            const int node2 = p2[idx2];
                            scoreP2 += inScores[node1][node2];
                        }
                        if(std::max(bestScoreP1, bestScoreP2) < std::max(scoreP1,scoreP2)){
                            bestNode = idxNode;
                            bestScoreP1 = scoreP1;
                            bestScoreP2 = scoreP2;
                        }
                    }
                    assert(bestNode != -1);

                    if(bestScoreP1 < bestScoreP2 || (bestScoreP1 == bestScoreP2 && (sizeMaxSecondPartition - p2.size()) > (sizeMaxFirstPartition - p1.size()))){
                        p2.push_back(nodesToProceed[bestNode]);
                    }
                    else{
                        p1.push_back(nodesToProceed[bestNode]);
                    }
                    std::swap(nodesToProceed[bestNode], nodesToProceed.back());
                    nodesToProceed.pop_back();
                }
            }
        }
        partitionsCurrent.push(p1);
        partitionsCurrent.push(p2);
    }
    assert(nbVectors == int(partitionsCurrent.size()));

    std::vector<std::vector<int>> subOperations;
    while(partitionsCurrent.size()){
        subOperations.push_back(partitionsCurrent.front());
        partitionsCurrent.pop();
        assert(int(subOperations.back().size()) <= VEC_SIZE);
    }
    return subOperations;
}

////////////////////////////////////////////////////////////////////////////

#endif
