#include <immintrin.h>
#include <iostream>
#include <iostream>
#include <memory>

#include "timer.hpp"

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// For debug

void printVec(__m512d inVec){
    double res[8];
    _mm512_storeu_pd(res, inVec);
    std::cout << "{";
    for(int idx = 0 ; idx < 8 ; ++idx){
        if(idx){
            std::cout << ",";
        }
        std::cout << " [" << idx << "] " << res[idx];
    }
    std::cout << "}" << std::endl;
}

void printVec(__m512i inVec){
    long int res[8];
    _mm512_storeu_si512(res, inVec);
    std::cout << "{";
    for(int idx = 0 ; idx < 8 ; ++idx){
        if(idx){
            std::cout << ",";
        }
        std::cout << " [" << idx << "] " << res[idx];
    }
    std::cout << "}" << std::endl;
}

void assert_approx_equal_core(const double inGood, const double inBad, const int inLineNumber){
    if((inGood == 0 && std::abs(inBad) > 1E-14)
        || (inGood != 0 && std::abs((inBad-inGood)/inGood) > 1E-14)){
        std::cout << "[ERROR] should be " << inGood
                  << " , but is " << inBad
                  << " at line " << inLineNumber << std::endl;
    }
}

#define assert_approx_equal(GOOD, BAD) assert_approx_equal_core((GOOD), (BAD), __LINE__)


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

void KernelExample2_vectorize(const double* tab0, const double* tab1, double* tab2){    
    __m512d ld_0 = _mm512_loadu_pd( tab0+0);
    __m512d ld_1 = _mm512_castpd128_pd512(_mm_loadu_pd( tab0+8));
    __m512d ld_2 = _mm512_mask_i64gather_pd( _mm512_setzero_pd(), 31,  _mm512_set_epi64(-1, -1, -1, 8, 6, 4, 2, 0),tab1, sizeof(double));
    __m512d tmp_0 = _mm512_mask_permutexvar_pd(_mm512_setzero_pd(), 63, _mm512_set_epi64(-1, -1, 7, 6, 5, 4, 3, 2),ld_0);
    __m512d tmp_1 = _mm512_mask_permutexvar_pd(_mm512_setzero_pd(), 192, _mm512_set_epi64(1, 0, -1, -1, -1, -1, -1, -1),ld_1);
    __m512d tmp_2 = _mm512_mask_permutexvar_pd(_mm512_setzero_pd(), 3, _mm512_set_epi64(-1, -1, -1, -1, -1, -1, 4, 3),ld_2);
    __m512d tmp_3 = _mm512_or_pd(tmp_0, tmp_1);
    __m512d tmp_4 = _mm512_mask_permutexvar_pd(_mm512_setzero_pd(), 255, _mm512_set_epi64(2, 1, 0, 4, 3, 2, 1, 0),ld_2);
    __m512d tmp_5 = _mm512_mul_pd(ld_0, tmp_2);
    __m512d tmp_6 = _mm512_mul_pd(tmp_3, tmp_4);
    _mm_storeu_pd( tab2+8, _mm512_castpd512_pd128( tmp_5));
    _mm512_storeu_pd( tab2+0, tmp_6);
}

template <class NumType>
void KernelExample2(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx*2)%size];
    }
}


//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

// g++ -march=skylake-avx512 -mtune=skylake-avx512 test.cpp -o test.exe
void test(){
    std::cout << "[TEST]" << std::endl;
    const long int size = 10;
    double tab0[size];
    double tab1[size];
        
    for(long int idx = 0 ; idx < size ; ++idx){
        tab0[idx] = double(idx);
        tab1[idx] = double(idx);
    }
    
    for(long int idx = 0 ; idx < size ; ++idx){
        std::cout << "tab0[" << idx << "] = " << tab0[idx] << std::endl;
    }
    
    for(long int idx = 0 ; idx < size ; ++idx){
        std::cout << "tab1[" << idx << "] = " << tab1[idx] << std::endl;
    }
    
    double tab2[size] = {0};
    KernelExample2(tab0, tab1, tab2, size);
    
    double tab2_vec[size] = {0};
    KernelExample2_vectorize(tab0, tab1, tab2_vec);
    
    
    for(long int idx = 0 ; idx < size ; ++idx){
        std::cout << "idx " << idx << " good " << tab2[idx] << " bad " << tab2_vec[idx] << std::endl;
        assert_approx_equal(tab2[idx], tab2_vec[idx]);
    }
}

void bench(){
    std::cout << "[BENCH]" << std::endl;
    const long int nbRepeat = 500;
    const long int nbLoops = 1000;
    const long int size = 10;
    double tab0[size*nbLoops];
    double tab1[size*nbLoops];

    for(long int idx = 0 ; idx < size*nbLoops ; ++idx){
        tab0[idx] = double(idx%size);
        tab1[idx] = double(idx%size);
    }

    double tab2[size*nbLoops] = {0};
    Timer scalarTimer;
    for(long int idxRepeat = 0 ; idxRepeat < nbRepeat ; ++idxRepeat){
        for(long int idxLoop = 0 ; idxLoop < nbLoops ; ++idxLoop){
            KernelExample2(tab0+idxLoop*size, tab1+idxLoop*size, tab2+idxLoop*size, size);
        }
    }
    scalarTimer.stop();

    double tab2_vec[size*nbLoops] = {0};
    Timer vecTimer;
    for(long int idxRepeat = 0 ; idxRepeat < nbRepeat ; ++idxRepeat){
        for(long int idxLoop = 0 ; idxLoop < nbLoops ; ++idxLoop){
            KernelExample2_vectorize(tab0+idxLoop*size, tab1+idxLoop*size, tab2_vec+idxLoop*size);
        }
    }
    vecTimer.stop();

    for(long int idx = 0 ; idx < size*nbLoops ; ++idx){
        assert_approx_equal(tab2[idx], tab2_vec[idx]);
    }

    std::cout << "Scalar = " << scalarTimer.getElapsed() << std::endl;
    std::cout << "Vectorize = " << vecTimer.getElapsed() << std::endl;
}

void benchHeap(){
    std::cout << "[BENCH-HEAP]" << std::endl;
    const long int nbRepeat = 500;
    const long int nbLoops = 1000;
    const long int size = 10;
    std::unique_ptr<double[]> tab0(new double[size*nbLoops]());
    std::unique_ptr<double[]> tab1(new double[size*nbLoops]());

    for(long int idx = 0 ; idx < size*nbLoops ; ++idx){
        tab0[idx] = double(idx%size);
        tab1[idx] = double(idx%size);
    }

    std::unique_ptr<double[]> tab2(new double[size*nbLoops]());
    Timer scalarTimer;
    for(long int idxRepeat = 0 ; idxRepeat < nbRepeat ; ++idxRepeat){
        for(long int idxLoop = 0 ; idxLoop < nbLoops ; ++idxLoop){
            KernelExample2(tab0.get()+idxLoop*size, tab1.get()+idxLoop*size, tab2.get()+idxLoop*size, size);
        }
    }
    scalarTimer.stop();

    std::unique_ptr<double[]> tab2_vec(new double[size*nbLoops]());
    Timer vecTimer;
    for(long int idxRepeat = 0 ; idxRepeat < nbRepeat ; ++idxRepeat){
        for(long int idxLoop = 0 ; idxLoop < nbLoops ; ++idxLoop){
            KernelExample2_vectorize(tab0.get()+idxLoop*size, tab1.get()+idxLoop*size, tab2_vec.get()+idxLoop*size);
        }
    }
    vecTimer.stop();

    for(long int idx = 0 ; idx < size*nbLoops ; ++idx){
        assert_approx_equal(tab2[idx], tab2_vec[idx]);
    }

    std::cout << "Scalar = " << scalarTimer.getElapsed() << std::endl;
    std::cout << "Vectorize = " << vecTimer.getElapsed() << std::endl;
}

int main(){
    test();
    bench();
    benchHeap();

    return  0;
}
