#include "../src/autovesk.hpp"

/////////////////////////////////////////////////
/// Test functions
/////////////////////////////////////////////////

template <class NumType>
void KernelExample1(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[idx] * inValue2[idx];
    }
}

template <class NumType>
void KernelExample2(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx*2)%size];
    }
}

template <class NumType>
void KernelExample3(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(size == 10);
    outValue[0] = inValue2[4] + inValue1[2];
    outValue[3] = inValue2[4] + inValue1[2];
    outValue[1] = inValue2[3] + inValue1[5];
    outValue[9] = inValue2[1] + inValue1[5];
    outValue[5] = inValue2[8] + inValue1[4];
    outValue[6] = inValue2[6] + inValue1[4];
    outValue[4] = inValue2[0] + inValue1[0];
    outValue[7] = inValue2[1] + inValue1[0];
}


template <class NumType>
void KernelExample4(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+2)%size];
    }
}

template <class NumType>
void KernelExample5(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[(idx+2)%size] = inValue1[idx] * inValue2[idx];
    }
}


template <class NumType>
void KernelExample6(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = inValue1[(idx+2)%size] * inValue2[(idx+4)%size];
    }
}

template <class NumType>
void KernelExample7(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = (idx%1 ? inValue1[idx] * inValue2[idx] : inValue1[idx] + inValue2[idx]);
    }
}

template <class NumType>
void KernelExample8(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    for(long int idx = 0 ; idx < size ; ++idx){
        outValue[idx] = (idx%1 ? inValue1[(idx+2)%size] * inValue2[(idx+2)%size] : inValue1[(idx+2)%size] + inValue2[(idx+2)%size]);
    }
}

template <class NumType>
void KernelExample9(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(size == 10);
    outValue[0] = inValue2[3] + inValue1[9];
    outValue[1] = inValue2[4] * inValue1[8];
    outValue[2] = inValue2[2] * inValue1[7];
    outValue[3] = inValue2[1] + inValue1[6];
    outValue[4] = inValue2[8] * inValue1[5];
    outValue[5] = inValue2[9] * inValue1[0];
    outValue[6] = inValue2[0] + inValue1[1];
    outValue[7] = inValue2[7] * inValue1[2];
    outValue[8] = inValue2[6] * inValue1[3];
    outValue[9] = inValue2[5] + inValue1[4];
}

template <class NumType>
void KernelExample10(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(2 <= size);
    // test of two levels operations with 2 loads and 1 store
    NumType x = inValue2[0] + inValue1[0];
    NumType y = inValue2[1] + inValue1[1];
    outValue[0] = x + inValue1[2];
    outValue[1] = y + inValue2[2];
}

template <class NumType>
void KernelExample11(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x,y;
    for(long int idx = 0 ; idx < size ; ++idx){
        x = inValue1[idx] + inValue2[idx];
        y = inValue1[(idx+2)%size] + inValue2[(idx+2)%size];
        outValue[idx] = x + y;
    }
}

template <class NumType>
void KernelExample12(const NumType* inValue1, [[maybe_unused]] const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample13(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] + inValue2[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample13bis(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] + inValue2[idx];
    }
    outValue[0] = x;
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] + inValue2[idx];
    }
    outValue[0] += x;
}

template <class NumType>
void KernelExample14(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] * inValue2[idx];
    }
    outValue[0] = x;
}

template <class NumType>
void KernelExample15(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(2 <= size);
    NumType x = NumType(0.);
    x = (x + inValue1[0] * inValue2[0]) + inValue1[1] * inValue2[1];

    outValue[0] = x;
}

template <class NumType>
void KernelExample16(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(size == 10);

    NumType x = NumType(0.);

    outValue[0] = (x + (inValue1[8] * inValue2[8])) + (inValue1[9] * inValue2[9]);
    outValue[1] = ((inValue1[1] + inValue2[1]) + (inValue1[2] + inValue2[2])) + (inValue1[3] + inValue2[3]);

    // Create other component to force load 1,2 and 3 to be in a separate vector then load 8 and 9
    outValue[2] = 0;
    for(int i = 4; i < 8; ++i)
        outValue[2] -= inValue1[i] - inValue2[i];
}

template <class NumType>
void KernelExample17(const NumType* inValue1, const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(2 <= size);
    outValue[0] = inValue1[0] * inValue2[0];
    outValue[1] = outValue[0] * inValue2[0];
}

template <class NumType>
void KernelExample18([[maybe_unused]] const NumType* inValue1, [[maybe_unused]] const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    assert(1 <= size);
    outValue[0] = 1;
}

template <class NumType>
void KernelExample19(const NumType* inValue1, const NumType* inValue2, NumType* outValue, const long size){
    NumType x = NumType(0.);
    NumType x2 = NumType(0.);
    for(long int idx = 0 ; idx < size ; ++idx){
        x += inValue1[idx] * inValue2[idx];
        x2 += inValue1[idx] * inValue2[idx];
    }
    x *= NumType(10.);
    x2 *= NumType(10.);
    outValue[0] = x;
    outValue[1] = x2;
}

template <class NumType>
void KernelExample20([[maybe_unused]] const NumType* inValue1, [[maybe_unused]] const NumType* inValue2, NumType* outValue, [[maybe_unused]] const long size){
    outValue[0] = ((NumType)1 + inValue1[1]) + ((NumType)2 + inValue1[0]);
}

template <class ckrt_real>
static inline void ckrt_prod_matvec33(const ckrt_real * mat1, const ckrt_real * vec1, ckrt_real * dest, [[maybe_unused]] const long size){
    for(int ii = 0; ii < std::min(3L,size); ii++) {
      dest[ii]=0;
      for(int jj = 0; jj < std::min(3L,size) && (ii * 3 + jj) < size; jj++) {
        dest[ii]+=mat1[ii * 3 + jj]*vec1[jj];
      }
    }
}

template <class ckrt_real>
static inline void ckrt_prod_matvec33_cumul(const ckrt_real * mat1, const ckrt_real * vec1, ckrt_real * dest, [[maybe_unused]] const long size){
    for(int ii = 0; ii < std::min(3L,size); ii++) {
      //dest[ii]=0;
      for(int jj = 0; jj < std::min(3L,size) && (ii * 3 + jj) < size; jj++) {
        dest[ii]+=mat1[ii * 3 + jj]*vec1[jj];
      }
    }
}

int main(int argc, char** argv){
    const Config config(argc, argv);
    if(!config.valid){
        return 1;
    }

    config.printConfig();

    using TreeType = Autovec::ScalarType;

    using BenchFunc = std::function<void(const TreeType*, const TreeType*, TreeType*, int)>;
    std::vector<std::pair<BenchFunc, std::string>> testFuncs;

   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample1<TreeType>, "KernelExample1"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample2<TreeType>, "KernelExample2"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample3<TreeType>, "KernelExample3"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample4<TreeType>, "KernelExample4"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample5<TreeType>, "KernelExample5"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample6<TreeType>, "KernelExample6"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample7<TreeType>, "KernelExample7"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample8<TreeType>, "KernelExample8"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample9<TreeType>, "KernelExample9"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample10<TreeType>, "KernelExample10"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample11<TreeType>, "KernelExample11"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample12<TreeType>, "KernelExample12"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample13<TreeType>, "KernelExample13"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample13bis<TreeType>, "KernelExample13bis"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample14<TreeType>, "KernelExample14"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample15<TreeType>, "KernelExample15"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample16<TreeType>, "KernelExample16"));
   testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample17<TreeType>, "KernelExample17"));
    testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample18<TreeType>, "KernelExample18"));
    testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample19<TreeType>, "KernelExample19"));
    testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(KernelExample20<TreeType>, "KernelExample20"));
    testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(ckrt_prod_matvec33<TreeType>, "ckrt_prod_matvec33"));
    testFuncs.emplace_back(std::make_pair<BenchFunc, std::string>(ckrt_prod_matvec33_cumul<TreeType>, "ckrt_prod_matvec33_cumul"));

    for(const auto& currentFunc : testFuncs){
        std::cout << "======================" << std::endl;
        std::cout << "Test " << currentFunc.second << std::endl;
        std::cout << "======================" << std::endl;

        std::cout << "Init data" << std::endl;

        const long int Size = 10;
        TreeType vec1[Size];
        TreeType vec2[Size];
        TreeType result[Size];

        for(long int idx = 0 ; idx < Size ; ++idx){
            vec1[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 0, idx));
            vec2[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 1, idx));
            result[idx].node->getData() = (Autovec::Instruction::make_scalar_load("double", 2, idx));
        }

        std::cout << "Run kernel" << std::endl;

        currentFunc.first(vec1, vec2, result, Size);

        std::cout << "Insert tree" << std::endl;

        for(long int idx = 0 ; idx < Size ; ++idx){
            assert(result[idx].node->getNbInputs(1) == 0 || result[idx].node->getNbInputs(0) != 0);
            if(result[idx].node->getNbInputs(0)
                    || result[idx].node->getData()->isSet()
                    || (result[idx].node->getData()->isLoad() &&
                        (result[idx].node->getData()->getScalar().getLoad().ptr != 2
                        || result[idx].node->getData()->getScalar().getLoad().offset != idx))){
                std::shared_ptr<Autovec::Node> newNode;
                newNode.reset(new Autovec::Node);
                newNode->getData() = (Autovec::Instruction::make_scalar_store("double", 2, idx));
                Autovec::Node::Connect(newNode, result[idx].node, 0);
                result[idx].node = newNode;
            }
        }

        std::cout << "======================" << std::endl;
        std::cout << "Resulting instructions" << std::endl;
        std::cout << "======================" << std::endl;

        std::vector<std::shared_ptr<Autovec::Node>> outputs;

        for(long int idx = 0 ; idx < Size ; ++idx){
            if(result[idx].node->getNbInputs(0)){
                outputs.emplace_back(result[idx].node);
            }
        }

        Print3AC(outputs);

        TreeToDot(outputs, Autovec::StringMerge("/tmp/scalar-", currentFunc.second, ".dot"), true);

        std::cout << "=================================" << std::endl;
        std::cout << "Vectorize (Vec size = " << config.vecsize << ")" << std::endl;
        std::cout << "=================================" << std::endl;

        //bool Verbose = true;
        //std::vector<std::shared_ptr<Autovec::Node>> vectorizedOutputs = Autovec::GraphTransformation::VectorizeGreedy(outputs, Verbose);

        std::vector<std::shared_ptr<Autovec::Node>>  vectorizedOutputs = Autovec::GraphTransformation::Vectorize(&outputs, config);

        Print3AC(vectorizedOutputs);

        TreeToDot(vectorizedOutputs, Autovec::StringMerge("/tmp/vectorized-", currentFunc.second, "-", config.vecsize, ".dot"));

        std::cout << "======================" << std::endl;
        std::cout << "AVX12" << std::endl;
        std::cout << "======================" << std::endl;

        if(config.format == ExportFormat::AVX512){
            std::stringstream stream;
            GenerateAVX512(vectorizedOutputs, stream, config.minimizeRegisters, config.vecsize);
            std::cout << stream.str() << std::endl;
        }
        else if(config.format == ExportFormat::AVX512ASM){
            std::stringstream stream;
            GenerateAVX512Asm(vectorizedOutputs, stream, config.minimizeRegisters, config.vecsize,
                              currentFunc.second, true);
            std::cout << stream.str() << std::endl;
        }
        else if(config.format == ExportFormat::ARMSVE){
            std::stringstream stream;
            GenerateARM_SVE(vectorizedOutputs, stream, config.minimizeRegisters, config.vecsize);
            std::cout << stream.str() << std::endl;
        }
    }

    return 0;
}
