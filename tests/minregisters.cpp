#include "../src/minimize-registers.hpp"
#include "../src/autovesk.hpp"

#include <random>

std::vector<MinimizeRegisters::InsDeps> NoUseIsStore(std::vector<MinimizeRegisters::InsDeps> && operations){
    std::set<int> used;
    // No output would mean it is a store
    for(int idx = int(operations.size())-1;  idx >= 0 ; --idx){
        auto iter = operations[idx].write.begin();
        while (iter != operations[idx].write.end()) {
            if(used.find(*iter) != used.end()){
                ++iter;
            }
            else{
                iter = operations[idx].write.erase(iter);
                operations[idx].isStore = true;
            }
            for(auto read : operations[idx].read){
                used.insert(read);
            }
        }
    }
    return operations;
}

std::vector<MinimizeRegisters::InsDeps> GenerateRand(const int nbOperations, const int widthDep = 10){
    std::mt19937 gen(0);
    std::uniform_int_distribution<> distrib(1, widthDep);

    std::vector<MinimizeRegisters::InsDeps> operations;

    for(int idx = 0;  idx < nbOperations ; ++idx){
        MinimizeRegisters::InsDeps op;
        op.write.emplace_back(idx);

        int prev = idx;
        for(int nbdeps = distrib(gen); prev > 0 && nbdeps ; --nbdeps){
            std::uniform_int_distribution<> distribDep(1, std::min(10, prev));
            prev -= distribDep(gen);
            op.read.emplace_back(prev);
        }
        operations.emplace_back(std::move(op));
    }

    return operations;
}

std::vector<MinimizeRegisters::InsDeps> ToyGraph(){
    std::vector<MinimizeRegisters::InsDeps> operations;
    if(true){
        operations.emplace_back(MinimizeRegisters::InsDeps{{0},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{1},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{2},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{3},{0}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{4},{1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{5},{2}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{6},{2}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{7},{0,4}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{8},{4,5}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{9},{6/*,5*/}});
    }
    else if(false){
        operations.emplace_back(MinimizeRegisters::InsDeps{{0},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{1},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{2},{0}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{3},{1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{4},{1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{5},{2,3}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{6},{4}});
    }
    else if(false){
        operations.emplace_back(MinimizeRegisters::InsDeps{{0},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{1},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{2},{0}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{3},{1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{4},{1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{5},{2,3}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{6},{2}});
    }
    else{
        operations.emplace_back(MinimizeRegisters::InsDeps{{0},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{1},{0}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{2},{}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{3},{2}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{4},{2}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{5},{2}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{6},{3}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{7},{3,4}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{8},{5,6}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{9},{0,1}});
        operations.emplace_back(MinimizeRegisters::InsDeps{{10},{8,9}});
    }
    return operations;
}

std::vector<MinimizeRegisters::InsDeps> GetRealData(){
    std::vector<std::vector<std::string>> vec;
    vec.push_back(std::vector<std::string>{"ld_0"});
    vec.push_back(std::vector<std::string>{"ld_1"});
    vec.push_back(std::vector<std::string>{"ld_2"});
    vec.push_back(std::vector<std::string>{"ld_3"});
    vec.push_back(std::vector<std::string>{"ld_4"});
    vec.push_back(std::vector<std::string>{"ld_5"});
    vec.push_back(std::vector<std::string>{"ld_6"});
    vec.push_back(std::vector<std::string>{"ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_0", "ld_7"});
    vec.push_back(std::vector<std::string>{"ld_8"});
    vec.push_back(std::vector<std::string>{"ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_1", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_2", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_3", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_4", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_5", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_6", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_7", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_8", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_9", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_10", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_11", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_12", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_13", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_14", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_15", "ld_1"});
    vec.push_back(std::vector<std::string>{"ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_16", "tmp_5", "tmp_0"});
    vec.push_back(std::vector<std::string>{"ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_17", "tmp_1", "tmp_2"});
    vec.push_back(std::vector<std::string>{"tmp_18", "tmp_3", "tmp_15"});
    vec.push_back(std::vector<std::string>{"tmp_19", "tmp_13", "tmp_12"});
    vec.push_back(std::vector<std::string>{"tmp_20", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_21", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_22", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_23", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_24", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_25", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_26", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_27", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_28", "tmp_7", "tmp_6"});
    vec.push_back(std::vector<std::string>{"tmp_29", "tmp_9", "tmp_8"});
    vec.push_back(std::vector<std::string>{"tmp_30", "tmp_11", "tmp_10"});
    vec.push_back(std::vector<std::string>{"tmp_31", "tmp_4", "tmp_14"});
    vec.push_back(std::vector<std::string>{"tmp_32", "ld_4"});
    vec.push_back(std::vector<std::string>{"ld_12"});
    vec.push_back(std::vector<std::string>{"ld_13"});
    vec.push_back(std::vector<std::string>{"ld_14"});
    vec.push_back(std::vector<std::string>{"tmp_33", "tmp_17", "tmp_18"});
    vec.push_back(std::vector<std::string>{"tmp_34", "tmp_31", "tmp_19"});
    vec.push_back(std::vector<std::string>{"tmp_35", "tmp_28", "tmp_16"});
    vec.push_back(std::vector<std::string>{"tmp_36", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_37", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_38", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_39", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_40", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_41", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_42", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_43", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_44", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_45", "tmp_21", "tmp_20"});
    vec.push_back(std::vector<std::string>{"tmp_46", "tmp_23", "tmp_22"});
    vec.push_back(std::vector<std::string>{"tmp_47", "tmp_25", "tmp_24"});
    vec.push_back(std::vector<std::string>{"tmp_48", "tmp_27", "tmp_26"});
    vec.push_back(std::vector<std::string>{"tmp_49", "tmp_30", "tmp_29"});
    vec.push_back(std::vector<std::string>{"tmp_50", "tmp_42", "tmp_41"});
    vec.push_back(std::vector<std::string>{"tmp_51", "ld_13"});
    vec.push_back(std::vector<std::string>{"tmp_52", "ld_14"});
    vec.push_back(std::vector<std::string>{"tmp_53", "tmp_33", "tmp_34"});
    vec.push_back(std::vector<std::string>{"tmp_54", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_55", "tmp_49", "tmp_35"});
    vec.push_back(std::vector<std::string>{"tmp_56", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_57", "tmp_48", "tmp_47"});
    vec.push_back(std::vector<std::string>{"tmp_58", "tmp_46", "tmp_45"});
    vec.push_back(std::vector<std::string>{"tmp_59", "tmp_44", "tmp_43"});
    vec.push_back(std::vector<std::string>{"tmp_60", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_61", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_62", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_63", "ld_13"});
    vec.push_back(std::vector<std::string>{"tmp_64", "ld_14"});
    vec.push_back(std::vector<std::string>{"tmp_65", "tmp_37", "tmp_36"});
    vec.push_back(std::vector<std::string>{"tmp_66", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_67", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_68", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_69", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_70", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_71", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_72", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_73", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_74", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_75", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_76", "tmp_39", "tmp_38"});
    vec.push_back(std::vector<std::string>{"tmp_77", "tmp_40", "tmp_32"});
    vec.push_back(std::vector<std::string>{"tmp_78", "tmp_59", "tmp_50"});
    vec.push_back(std::vector<std::string>{"tmp_79", "tmp_51", "tmp_52"});
    vec.push_back(std::vector<std::string>{"tmp_80", "tmp_53", "tmp_54"});
    vec.push_back(std::vector<std::string>{"tmp_81", "tmp_55", "tmp_56"});
    vec.push_back(std::vector<std::string>{"tmp_82", "tmp_57", "tmp_58"});
    vec.push_back(std::vector<std::string>{"tmp_83", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_84", "tmp_77", "tmp_76"});
    vec.push_back(std::vector<std::string>{"tmp_85", "tmp_73", "tmp_72"});
    vec.push_back(std::vector<std::string>{"tmp_86", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_87", "tmp_69", "tmp_70"});
    vec.push_back(std::vector<std::string>{"tmp_88", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_89", "tmp_67", "tmp_68"});
    vec.push_back(std::vector<std::string>{"tmp_90", "tmp_65", "tmp_66"});
    vec.push_back(std::vector<std::string>{"tmp_91", "tmp_63", "tmp_64"});
    vec.push_back(std::vector<std::string>{"tmp_92", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_93", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_94", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_95", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_96", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_97", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_98", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_99", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_100", "tmp_61", "tmp_62"});
    vec.push_back(std::vector<std::string>{"tmp_101", "tmp_71", "tmp_60"});
    vec.push_back(std::vector<std::string>{"tmp_102", "tmp_75", "tmp_74"});
    vec.push_back(std::vector<std::string>{"tmp_103", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_104", "tmp_93", "tmp_92"});
    vec.push_back(std::vector<std::string>{"tmp_105", "tmp_95", "tmp_94"});
    vec.push_back(std::vector<std::string>{"tmp_106", "tmp_97", "tmp_96"});
    vec.push_back(std::vector<std::string>{"tmp_107", "tmp_99", "tmp_98"});
    vec.push_back(std::vector<std::string>{"tmp_108", "tmp_101", "tmp_100"});
    vec.push_back(std::vector<std::string>{"tmp_109", "tmp_102", "tmp_85"});
    vec.push_back(std::vector<std::string>{"tmp_110", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_111", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_112", "tmp_78", "tmp_84"});
    vec.push_back(std::vector<std::string>{"tmp_113", "tmp_79", "tmp_80"});
    vec.push_back(std::vector<std::string>{"tmp_114", "tmp_81"});
    vec.push_back(std::vector<std::string>{"tmp_115", "tmp_82", "tmp_83"});
    vec.push_back(std::vector<std::string>{"tmp_116", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_117", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_118", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_119", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_120", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_121", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_122", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_123", "tmp_91", "tmp_90"});
    vec.push_back(std::vector<std::string>{"tmp_124", "tmp_89", "tmp_88"});
    vec.push_back(std::vector<std::string>{"tmp_125", "tmp_87", "tmp_86"});
    vec.push_back(std::vector<std::string>{"tmp_126", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_127", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_128", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_129", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_130", "ld_4"});
    vec.push_back(std::vector<std::string>{"tmp_131", "tmp_125"});
    vec.push_back(std::vector<std::string>{"tmp_132", "tmp_113", "tmp_114"});
    vec.push_back(std::vector<std::string>{"tmp_133", "tmp_115"});
    vec.push_back(std::vector<std::string>{"tmp_134", "tmp_112", "tmp_111"});
    vec.push_back(std::vector<std::string>{"tmp_135", "tmp_109", "tmp_108"});
    vec.push_back(std::vector<std::string>{"tmp_136", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_137", "tmp_107", "tmp_106"});
    vec.push_back(std::vector<std::string>{"tmp_138", "tmp_105", "tmp_104"});
    vec.push_back(std::vector<std::string>{"tmp_139", "tmp_103", "tmp_116"});
    vec.push_back(std::vector<std::string>{"tmp_140", "tmp_117", "tmp_118"});
    vec.push_back(std::vector<std::string>{"tmp_141", "tmp_119", "tmp_120"});
    vec.push_back(std::vector<std::string>{"tmp_142", "ld_6"});
    vec.push_back(std::vector<std::string>{"tmp_143", "ld_5"});
    vec.push_back(std::vector<std::string>{"tmp_144", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_145", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_146", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_147", "tmp_110", "tmp_126"});
    vec.push_back(std::vector<std::string>{"tmp_148", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_149", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_150", "tmp_123", "tmp_124"});
    vec.push_back(std::vector<std::string>{"tmp_151", "tmp_121", "tmp_122"});
    vec.push_back(std::vector<std::string>{"tmp_152", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_153", "tmp_148", "tmp_149"});
    vec.push_back(std::vector<std::string>{"tmp_154", "tmp_146", "tmp_130"});
    vec.push_back(std::vector<std::string>{"tmp_155", "tmp_144", "tmp_145"});
    vec.push_back(std::vector<std::string>{"tmp_156", "tmp_142", "tmp_143"});
    vec.push_back(std::vector<std::string>{"tmp_157", "tmp_141", "tmp_151"});
    vec.push_back(std::vector<std::string>{"tmp_158", "tmp_139", "tmp_140"});
    vec.push_back(std::vector<std::string>{"tmp_159", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_160", "tmp_137", "tmp_138"});
    vec.push_back(std::vector<std::string>{"tmp_161", "tmp_135", "tmp_136"});
    vec.push_back(std::vector<std::string>{"tmp_162", "tmp_134"});
    vec.push_back(std::vector<std::string>{"tmp_163", "tmp_132", "tmp_133"});
    vec.push_back(std::vector<std::string>{"tmp_164", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_165", "tmp_128", "tmp_129"});
    vec.push_back(std::vector<std::string>{"ld_15", "tmp_6", "tmp_7", "tmp_8", "tmp_9"});
    vec.push_back(std::vector<std::string>{"tmp_166", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_167", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_168", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_169", "tmp_147", "tmp_127"});
    vec.push_back(std::vector<std::string>{"tmp_170", "tmp_150", "tmp_131"});
    vec.push_back(std::vector<std::string>{"tmp_171", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_172", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_173", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_174", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_175", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_176", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_177", "ld_8"});
    vec.push_back(std::vector<std::string>{"tmp_178", "ld_11"});
    vec.push_back(std::vector<std::string>{"tmp_179", "tmp_172", "tmp_171"});
    vec.push_back(std::vector<std::string>{"tmp_180", "tmp_174", "tmp_164"});
    vec.push_back(std::vector<std::string>{"tmp_181", "tmp_176", "tmp_175"});
    vec.push_back(std::vector<std::string>{"tmp_182", "tmp_152", "tmp_173"});
    vec.push_back(std::vector<std::string>{"tmp_183", "tmp_154", "tmp_153"});
    vec.push_back(std::vector<std::string>{"tmp_184", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_185", "tmp_158", "tmp_157"});
    vec.push_back(std::vector<std::string>{"tmp_186", "tmp_160", "tmp_159"});
    vec.push_back(std::vector<std::string>{"tmp_187", "tmp_161"});
    vec.push_back(std::vector<std::string>{"tmp_188", "tmp_163", "tmp_162"});
    vec.push_back(std::vector<std::string>{"tmp_189", "tmp_156", "tmp_155"});
    vec.push_back(std::vector<std::string>{"tmp_190", "ld_1"});
    vec.push_back(std::vector<std::string>{"tmp_191", "ld_0"});
    vec.push_back(std::vector<std::string>{"tmp_192", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_193", "ld_15"});
    vec.push_back(std::vector<std::string>{"tmp_194", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_195", "ld_15"});
    vec.push_back(std::vector<std::string>{"tmp_196", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_197", "tmp_167", "tmp_166"});
    vec.push_back(std::vector<std::string>{"tmp_198", "tmp_170", "tmp_169"});
    vec.push_back(std::vector<std::string>{"tmp_199", "ld_10"});
    vec.push_back(std::vector<std::string>{"tmp_200", "ld_3"});
    vec.push_back(std::vector<std::string>{"tmp_201", "ld_9"});
    vec.push_back(std::vector<std::string>{"tmp_202", "ld_2"});
    vec.push_back(std::vector<std::string>{"tmp_203", "tmp_165", "tmp_168"});
    vec.push_back(std::vector<std::string>{"tmp_204", "tmp_178", "tmp_177"});
    vec.push_back(std::vector<std::string>{"ld_16"});
    vec.push_back(std::vector<std::string>{"tmp_205", "tmp_188", "tmp_187"});
    vec.push_back(std::vector<std::string>{"tmp_206", "tmp_186"});
    vec.push_back(std::vector<std::string>{"tmp_207", "tmp_185", "tmp_184"});
    vec.push_back(std::vector<std::string>{"tmp_208", "tmp_189", "tmp_183"});
    vec.push_back(std::vector<std::string>{"tmp_209", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_210", "tmp_182", "tmp_181"});
    vec.push_back(std::vector<std::string>{"tmp_211", "tmp_180", "tmp_179"});
    vec.push_back(std::vector<std::string>{"tmp_212", "tmp_191", "tmp_190"});
    vec.push_back(std::vector<std::string>{"tmp_213", "tmp_202", "tmp_201"});
    vec.push_back(std::vector<std::string>{"tmp_214", "tmp_200", "tmp_199"});
    vec.push_back(std::vector<std::string>{"tmp_215", "tmp_198", "tmp_203"});
    vec.push_back(std::vector<std::string>{"tmp_216", "tmp_197", "tmp_196"});
    vec.push_back(std::vector<std::string>{"tmp_217", "tmp_195", "tmp_194"});
    vec.push_back(std::vector<std::string>{"tmp_218", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_219", "tmp_193", "tmp_192"});
    vec.push_back(std::vector<std::string>{"tmp_220", "ld_12"});
    vec.push_back(std::vector<std::string>{"tmp_221", "tmp_205", "tmp_206"});
    vec.push_back(std::vector<std::string>{"tmp_222", "tmp_207"});
    vec.push_back(std::vector<std::string>{"tmp_223", "tmp_208", "tmp_209"});
    vec.push_back(std::vector<std::string>{"tmp_224", "tmp_210", "tmp_211"});
    vec.push_back(std::vector<std::string>{"tmp_225", "ld_16"});
    vec.push_back(std::vector<std::string>{"tmp_226", "tmp_204", "tmp_212"});
    vec.push_back(std::vector<std::string>{"tmp_227", "tmp_215", "tmp_216"});
    vec.push_back(std::vector<std::string>{"tmp_228", "tmp_217", "tmp_218"});
    vec.push_back(std::vector<std::string>{"tmp_229", "tmp_219", "tmp_220"});
    vec.push_back(std::vector<std::string>{"tmp_230", "ld_15"});
    vec.push_back(std::vector<std::string>{"tmp_231", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_232", "tmp_213", "tmp_214"});
    vec.push_back(std::vector<std::string>{"tmp_233", "tmp_230", "tmp_231"});
    vec.push_back(std::vector<std::string>{"tmp_234", "tmp_221", "tmp_222"});
    vec.push_back(std::vector<std::string>{"tmp_235", "tmp_223"});
    vec.push_back(std::vector<std::string>{"tmp_236", "tmp_224", "tmp_225"});
    vec.push_back(std::vector<std::string>{"tmp_237", "tmp_226", "tmp_232"});
    vec.push_back(std::vector<std::string>{"tmp_238", "ld_16"});
    vec.push_back(std::vector<std::string>{"tmp_239", "tmp_227", "tmp_228"});
    vec.push_back(std::vector<std::string>{"tmp_240", "tmp_229"});
    vec.push_back(std::vector<std::string>{"tmp_241", "ld_16"});
    vec.push_back(std::vector<std::string>{"tmp_242", "ld_15"});
    vec.push_back(std::vector<std::string>{"tmp_243", "ld_7"});
    vec.push_back(std::vector<std::string>{"tmp_244", "tmp_242", "tmp_243"});
    vec.push_back(std::vector<std::string>{"tmp_245", "tmp_236"});
    vec.push_back(std::vector<std::string>{"tmp_246", "tmp_234", "tmp_235"});
    vec.push_back(std::vector<std::string>{"tmp_247", "tmp_237", "tmp_238"});
    vec.push_back(std::vector<std::string>{"tmp_248", "tmp_233", "tmp_241"});
    vec.push_back(std::vector<std::string>{"tmp_249", "ld_16"});
    vec.push_back(std::vector<std::string>{"tmp_250", "tmp_239", "tmp_240"});
    vec.push_back(std::vector<std::string>{"tmp_251", "tmp_244", "tmp_249"});
    vec.push_back(std::vector<std::string>{"tmp_252", "tmp_250", "tmp_248"});
    vec.push_back(std::vector<std::string>{"tmp_253", "tmp_247"});
    vec.push_back(std::vector<std::string>{"tmp_254", "tmp_246", "tmp_245"});
    vec.push_back(std::vector<std::string>{"tmp_255", "tmp_252", "tmp_251"});
    vec.push_back(std::vector<std::string>{"tmp_256", "tmp_254", "tmp_253"});
    vec.push_back(std::vector<std::string>{"tmp_257", "tmp_255"});
    vec.push_back(std::vector<std::string>{"tmp_258", "tmp_256"});
    vec.push_back(std::vector<std::string>{"tmp_259", "tmp_255"});
    vec.push_back(std::vector<std::string>{"tmp_260", "tmp_256"});
    vec.push_back(std::vector<std::string>{"tmp_261", "tmp_258", "tmp_257"});
    vec.push_back(std::vector<std::string>{"tmp_262", "tmp_260", "tmp_259"});
    vec.push_back(std::vector<std::string>{"x", "tmp_262"});
    vec.push_back(std::vector<std::string>{"y", "tmp_261"});

    std::vector<MinimizeRegisters::InsDeps> vecids;
    std::unordered_map<std::string,int> strToIds;
    auto GetId = [&](const std::string& str) -> int{
        if(strToIds.find(str) == strToIds.end()){
            strToIds[str] = int(strToIds.size());
        }
        return strToIds[str];
    };
    for(auto& strIns : vec){
        if(strIns.size()){
            MinimizeRegisters::InsDeps op;
            op.write.push_back(GetId(strIns[0]));
            for(int idx = 1 ; idx < int(strIns.size()) ; ++idx){
                op.read.push_back(GetId(strIns[idx]));
            }
            vecids.push_back(std::move(op));
        }
    }

    return vecids;
}

inline std::vector<MinimizeRegisters::InsDeps> AddOffsetToIns(std::vector<MinimizeRegisters::InsDeps> inIds, const int offset){
    for(int idxId = 0 ; idxId < int(inIds.size()) ; ++idxId){
        auto& op = inIds[idxId];
        for(auto& idxR : op.read){
            idxR += offset;
        }
        for(auto& idxW : op.write){
            idxW += offset;
        }
    }
    return inIds;
}

inline std::vector<MinimizeRegisters::InsDeps> MixInstructions(const std::vector<MinimizeRegisters::InsDeps>& inIds1, const int inStep1,
            const std::vector<MinimizeRegisters::InsDeps>& inIds2, const int inStep2){
    std::vector<MinimizeRegisters::InsDeps> inIds;
    auto iter1 = inIds1.begin();
    auto end1 = inIds1.end();
    auto iter2 = inIds2.begin();
    auto end2 = inIds2.end();
    while(iter1 != end1 || iter2 != end2){
        for(int cpt = 0 ; cpt < inStep1 && iter1 != end1; ++cpt, ++iter1){
            inIds.push_back(*iter1);
        }
        for(int cpt = 0 ; cpt < inStep2 && iter2 != end2; ++cpt, ++iter2){
            inIds.push_back(*iter2);
        }
    }
    return inIds;
}

void GenerateCodeFromIns(const std::vector<MinimizeRegisters::InsDeps>& instructions,
                         std::string refName = ""){
    std::set<int> listOfStores;
    {
        std::set<int> useInRead;
        for(auto& ins: instructions){
            for(int id: ins.read){
                useInRead.insert(id);
            }
        }
        for(auto& ins: instructions){
            for(int id: ins.write){
                if(useInRead.find(id) == useInRead.end()){
                    listOfStores.insert(id);
                }
            }
        }
    }

    {
        std::cout << "void func(const double* input, double* output){" << std::endl;

        int cptW = 0;
        int cptR = 0;
        for(auto& ins: instructions){
            for(int id: ins.write){
                if(listOfStores.find(id) == listOfStores.end()){
                    std::cout << "double var" << id << " = ";
                }
                else{
                    std::cout << "output[" << cptW++ << "] = ";
                }
            }

            if(ins.read.size() != 0){
                bool isFirst = true;
                for(int id: ins.read){
                    assert((listOfStores.find(id) == listOfStores.end()));
                    if(isFirst){
                        isFirst = false;
                        std::cout << "var" << id;
                    }
                    else{
                        std::cout << " + var" << id;
                    }
                }
                std::cout << ";" << std::endl;
            }
            else{
                std::cout << "input[" << cptR++ << "];" << std::endl;;
            }
        }

        std::cout  << "}" << std::endl;
    }

    {
        std::unordered_map<int, std::shared_ptr<MinimizeRegisters::Node>> allNodes;
        std::vector<std::shared_ptr<MinimizeRegisters::Node>> roots;
        std::tie(allNodes,roots) = MinimizeRegisters::CreateDag(instructions);

        std::unordered_map<int, std::shared_ptr<Autovec::Node>> finalIdNodes;
        std::vector<std::shared_ptr<Autovec::Node>> outputNodes;

        std::queue<int> readyNodes;

        for(auto& idnd : allNodes){
            auto& nd = idnd.second;
            if(nd->predecessors.size() == 0){
                assert(std::find(roots.begin(), roots.end(), nd) != roots.end());
                readyNodes.push(nd->opId);
            }
        }
        assert(readyNodes.size());

        std::vector<int> countDeps(allNodes.size(), 0);

        std::shared_ptr<Autovec::Node> set99(new Autovec::Node);
        set99->getData() = Autovec::Instruction::make_vectorial_set("double", 99.);

        int inputIdx = 0;
        int outputIdx = 0;

        while(readyNodes.size()){
            auto ndId = readyNodes.front();
            const auto& toproceed = (*allNodes.find(ndId)).second;
            readyNodes.pop();
            std::shared_ptr<Autovec::Node> newNd(new Autovec::Node);
            if(toproceed->predecessors.empty()){
                std::vector<std::ptrdiff_t> offset;
                while(offset.size() != 8){
                    offset.push_back(inputIdx++);
                }
                newNd->getData() = Autovec::Instruction::make_vectorial_load("double", 0, offset);
            }
            else if(toproceed->successors.empty()){
                std::vector<std::ptrdiff_t> offset;
                while(offset.size() != 8){
                    offset.push_back(outputIdx++);
                }
                newNd->getData() = Autovec::Instruction::make_vectorial_store("double", 1, offset);
                outputNodes.push_back(newNd);
            }
            else if(toproceed->predecessors.size() > 1){
                newNd->getData() = Autovec::Instruction::make_vectorial_operation("double", "+");
            }
            else {
                newNd->getData() = Autovec::Instruction::make_vectorial_operation("double", "-");
            }
            finalIdNodes[toproceed->opId] = newNd;

            if(toproceed->predecessors.size() == 2){
                Autovec::Node::Connect(newNd, finalIdNodes[toproceed->predecessors[0]->opId], 0);
                Autovec::Node::Connect(newNd, finalIdNodes[toproceed->predecessors[1]->opId], 1);
            }
            else if(toproceed->predecessors.size() == 1){
                Autovec::Node::Connect(newNd, finalIdNodes[toproceed->predecessors[0]->opId], 0);
                Autovec::Node::Connect(newNd, set99, 1);
            }
            else if(toproceed->predecessors.size() > 2){
                std::shared_ptr<Autovec::Node> joinNd(new Autovec::Node);
                joinNd->getData() = Autovec::Instruction::make_vectorial_operation("double", "+");
                Autovec::Node::Connect(joinNd, finalIdNodes[toproceed->predecessors[0]->opId], 0);
                for(int idxPred = 1 ; idxPred < int(toproceed->predecessors.size())-2 ; ++idxPred){
                    Autovec::Node::Connect(joinNd, finalIdNodes[toproceed->predecessors[idxPred]->opId], 1);
                    std::shared_ptr<Autovec::Node> newJoinNd(new Autovec::Node);
                    newJoinNd->getData() = Autovec::Instruction::make_vectorial_operation("double", "+");
                    Autovec::Node::Connect(newJoinNd, joinNd, 0);
                    joinNd = newJoinNd;
                }
                Autovec::Node::Connect(joinNd, finalIdNodes[toproceed->predecessors[toproceed->predecessors.size()-2]->opId], 1);

                Autovec::Node::Connect(newNd, joinNd, 0);
                Autovec::Node::Connect(newNd, finalIdNodes[toproceed->predecessors[toproceed->predecessors.size()-1]->opId], 1);
            }

            // Release dependencies
            for(auto succ : toproceed->successors){
                countDeps[succ->opId] += 1;
                if(countDeps[succ->opId] == int(succ->predecessors.size())){
                    readyNodes.push(succ->opId);
                }
            }
        }

        {// Sanity check
            std::set<Autovec::Node*> alreadySeen;
            std::queue<std::shared_ptr<Autovec::Node>> nodesToProceed;
            for(auto& node : outputNodes){
                nodesToProceed.push(node);
                assert(node->isRoot());
                assert(alreadySeen.find(node.get()) == alreadySeen.end());
                alreadySeen.insert(node.get());
            }

            std::unordered_map<Autovec::Node*, int> cpt;

            while(nodesToProceed.size()){
                std::shared_ptr<Autovec::Node> currentNode = nodesToProceed.front();
                nodesToProceed.pop();

                if(currentNode->getNbInputs(0)){
                    assert(currentNode->getNbInputs(0) == 1);
                    cpt[currentNode->getInputs(0).front().get()] += 1;
                    if(currentNode->getInputs(0).front()->getNbOutputs()
                            == cpt[currentNode->getInputs(0).front().get()]){
                        assert(alreadySeen.find(currentNode->getInputs(0).front().get()) == alreadySeen.end());
                        alreadySeen.insert(currentNode->getInputs(0).front().get());
                        nodesToProceed.push(currentNode->getInputs(0).front());
                    }
                    if(currentNode->getNbInputs(1)){
                        assert(currentNode->getNbInputs(1) == 1);
                        cpt[currentNode->getInputs(1).front().get()] += 1;
                        if(currentNode->getInputs(1).front()->getNbOutputs()
                                == cpt[currentNode->getInputs(1).front().get()]){
                            assert(alreadySeen.find(currentNode->getInputs(1).front().get()) == alreadySeen.end());
                            alreadySeen.insert(currentNode->getInputs(1).front().get());
                            nodesToProceed.push(currentNode->getInputs(1).front());
                        }
                    }
                }
                else{
                    assert(currentNode->getNbInputs(1) == 0);
                }
            }
        }


        {
            std::stringstream stream;
            Autovec::GenerateAVX512(outputNodes, stream, false, 8);
            if(refName.empty()){
                std::cout << "void func(double* tab0, double* tab1){\n";
                std::cout << stream.str() << std::endl;
                std::cout << "}\n";
            }
            else{
                std::ofstream myfile;
                  myfile.open (refName + "avx-noperm.cpp");
                  myfile << "#include <immintrin.h>\n";
                  myfile << "void func(double* tab0, double* tab1){\n";
                  myfile << stream.str() << std::endl;
                  myfile << "}\n";
                  myfile.close();
            }
        }
        {
            std::stringstream stream;
            Autovec::GenerateAVX512(outputNodes, stream, true, 8);
            if(refName.empty()){
                std::cout << "void func(double* tab0, double* tab1){\n";
                std::cout << stream.str() << std::endl;
                std::cout << "}\n";
            }
            else{
                std::ofstream myfile;
                  myfile.open (refName + "avx-perm.cpp");
                  myfile << "#include <immintrin.h>\n";
                  myfile << "void func(double* tab0, double* tab1){\n";
                  myfile << stream.str() << std::endl;
                  myfile << "}\n";
                  myfile.close();
            }
        }
//        {
//            std::stringstream stream;
//            Autovec::GenerateAVX512Asm(outputNodes, stream, false, 8,
//                              "func", true);
//            if(refName.empty()){
//                std::cout << stream.str() << std::endl;
//            }
//            else{
//                std::ofstream myfile;
//                  myfile.open (refName + "asm-noperm.cpp");
//                  myfile << stream.str() << std::endl;
//                  myfile.close();
//            }
//        }
//        {
//            std::stringstream stream;
//            Autovec::GenerateAVX512Asm(outputNodes, stream, true, 8,
//                              "func", true);
//            if(refName.empty()){
//                std::cout << stream.str() << std::endl;
//            }
//            else{
//                std::ofstream myfile;
//                  myfile.open (refName + "asm-perm.cpp");
//                  myfile << stream.str() << std::endl;
//                  myfile.close();
//            }
//        }
    }
}

void test(){
    // GetRealData() GenerateRand(200) ToyGraph()
    auto base = NoUseIsStore(GenerateRand(300));
    //const int nbRegisters = 3;
    //auto base = GetRealData();
    const int nbRegisters = 32;

    auto operations = base;//MixInstructions(base, 1, AddOffsetToIns(base, GetMaxId(base)+1), 1);
    MinimizeRegisters::DagToDot(std::get<1>(MinimizeRegisters::CreateDag(operations)), "/tmp/graph.dot");
    std::cout << "Nb operations = " << operations.size() << std::endl;

    {
        auto pushPop = MinimizeRegisters::GetNbPushPop(operations, nbRegisters);
        std::cout << "Nb push = " << std::get<0>(pushPop) << std::endl;
        std::cout << "Nb pop  = " << std::get<1>(pushPop) << std::endl;
        MinimizeRegisters::PrintOperations(operations, std::get<2>(pushPop), std::get<3>(pushPop));
        GenerateCodeFromIns(operations);
    }

    /////////////////////////////////////////////////////
    std::cout << " ============================================= " << std::endl;
    std::cout << " ============================================= " << std::endl;
    std::cout << " ============================================= " << std::endl;
    {
        auto permute = MinimizeRegisters::Permute(operations);
        auto permutedOperationsIds = MinimizeRegisters::ApplyPermutation(operations, permute);

        {
            auto pushPop = MinimizeRegisters::GetNbPushPop(permutedOperationsIds, nbRegisters);
            std::cout << "Nb push = " << std::get<0>(pushPop) << std::endl;
            std::cout << "Nb pop  = " << std::get<1>(pushPop) << std::endl;
            MinimizeRegisters::PrintOperations(permutedOperationsIds, std::get<2>(pushPop), std::get<3>(pushPop));
            GenerateCodeFromIns(permutedOperationsIds);
        }
    }
    /////////////////////////////////////////////////////
    if(operations.size() < 15){
        std::cout << " ============================================= " << std::endl;
        std::cout << " ============================================= " << std::endl;
        std::cout << " ============================================= " << std::endl;
        {
            auto permute = MinimizeRegisters::PermuteCutAndBranch(operations, nbRegisters);
            auto permutedOperationsIds = ApplyPermutation(operations, permute);

            {
                auto pushPop = MinimizeRegisters::GetNbPushPop(permutedOperationsIds, nbRegisters);
                std::cout << "Nb push = " << std::get<0>(pushPop) << std::endl;
                std::cout << "Nb pop  = " << std::get<1>(pushPop) << std::endl;
                MinimizeRegisters::PrintOperations(permutedOperationsIds, std::get<2>(pushPop), std::get<3>(pushPop));
                GenerateCodeFromIns(permutedOperationsIds);
            }
        }
    }
}

void bench(){
    const std::vector<int> kernelSize = {4, 10};
    const int maxStep = 11;
    const int startSize = 64;
    int res[2][maxStep][4] = {0};

    for(int idxks = 0 ; idxks < int(kernelSize.size()) ; ++idxks){
        const auto ks = kernelSize[idxks];

        for(int idxStep = 0 ; idxStep < maxStep ; idxStep += 1){
            const int idxSize = idxStep*32+startSize;

            std::cout << "Size = " << idxSize << std::endl;
            auto base = NoUseIsStore(GenerateRand(idxSize,ks));
            const int nbRegisters = 32;

            auto operations = base;
            MinimizeRegisters::DagToDot(std::get<1>(MinimizeRegisters::CreateDag(operations)), "/tmp/graph.dot");
            std::cout << "Nb operations = " << operations.size() << std::endl;

            {
                auto pushPop = MinimizeRegisters::GetNbPushPop(operations, nbRegisters);
                std::cout << "Nb push = " << std::get<0>(pushPop) << std::endl;
                std::cout << "Nb pop  = " << std::get<1>(pushPop) << std::endl;
                MinimizeRegisters::PrintOperations(operations, std::get<2>(pushPop), std::get<3>(pushPop));
                GenerateCodeFromIns(operations, "base-" + std::to_string(idxSize));
            }

            /////////////////////////////////////////////////////
            std::cout << " ============================================= " << std::endl;
            std::cout << " ============================================= " << std::endl;
            std::cout << " ============================================= " << std::endl;
            {
                auto permute = MinimizeRegisters::Permute(operations);
                auto permutedOperationsIds = MinimizeRegisters::ApplyPermutation(operations, permute);

                {
                    auto pushPop = MinimizeRegisters::GetNbPushPop(permutedOperationsIds, nbRegisters);
                    std::cout << "Nb push = " << std::get<0>(pushPop) << std::endl;
                    std::cout << "Nb pop  = " << std::get<1>(pushPop) << std::endl;
                    MinimizeRegisters::PrintOperations(permutedOperationsIds, std::get<2>(pushPop), std::get<3>(pushPop));
                    GenerateCodeFromIns(permutedOperationsIds, "perm-" + std::to_string(idxSize));
                }
            }

            const std::string filenames[4]=
            {"base-" + std::to_string(idxSize) + "avx-perm",
             "base-" + std::to_string(idxSize) + "avx-noperm",
                "perm-" + std::to_string(idxSize) + "avx-perm",
            "perm-" + std::to_string(idxSize) + "avx-noperm"};
            for(int idx = 0 ; idx < 4 ; ++idx){
                // clang++ -march=skylake-avx512 -mtune=skylake-avx512 -O3 -S X.cpp
                // cat perm-320asm-perm.s | grep '(%rsp)' | wc -l
                const std::string command = "clang++ -march=skylake-avx512 -mtune=skylake-avx512 -O3 -S " + filenames[idx] + ".cpp";
                system(command.c_str());
                std::ifstream assemblyFile(filenames[idx] + ".s");
                std::stringstream buffer;
                buffer << assemblyFile.rdbuf();
                const std::string assembly = buffer.str();

                std::string needle = "(%rsp)";
                auto byneedle = std::boyer_moore_searcher(needle.begin(), needle.end());
                auto it = assembly.begin();
                const auto assemblyEnd = assembly.end();
                int cpt = 0;
                while((it = search(it, assemblyEnd, byneedle)) != assembly.end()){
                        cpt += 1;
                        ++it;
                }

                std::cout << "Result = " << cpt << std::endl;

                res[idxks][idxStep][idx] = cpt;
            }
        }
    }

    const std::string labels[4]= {"base-avx-perm", "base-avx-noperm",
                                     "perm-avx-perm", "perm-avx-noperm"};

    std::cout << "# Size";
    for(int idxks = 0 ; idxks < int(kernelSize.size()) ; ++idxks){
        const auto ks = kernelSize[idxks];
        for(int idx = 0 ; idx < 4 ; ++idx){
            std::cout << "\t" << labels[idx] << "-" << ks;
        }
    }
    std::cout << std::endl;
    for(int idxStep = 0 ; idxStep < maxStep ; idxStep += 1){
        const int idxSize = idxStep*32+startSize;
        std::cout << idxSize;
        for(int idxks = 0 ; idxks < int(kernelSize.size()) ; ++idxks){
            for(int idx = 0 ; idx < 4 ; ++idx){
                std::cout << "\t" << res[idxks][idxStep][idx];
            }
        }
        std::cout << std::endl;
    }
}

int main(){
    bench();
    return 0;
}
