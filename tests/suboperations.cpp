#include <iostream>
#include "../src/suboperations-algos.hpp"

void printRes(const std::vector<std::vector<int>>& inSplit, const std::vector<std::vector<double>>& inScores){
    for(int idxVec = 0 ; idxVec < int(inSplit.size()) ; ++idxVec){
        std::cout << "Vec: " << idxVec << " of size " << inSplit[idxVec].size() << std::endl;
        std::cout << "Indices: ";
        for(auto& idx : inSplit[idxVec] ){
            std::cout << idx << " ";
        }
        std::cout << std::endl;


        for(int idx1 = 0 ; idx1 < int(inSplit[idxVec].size()) ; ++idx1){
            for(int idx2 = 0 ; idx2 < int(inSplit[idxVec].size()) ; ++idx2){
                std::cout << inScores[inSplit[idxVec][idx1]][inSplit[idxVec][idx2]] << " ";
            }
            std::cout << std::endl;
        }
    }
}

std::vector<std::vector<double>> ArrayToVec(const double* array, const int size){
    std::vector<std::vector<double>> res(size);
    for(int idx1 = 0 ; idx1 < size ; ++idx1){
        res[idx1].resize(size);
        for(int idx2 = 0 ; idx2 < size ; ++idx2){
            res[idx1][idx2] = array[idx1*size+idx2];
        }
    }
    return res;
}

void test(const double* array, const int size){
    auto scores = ArrayToVec(array, size);
    auto resTogetherT = GetSubOperationsVecTogether(scores, 8, true);
    auto res1by1T = GetSubOperations1by1(scores, 8, true);
    auto resTogetherF = GetSubOperationsVecTogether(scores, 8, false);
    auto res1by1F = GetSubOperations1by1(scores, 8, false);

    std::cout << "Res together True" << std::endl;
    printRes(resTogetherT, scores);

    std::cout << "=============" << std::endl;

    std::cout << "Res 1by1 True" << std::endl;
    printRes(res1by1T, scores);

    std::cout << "=============" << std::endl;

    std::cout << "Res together False" << std::endl;
    printRes(resTogetherF, scores);

    std::cout << "=============" << std::endl;

    std::cout << "Res 1by1 False" << std::endl;
    printRes(res1by1F, scores);
}

int main(){
    std::cout << "======================================" << std::endl;
    {
        const double array[] = {2, 2, 2, 2, 0, 0,
                                2, 2, 2, 2, 0, 0,
                                2, 2, 2, 2, 0, 0,
                                2, 2, 2, 2, 0, 0,
                                0, 0, 0, 0, 2, 2,
                                0, 0, 0, 0, 2, 2};
        test(array, 6);
    }
    // std::cout << "======================================" << std::endl;
    // {
    //     const double array[] = {2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 1, 1,
    //                             1, 1, 1, 1, 2, 2,
    //                             1, 1, 1, 1, 2, 2};
    //     test(array, 6);
    // }
    // std::cout << "======================================" << std::endl;
    // {
    //     const double array[] = {2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 1, 1,
    //                             2, 2, 2, 2, 2, 2,
    //                             1, 1, 1, 2, 2, 2,
    //                             1, 1, 1, 2, 2, 2};
    //     test(array, 6);
    // }

    std::cout << "======================================" << std::endl;
    {
        const double array[] = {2, 2, 2, 0, 0, 0, 0, 0, 0,
                                2, 2, 2, 0, 0, 0, 0, 0, 0,
                                2, 2, 2, 0, 0, 0, 0, 0, 0,
                                0, 0, 0, 2, 2, 2, 0, 0, 0,
                                0, 0, 0, 2, 2, 2, 0, 0, 0,
                                0, 0, 0, 2, 2, 2, 0, 0, 0,
                                0, 0, 0, 0, 0, 0, 2, 2, 2,
                                0, 0, 0, 0, 0, 0, 2, 2, 2,
                                0, 0, 0, 0, 0, 0, 2, 2, 2};
        std::cout << "============= TEST ==============" << std::endl;
        test(array, 9);
    }

    std::cout << "======================================" << std::endl;
    {
        const double array[] = {3, 3, 2, 2, 2, 2, 2, 2, 1, 1,
                                3, 3, 2, 2, 2, 2, 2, 2, 1, 1,
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2, 
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2,
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2,
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2,
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2,
                                2, 2, 3, 3, 3, 3, 3, 3, 2, 2,
                                1, 1, 2, 2, 2, 2, 2, 2, 3, 3,
                                1, 1, 2, 2, 2, 2, 2, 2, 3, 3};
        std::cout << "============= TEST ==============" << std::endl;
        test(array, 10);
    }

    std::cout << "======================================" << std::endl;
    {
        const double array[] = {3, 3, 3, 3, 3, 3, 2, 2, 1, 1,
                                3, 3, 3, 3, 3, 3, 2, 2, 1, 1,
                                3, 3, 3, 3, 3, 3, 2, 2, 1, 1, 
                                3, 3, 3, 3, 3, 3, 2, 2, 1, 1,
                                3, 3, 3, 3, 3, 3, 2, 2, 1, 1,
                                3, 3, 3, 3, 3, 3, 2, 2, 1, 1,
                                2, 2, 2, 2, 2, 2, 3, 3, 0, 0,
                                2, 2, 2, 2, 2, 2, 3, 3, 0, 0,
                                1, 1, 1, 1, 1, 1, 0, 0, 3, 3,
                                1, 1, 1, 1, 1, 1, 0, 0, 3, 3};
        std::cout << "============= TEST ==============" << std::endl;
        test(array, 10);
    }

    // test with VEC_SIZE = 2
    // std::cout << "======================================" << std::endl;
    // {
    //     const double array[] = {3, 2, 1, 1, 0,
    //                             2, 3, 0, 0, 1,
    //                             1, 0, 3, 2, 2,
    //                             1, 0, 2, 3, 1, 
    //                             0, 1, 2, 1, 3};
    //     test(array, 5);
    // }

    return 0;
}
